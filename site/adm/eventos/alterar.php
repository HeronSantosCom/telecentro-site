<?php
// Restabelecendo a sess�o
require('../sessao.php');
$usuarioatual 	= $_SESSION['usuario'];
$nomeatual 		= $_SESSION['nome'];
$nivelatual		= $_SESSION['nivel'];

//Definindo menu
$pagina = 'evento';

//Recebendo vari�vel
$id = $_GET["id"];

//Conectando o banco
include ('../classes/banco.class.php');
$banco = new Banco;
$banco->conectar();

//Executando a consulta no banco de dados
$sql = "SELECT * FROM evento WHERE id='$id'";
$resultado = mysql_query($sql);

if (!$resultado){
?>
<script language="javascript" type="text/javascript">
alert('Erro no Banco de Dados: N�o foi poss�vel realizar a consulta.');
window.location.href = 'controle.php';
</script>
<?php
exit();
}

//Lendo o banco de dados e separando os valores nas variaveis
while ($linha = mysql_fetch_array ($resultado)){
	$id 			= $linha ["id"];
	$titulo			= $linha ["titulo"];
	$subtitulo		= $linha ["subtitulo"];
	$texto			= $linha ["texto"];
	$pprincipal		= $linha ["paginaprincipal"];
	$pcomunidade	= $linha ["paginacomunidade"];
	$pescola		= $linha ["paginaescola"];
	$disponivel		= $linha ["disponivel"];

	//Formatando vari&aacute;veis para impress&atilde;o
	$titulo			= stripslashes($titulo);
	$subtitulo		= stripslashes($subtitulo);
}

$banco->fechar();
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Nova Igua&ccedil;u Digital - Administra&ccedil;&atilde;o</title>

<!-- Estilo da P�gina -->
<link href="../css/estilo.css" rel="stylesheet" type="text/css" />

<!-- Validar Cadastro -->
<script language="javascript" type="text/javascript" src="../javascript/script.js"></script>
<script type="text/javascript">
      _editor_url = "htmlarea/";
      _editor_lang = "en";
    </script>

    <!-- load the main HTMLArea files -->
    <script type="text/javascript" src="htmlarea/htmlarea.js"></script>

    <script type="text/javascript">
      HTMLArea.loadPlugin("FullPage");

      function initDocument() {
        var editor = new HTMLArea("texto");
        editor.registerPlugin(FullPage);
        editor.generate();
      }
	  var config = new HTMLArea.Config(); 
          config.width = '670px';
          config.height = '400px';
          config.pageStyle =  'body { font-family: Arial; font-size:11px; }';

    </script>

</head>

<body onLoad="HTMLArea.replace('texto', config);">
<div id="pagina">
  <div id="topo"><img src="../images/topo.jpg" alt="Nova Igua&ccedil;u Digital (Adminitra&ccedil;&atilde;o)" width="777" height="49"></div>
  <div class="menu" id="menu">
    <table width="100%" height="100%" border="0" align="left" cellpadding="2" cellspacing="2">
      <tr>
        <td valign="bottom"><?php include ('../menu.php'); ?></td>
      </tr>
    </table>
  </div>
  <div id="linha"><img src="../usuario/images/linha.jpg" width="1" height="3"></div>
  <div id="id">
    <table width="100%" border="0" cellspacing="0" cellpadding="2">
      <tr>
        <td width="20%" valign="bottom"><span class="txt">Ol&aacute;, <strong><?php echo $nomeatual; ?></strong></span></td>
        <td width="50%" valign="bottom"><span class="txt">N�vel: <strong><?php switch ($nivelatual) { 
																case 1 : echo "Administrador"; break;
																case 2 : echo "Redator"; break;
																case 3 : echo "Usu�rio"; break;
															}
													  ?></strong></span></td>
        <td width="30%" valign="bottom" align="right"><span class="txt"><script language="javascript" type="text/javascript" src="../usuario/javascript/data.js"></script></span></td>
      </tr>
    </table>
    <hr color="#E7701A" size="1px">     
  </div>
  <div id="cont"><span class="titulo-01">Alterar Evento </span>
    <hr color="#E7701A" size="1px">
    <table width="100%"  border="0" cellspacing="2" cellpadding="2">
	<form name="evento" method="post" action="alterardb.php?id=<?php echo $id; ?>" onSubmit="return validarEvento()">
	  <tr class="txt">
        <td width="22%" class="txtb"><strong>ID: </strong></td>
        <td width="78%" class="txtb"><?php echo $id; ?></td>
      </tr>
      <tr class="txt">
        <td><span  class="txtb"><strong>T&iacute;tulo: </strong></span></td>
        <td><input name="titulo" type="text" class="caixa" id="titulo" value="<?php echo $titulo; ?>" size="80" maxlength="100"></td>
      </tr>
      <tr class="txt">
        <td><strong>Subt&iacute;tulo:</strong></td>
        <td><input name="subtitulo" type="text" class="caixa" id="subtitulo" value="<?php echo $subtitulo; ?>" size="100" maxlength="200"></td>
      </tr>
      <tr class="txt">
        <td valign="top"><span  class="txtb"><strong>Texto:</strong></span></td>
        <td><textarea name="texto"  class="caixa" id="texto"><?php echo $texto; ?></textarea></td>
      </tr>
      <tr class="txt">
        <td><strong>Destino:</strong></td>
        <td><input name="paginaprincipal" type="checkbox" value="S" <?php if ($pprincipal == "S") {echo "checked='checked'";} ?>>Portal Principal
  			<input name="paginacomunidade" type="checkbox" value="S" <?php if ($pcomunidade == "S") {echo "checked='checked'";} ?>>Portal Comunidade
			<input name="paginaescola" type="checkbox" value="S" <?php if ($pescola == "S") {echo "checked='checked'";} ?>>Portal Escola</td>
      </tr>
      <tr class="txt">
        <td><strong>Publicar:</strong></td>
        <td><input name="disponivel" type="checkbox" id="disponivel" value="S" <?php if ($disponivel == "S") {echo "checked='checked'";} ?>></td>
      </tr>
      <tr class="txt">
        <td>&nbsp;</td>
        <td><input name="Submit" type="submit" class="caixa" value=" Alterar "></td>
      </tr>
    </form></table>
  </div>
  <div id="rodape"><img src="../images/rodape.jpg" alt="Desenvolvido por Nova Igua&ccedil;u Digital" width="178" height="41"></div>
</div>
</body>
</html>