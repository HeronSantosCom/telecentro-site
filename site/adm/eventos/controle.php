<?php
// Restabelecendo a sess�o
require('../sessao.php');
$usuarioatual 		= $_SESSION['usuario'];
$nomeatual 			= $_SESSION['nome'];
$nivelatual			= $_SESSION['nivel'];

//Definindo menu
$pagina = 'evento';

//Conectando o banco
include ('../classes/banco.class.php');
$banco = new Banco;
$banco->conectar();

//Executando a consulta no banco de dados
$sql = "SELECT * FROM evento ORDER BY id";
$resultado = mysql_query($sql);
if (!$resultado){
?>
<script language="javascript" type="text/javascript">
alert('Erro no Banco de Dados: N�o foi poss�vel realizar a consulta.');
window.location.href = '../principal.php';
</script>
<?php
exit();
}

$data = date("Y,m,d");
$hora = date("H:i;s");
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Nova Igua&ccedil;u Digital - Administra&ccedil;&atilde;o</title>

<!-- Estilo da P�gina -->
<link href="../css/estilo.css" rel="stylesheet" type="text/css" />

<!-- Validar Cadastro -->
<script language="javascript" type="text/javascript" src="../javascript/script.js"></script>
<script type="text/javascript">
      _editor_url = "htmlarea/";
      _editor_lang = "en";
    </script>

    <!-- load the main HTMLArea files -->
    <script type="text/javascript" src="htmlarea/htmlarea.js"></script>

    <script type="text/javascript">
      HTMLArea.loadPlugin("FullPage");

      function initDocument() {
        var editor = new HTMLArea("texto");
        editor.registerPlugin(FullPage);
        editor.generate();
      }
	  var config = new HTMLArea.Config(); 
          config.width = '670px';
          config.height = '400px';
          config.pageStyle =  'body { font-family: Arial; font-size:11px; }';
    </script>

</head>

<body onLoad="HTMLArea.replace('texto', config);">
<div id="pagina">
  <div id="topo"><img src="../images/topo.jpg" alt="Nova Igua&ccedil;u Digital (Adminitra&ccedil;&atilde;o)" width="777" height="49"></div>
  <div class="menu" id="menu">
    <table width="100%" height="100%" border="0" align="left" cellpadding="2" cellspacing="2">
      <tr>
        <td valign="bottom"><?php include ('../menu.php'); ?></td>
      </tr>
    </table>
  </div>
  <div id="linha"><img src="../usuario/images/linha.jpg" width="1" height="3"></div>
  <div id="id">
    <table width="100%" border="0" cellspacing="0" cellpadding="2">
      <tr>
        <td width="20%" valign="bottom"><span class="txt">Ol&aacute;, <strong><?php echo $nomeatual; ?></strong></span></td>
        <td width="50%" valign="bottom"><span class="txt">N�vel: <strong>
          <?php switch ($nivelatual) { 
																case 1 : echo "Administrador"; break;
																case 2 : echo "Redator"; break;
																case 3 : echo "Usu�rio"; break;
															}
													  ?>
        </strong></span></td>
        <td width="30%" valign="bottom" align="right"><span class="txt">
          <script language="javascript" type="text/javascript" src="../usuario/javascript/data.js"></script>
        </span></td>
      </tr>
    </table>
    <hr color="#E7701A" size="1px">
  </div>
  <div id="cont"><span class="titulo-01">Cadastrar Evento </span>
      <hr color="#E7701A" size="1px">
      <table width="100%" border="0" cellspacing="2" cellpadding="2">
        <form name="evento" method="post" action="inserir.php" onSubmit="return validarEvento()">
          <tr>
            <td width="14%" class="txt"><strong>T&iacute;tulo: </strong></td>
            <td width="86%"><input name="titulo" type="text" class="caixa" id="titulo" size="80" maxlength="100"></td>
          </tr>
          <tr>
            <td valign="top" class="txt"><strong>Subt&iacute;tulo:</strong></td>
            <td><input name="subtitulo" type="text" class="caixa" id="subtitulo" size="100" maxlength="200"></td>
          </tr>
          <tr>
            <td valign="top" class="txt"><strong>Texto:</strong></td>
            <td><textarea name="texto" id="texto"></textarea></td>
          </tr>
          <tr>
            <td class="txt"><strong>Destino:</strong></td>
            <td class="txt">
            	<input name="paginaprincipal" type="checkbox" value="S">Portal Principal
             	<input name="paginacomunidade" type="checkbox" value="S">Portal Comunidade
              	<input name="paginaescola" type="checkbox" value="S">Portal Escola</td>
          </tr>

          <tr>
            <td class="txt"><strong>Publicar:</strong></td>
            <td><input name="disponivel" type="checkbox" id="disponivel" value="S"></td>
          </tr>
          <tr>
            <td class="txt"><input name="autor" type="hidden" value="<?php echo $nomeatual; ?>">
							<input name="data" type="hidden" value="<?php echo $data; ?>">
							<input name="hora" type="hidden" value="<?php echo $hora; ?>">
							</td>
            <td><input name="enviar" type="submit" class="caixa" id="enviar" value="Incluir"></td>
          </tr>
        </form>
      </table>
    <br>
      <span class="titulo-01">Telecentros</span>
      <hr color="#E7701A" size="1px">
      <table width="100%" border="0" cellspacing="2" cellpadding="2">
        <tr>
          <td width="7%" class="caixa">ID</td>
          <td width="22%" class="caixa">Autor</td>
          <td width="14%" class="caixa">Data</td>
          <td width="14%" class="caixa">Hora</td>
          <td width="33%" class="caixa">T&iacute;tulo</td>
          <td width="5%" class="caixa">&nbsp;</td>
          <td width="5%" class="caixa">&nbsp;</td>
        </tr>
        <?php

	//Lendo o banco de dados e separando os valores nas variaveis
	while ($linha = mysql_fetch_array ($resultado)) {
		$id 		= $linha ["id"];
		$autor 		= $linha ["autor"];
		$data		= $linha ["data"];
		$hora		= $linha ["hora"];
		$titulo		= $linha ["titulo"];

		//Formatando vari&aacute;veis para impress&atilde;o
		$titulo 	= stripslashes($titulo);
		$publidata = substr($data,8,2). "/" .substr($data,5,2). "/" . substr($data,0,4);
		$publihora = substr($hora,0,2). "h" .substr($hora,3,2). "min";
	?>
        <tr bgcolor="#f5f5f5">
          <td class="txt"><strong><?php echo $id; ?></strong></td>
          <td class="txt"><?php echo $autor; ?></td>
          <td class="txt"><?php echo $publidata; ?></td>
          <td class="txt"><?php echo $publihora; ?></td>
          <td class="txt"><?php echo $titulo; ?></td>
          <td class="txt"><a href="alterar.php?id=<?php echo $id; ?>"><img src="../images/altera.png" alt="Alterar" width="34" height="31" border="0"></a></td>
          <td class="txt"><a href="excluir.php?id=<?php echo $id; ?>"><img src="../images/delete.png" alt="Excluir" width="34" height="31" border="0"></a></td>
        </tr>
        <?php
  		}
  		$banco->fechar();
  		?>
      </table>
  </div>
  <div id="rodape"><img src="../images/rodape.jpg" alt="Desenvolvido por Nova Igua&ccedil;u Digital" width="178" height="41"></div>
</div>
</body>
</html>