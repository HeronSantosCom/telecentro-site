// Validar Login---------------------------------------------------//
function validarLogin(){
	var login = document.login;
 	if (login.usuario.value == '') {
 		alert('O campo Usu�rio � necess�rio para poder acessar a administra��o!');
		login.usuario.focus();
		return false;
		exit();
 	}
  	if (login.senha.value == '') {
 		alert('O campo Senha � necess�rio para poder acessar a administra��o!');
		login.senha.focus();
		return false;
		exit();
 	}
 	return true;
}

// Validar Cadastro---------------------------------------------------//
function validarCadastro(){
	var cadastro = document.cadastro_usuario;
 	if (cadastro.nome.value == '') {
 		alert('O campo Nome � necess�rio para poder cadastrar-se!');
		cadastro.nome.focus();
		return false;
		exit();
 	}
  	if (cadastro.telefone.value == '') {
 		alert('O campo Telefone � necess�rio para poder cadastrar-se!!');
		cadastro.telefone.focus();
		return false;
		exit();
 	}
	if (cadastro.nivel.value == '') {
 		alert('O campo N�vel � necess�rio para poder cadastrar-se!!');
		cadastro.nivel.focus();
		return false;
		exit();
 	}
	if (cadastro.usuario.value == '') {
 		alert('O campo Usu�rio � necess�rio para poder cadastrar-se!!');
		cadastro.usuario.focus();
		return false;
		exit();
 	}
	if (cadastro.senha.value == '') {
 		alert('O campo Senha � necess�rio para poder cadastrar-se!!');
		cadastro.senha.focus();
		return false;
		exit();
 	}
	if (cadastro.csenha.value == '') {
 		alert('O campo Confirmar Senha � necess�rio para poder cadastrar-se!!');
		cadastro.csenha.focus();
		return false;
		exit();
 	}
	if ((cadastro.senha.value) != (cadastro.csenha.value)) {
 		alert('Os campos Senha e Confimar Senha devem ser iguais!');
		cadastro.senha.focus();
		return false;
		exit();
 	}
 	return true;
}

// Validar Cadastro do Not�cia---------------------------------------------------//
function validarNoticia(){
	var noticia = document.noticia;
 	if (noticia.titulo.value == '') {
 		alert('O campo T�tulo � necess�rio para cadastrar a Not�cia!');
		noticia.titulo.focus();
		return false;
		exit();
 	}
	if (noticia.subtitulo.value == '') {
 		alert('O campo Subt�tulo � necess�rio para cadastrar a Not�cia!');
		noticia.subtitulo.focus();
		return false;
		exit();
 	}
 	if (noticia.texto.value == '') {
 		alert('O campo Texto � necess�rio para cadastrar a Not�cia!');
		noticia.texto.focus();
		return false;
		exit();
 	}
 	return true;
}

// Validar Cadastro do Evento---------------------------------------------------//
function validarEvento(){
	var evento = document.evento;
 	if (evento.titulo.value == '') {
 		alert('O campo T�tulo � necess�rio para cadastrar o Evento!');
		evento.titulo.focus();
		return false;
		exit();
 	}
	if (evento.subtitulo.value == '') {
 		alert('O campo Subt�tulo � necess�rio para cadastrar o Evento!');
		evento.subtitulo.focus();
		return false;
		exit();
 	}
 	if (evento.texto.value == '') {
 		alert('O campo Texto � necess�rio para cadastrar o Evento!');
		evento.texto.focus();
		return false;
		exit();
 	}
 	return true;
}