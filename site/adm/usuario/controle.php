<?php
// Restabelecendo a sess�o
require('../sessao.php');
$usuarioatual 	= $_SESSION['usuario'];
$nomeatual 		= $_SESSION['nome'];
$nivelatual		= $_SESSION['nivel'];

//Definindo menu
$pagina = 'usuario';

//Conectando o banco
include ('../classes/banco.class.php');
$banco = new Banco;
$banco->conectar();

//Executando a consulta no banco de dados
$sql = "SELECT * FROM usuario ORDER BY id";
$resultado = mysql_query ($sql);
if (!$resultado){
?>
<script language="javascript" type="text/javascript">
alert('Erro no Banco de Dados: N�o foi poss�vel realizar a consulta.');
window.location.href = '../principal.php';
</script>
<?php
exit();
}
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Nova Igua&ccedil;u Digital - Administra&ccedil;&atilde;o</title>

<!-- Estilo da P�gina -->
<link href="../css/estilo.css" rel="stylesheet" type="text/css" />
<style type="text/css">
/* Descendentes de cont */
#cont tr:hover {
	background-color:#FFFFCC;	
}
</style>
</head>

<body>
<div id="pagina">
  <div id="topo"><img src="../images/topo.jpg" alt="Nova Igua&ccedil;u Digital (Adminitra&ccedil;&atilde;o)" width="777" height="49"></div>
  <div class="menu" id="menu">
    <table width="100%" height="100%" border="0" align="left" cellpadding="2" cellspacing="2">
      <tr>
        <td valign="bottom"><?php include ('../menu.php'); ?></td>
        </tr>
    </table>
  </div>
  <div id="linha"><img src="images/linha.jpg" width="1" height="3"></div>
  <div id="id">
    <table width="100%" border="0" cellspacing="0" cellpadding="2">
      <tr>
        <td width="20%" valign="bottom"><span class="txt">Ol&aacute;, <strong><?php echo $nomeatual; ?></strong></span></td>
        <td width="50%" valign="bottom"><span class="txt">N�vel: <strong><?php switch ($nivelatual) { 
																case 1 : echo "Administrador"; break;
																case 2 : echo "Redator"; break;
																case 3 : echo "Usu�rio"; break;
															}
													  ?></strong></span></td>
        <td width="30%" valign="bottom" align="right"><span class="txt"><script language="javascript" type="text/javascript" src="javascript/data.js"></script></span></td>
      </tr>
    </table>
    <hr color="#E7701A" size="1px">     
  </div>
  <div id="cont">
  	<span class="titulo-01">Cadastro de Usu�rios </span><hr color="#E7701A" size="1px">
  	<table width="100%"  border="0" cellspacing="2" cellpadding="2">
  	  <form name="cadastro_usuario" method="post" action="inserir.php" onSubmit="return validarCadastro()">
        <input type="hidden" name="altsenha" value="nao">

        <tr class="txt">
          <td width="22%"><span  class="txtb"><strong>Nome: </strong></span></td>
          <td width="78%"><input name="nome" type="text" class="caixa" id="nome" size="50"></td>
        </tr>
        <tr class="txt">
          <td><span  class="txtb"><strong>E-mail:</strong></span></td>
          <td><input name="email" type="text" class="caixa" id="email" size="50"></td>
        </tr>
        <tr class="txt">
          <td><strong>Telefone:</strong></td>
          <td><input name="telefone" type="text" class="caixa" id="telefone" size="15" maxlength="10"></td>
        </tr>
        <tr class="txt">
          <td><strong>N&iacute;vel:</strong></td>
          <td><input type="radio" name="nivel" value="1" >
            Administrador
            <input type="radio" name="nivel" value="2" >
            Redator
            <input type="radio" name="nivel" value="3" >
            Usu&aacute;rio</td>
        </tr>
        <tr class="txt">
          <td><strong>Usu&aacute;rio:</strong></td>
          <td><input name="usuario" type="text" class="caixa" id="usuario" value="" size="20" maxlength="10"></td>
        </tr>
        <tr class="txt">
          <td><strong>Senha:</strong></td>
          <td><input name="senha" type="password" class="caixa" id="senha" value="" size="20" maxlength="10" ></td>
        </tr>
        <tr class="txt">
          <td><strong>Confirmar senha: </strong></td>
          <td><input name="csenha" type="password" class="caixa" id="csenha" value="" size="20" maxlength="10"></td>
        </tr>
        <tr class="txt">
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr class="txt">
          <td colspan="2"><input name="Submit" type="submit" class="caixa" value=" Cadastrar "></td>
        </tr>
      </form>
    </table>
  </div>
  <div id="cont"><span class="titulo-01">Controle de Usu�rios</span><hr color="#E7701A" size="1px">
  
    <table width="100%" border="0" cellspacing="2" cellpadding="2">
      <tr>
        <td width="3%" class="caixa">ID</td>
        <td width="17%" class="caixa">Nome</td>
        <td width="13%" class="caixa">E-mail</td>
        <td width="10%" class="caixa">Telefone</td>
		<td width="11%" class="caixa">Usu&aacute;rio</td>
		<td width="10%" class="caixa">N�vel</td>
		<td width="5%" class="caixa">&nbsp;</td>
		<td width="5%" class="caixa">&nbsp;</td>
      </tr>
      <?php
	//Lendo o banco de dados e separando os valores nas variaveis
	while ($linha = mysql_fetch_array ($resultado))
	{
	$id 		= $linha ["id"];
	$nome 		= $linha ["nome"];
	$email	 	= $linha ["email"];
	$telefone	= $linha ["telefone"];
	$usuario 	= $linha ["usuario"];
	$nivel	 	= $linha ["nivel"];

	//Formatando vari&aacute;veis para impress&atilde;o
	$nome 		= stripslashes($nome);
	$email	 	= stripslashes($email);
	$telefone 	= stripslashes($telefone);
	$usuario 	= stripslashes($usuario);
	?>
      <tr bgcolor="#f5f5f5">
        <td class="txt"><strong><?php echo $id; ?></strong></td>
        <td class="txt"><?php echo $nome; ?></td>
        <td class="txt"><?php echo $email; ?></td>
        <td class="txt"><?php echo $telefone; ?></td>
		<td class="txt"><?php echo $usuario; ?></td>
		<td class="txt"><?php switch ($nivel) { 
									case 1 : echo "Administrador"; break;
									case 2 : echo "Redator"; break;
									case 3 : echo "Usu�rio"; break;
								}			
						?></td>
		<td class="txt"><a href="alterar.php?id=<?php echo $id; ?>"><img src="../images/altera.png" alt="Alterar" border="0"></a></td>
		<td class="txt"><a href="excluir.php?id=<?php echo $id; ?>"><img src="../images/delete.png" alt="Excluir" width="34" height="31" border="0"></a></td>
      </tr>
      <?php
  }
  $banco->fechar();
  ?>
    </table>
  </div>
  <div id="rodape"><img src="../images/rodape.jpg" alt="Desenvolvido por Nova Igua&ccedil;u Digital" width="178" height="41"></div>
</form></div>
</body>
</html>