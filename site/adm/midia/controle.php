<?php
// Restabelecendo a sess�o
require('../sessao.php');
$usuarioatual 		= $_SESSION['usuario'];
$nomeatual 			= $_SESSION['nome'];
$nivelatual			= $_SESSION['nivel'];

//Definindo menu
$pagina = 'midia';

//Conectando o banco
include ('../classes/banco.class.php');
$banco = new Banco;
$banco->conectar();

//Executando a consulta no banco de dados
if ($nivelatual == 3) {
	$sql = "SELECT * FROM noticia WHERE autor = '$nomeatual' ORDER BY id";
} else {
	$sql = "SELECT * FROM noticia ORDER BY id";
}
$resultado = mysql_query($sql);
if (!$resultado){
?>
<script language="javascript" type="text/javascript">
alert('Erro no Banco de Dados: N�o foi poss�vel realizar a consulta.');
window.location.href = '../principal.php';
</script>
<?php
exit();
}

$data = date("Y,m,d");
$hora = date("H:i;s");

$banco->fechar();
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Nova Igua&ccedil;u Digital - Administra&ccedil;&atilde;o</title>

<!-- Estilo da P�gina -->
<link href="../css/estilo.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="pagina">
  <div id="topo"><img src="../images/topo.jpg" alt="Nova Igua&ccedil;u Digital (Adminitra&ccedil;&atilde;o)" width="777" height="49"></div>
  <div class="menu" id="menu">
    <table width="100%" height="100%" border="0" align="left" cellpadding="2" cellspacing="2">
      <tr>
        <td valign="bottom"><?php include ('../menu.php'); ?></td>
      </tr>
    </table>
  </div>
  <div id="linha"><img src="../usuario/images/linha.jpg" width="1" height="3"></div>
  <div id="id">
    <table width="100%" border="0" cellspacing="0" cellpadding="2">
      <tr>
        <td width="20%" valign="bottom"><span class="txt">Ol&aacute;, <strong><?php echo $nomeatual; ?></strong></span></td>
        <td width="50%" valign="bottom"><span class="txt">N�vel: <strong>
          <?php switch ($nivelatual) { 
																case 1 : echo "Administrador"; break;
																case 2 : echo "Redator"; break;
																case 3 : echo "Usu�rio"; break;
															}
													  ?>
        </strong></span></td>
        <td width="30%" valign="bottom" align="right"><span class="txt">
          <script language="javascript" type="text/javascript" src="../usuario/javascript/data.js"></script>
        </span></td>
      </tr>
    </table>
    <hr color="#E7701A" size="1px">
  </div>
  <div id="cont"><span class="titulo-01">Cadastrar M&iacute;dia </span>
      <hr color="#E7701A" size="1px">
      <table width="100%" border="0" cellspacing="2" cellpadding="2">
        <form name="midia" method="post" action="inserir.php" enctype="multipart/form-data">
          <tr>
            <td width="14%" class="txt"><strong>Foto: </strong></td>
            <td width="86%"><input name="foto" type="file" class="caixa" id="foto">
            <input name="enviar" type="submit" class="caixa" id="enviar" value="Enviar"></td>
          </tr>
        </form>
      </table>
    <br>
      <span class="titulo-01">M�dias</span>
      <hr color="#E7701A" size="1px">
      <table width="100%" cellpadding="0" cellpadding="2" border="0">
		<?php
		$contador = 0;
	  	$dir = opendir('imagens');
        while ($arquivo = readdir($dir))
        {
            if ($contador%7 == 0)
			{
				?>
				<tr align="center">
				<?php
			}
            if (($arquivo != '.') && ($arquivo != '..'))
            {
                ?>
                <td>
                	<img src="imagens/<?php echo $arquivo; ?>" width="100"/><br />
                    <span class="txt"><?php echo $arquivo; ?></span><br />
                    <a href="excluir.php?imagem=<?php echo $arquivo; ?>"><img src="../images/excluir.png" width="12" height="12" border="0" /></a>
                </td>
                <?php
            }
			if ($contador == 6)
			{
				?>
                </tr>
                <?php
                $contador = 0;
            }
            else
                $contador++;
		}
        closedir($dir);
		?>
		</table>
  </div>
  <div id="rodape"><img src="../images/rodape.jpg" alt="Desenvolvido por Nova Igua&ccedil;u Digital" width="178" height="41"></div>
</div>
</body>
</html>