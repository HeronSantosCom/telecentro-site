<?php
// Restabelecendo a sess�o
require('sessao.php');
$usuarioatual 	= $_SESSION['usuario'];
$nomeatual 		= $_SESSION['nome'];
$nivelatual		= $_SESSION['nivel'];

//Definindo menu
$pagina = 'principal';

//Conectando o banco
include ('classes/banco.class.php');
$banco = new Banco;
$banco->conectar();
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Nova Igua&ccedil;u Digital - Administra&ccedil;&atilde;o</title>

<!-- Estilo da P�gina -->
<link href="css/estilo.css" rel="stylesheet" type="text/css" />
<script type="text/JavaScript">
<!--
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
</head>

<body onLoad="MM_preloadImages('images/admnoticia-rol.jpg','images/admusuario-rol.jpg','images/admevento-rol.jpg','images/admbairro-rol.jpg','images/admmidia-rol.jpg')">
<div id="pagina">
  <div id="topo"><img src="images/topo.jpg" alt="Nova Igua&ccedil;u Digital (Adminitra&ccedil;&atilde;o)" width="777" height="49"></div>
  <div class="menu" id="menu">
    <table width="100%" height="100%" border="0" align="left" cellpadding="2" cellspacing="2">
      <tr>
        <td valign="bottom"><?php include ('menu.php'); ?></td>
      </tr>
    </table>
  </div>
  <div id="linha"><img src="images/linha.jpg" width="1" height="3"></div>
  <div id="id">
    <table width="100%" border="0" cellspacing="0" cellpadding="2">
      <tr>
        <td width="20%" valign="bottom"><span class="txt">Ol&aacute;, <strong><?php echo $nomeatual; ?></strong></span></td>
        <td width="50%" valign="bottom"><span class="txt">N�vel: <strong>
          <?php switch ($nivelatual) { 
																case 1 : echo "Administrador"; break;
																case 2 : echo "Redator"; break;
																case 3 : echo "Usu�rio"; break;
															}
													  ?>
        </strong></span></td>
        <td width="30%" valign="bottom" align="right"><span class="txt">
          <script language="javascript" type="text/javascript" src="javascript/data.js"></script>
        </span></td>
      </tr>
    </table>
    <hr color="#E7701A" size="1px">
  </div>
  <div id="cont">
    <table width="100%" border="0" cellspacing="2" cellpadding="2">
      <tr>
        <?php if ($nivelatual == 1) { ?><td width="14%">
          <a href="usuario/controle.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image5','','images/admusuario-rol.jpg',1)"><img src="images/admusuario.jpg" alt="Administrar Usu�rios" name="Image5" width="100" height="106" border="0"></a>
         </td><?php } ?> 
        <?php if ($nivelatual != 3) { ?><td width="14%"><a href="eventos/controle.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image7','','images/admevento-rol.jpg',1)"><img src="images/admevento.jpg" alt="Administrar Eventos" name="Image7" width="100" height="106" border="0"></a></td>
        <td width="14%"><a href="noticia/controle.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image4','','images/admnoticia-rol.jpg',1)"><img src="images/admnoticia.jpg" alt="Administrar Not�cias" name="Image4" width="100" height="106" border="0"></a></td>
        <?php } ?>         
        <td width="58%"><a href="midia/controle.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Midia','','images/admmidia-rol.jpg',1)"><img src="images/admmidia.jpg" alt="Administrar M�dia" name="Midia" width="100" height="106" border="0"></a></td>
      </tr>
    </table>
  </div>
  <div id="rodape"><img src="images/rodape.jpg" alt="Desenvolvido por Nova Igua&ccedil;u Digital" width="178" height="41"></div>
</div>
</body>
</html>