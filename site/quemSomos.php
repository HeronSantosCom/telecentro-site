<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Telecentro Nova Igua&ccedil;u</title>

<!-- Estilo da P�gina -->
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<style type="text/css">
body {
	margin:0px;
	text-align:center;
}
</style>

<!-- Validar Login -->
<script language="javascript" type="text/javascript" src="javascript/script.js"></script>
</head>

<body>
<div id="pagina">
	<div id="topo">
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="77%"><img src="images/topoprincipal.jpg" alt="Telecentro Nova Igua&ccedil;u" width="602" height="96"></td>
          <td width="23%" background="images/topoprincipal2.jpg">&nbsp;</td>
        </tr>
      </table>
	</div>
	<div id="esquerdo">
	  <?php include ('menu.php'); ?>
	  
      <img src="images/linhamenu.jpg" width="178" height="3"><br>
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td bgcolor="#f5f5f5"><div align="center" class="titulo1">Acesso</div></td>
        </tr>
        <tr>
          <td bgcolor="#f5f5f5"><div align="center">
            <table width="100%" height="100%" border="0" cellspacing="2" cellpadding="2" class="txt">
              <form name="login" method="post" action="adm/login.php" onSubmit="javascript:return validarLogin()">
                <tr>
                  <td width="34%"><div align="right">Usu&aacute;rio:</div></td>
                  <td width="66%"><input name="usuario" type="text" class="caixa" id="usuario" size="15" maxlength="15">
                  </td>
                </tr>
                <tr>
                  <td><div align="right">Senha:</div></td>
                  <td><input name="senha" type="password" class="caixa" id="senha" size="15" maxlength="15"></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td><input name="acessar" type="submit" class="caixa" id="acessar" value=" Acessar "></td>
                </tr>
              </form>
            </table>
            </div></td>
        </tr>
      </table>
  </div>
	<div id="centro">
		 <div id="apresentacao">
		   <div class="data"><script language="javascript" type="text/javascript" src="javascript/data.js"></script></div><hr size="1px" color="#CCCCCC">
	  </div>
	  <div id="conteudo">
<p class="titulo1">Quem somos?</p>
<p class="txt">Um Telecentro � um lugar que disponibiliza Tecnologia com  o objetivo de estimular e responder � demanda por servi�os de informa��o e  comunica��o (TIC). Sua proposta vem da ONU que criou uma For�a-Tarefa com um  fundo de investimentos em tecnologias de informa��o e comunica��o, como forma  de &quot;amenizar as disparidades e universalizar o acesso aos meios de  comunica��o, superando o abismo tecnol�gico que separa ricos e pobres no mundo  inteiro&quot;.</p>
<p class="txt">O Projeto de Telecentro da Cidade  de Nova Igua�u no Estado do Rio de Janeiro n�o foge a esta proposta quando  estabelece como sua miss�o realizar a Inclus�o Digital-Social no Munic�pio de  Nova Igua�u. </p>
<p><span class="txt">Os objetivos que ele pretende alcan�ar na cidade s�o: </span>
<li class="txt">Extinguir a exclus�o digital em Nova Igua�u;</li>
  <li class="txt">Estimular o desenvolvimento econ�mico;</li>
  <li class="txt">Promover a melhoria da qualidade de vida da popula��o do Munic�pio.</li>

  <span class="txt">
  </p>
  </span>
  <p><span class="txt">O projeto apresenta um Plano de A��o que estabelece como suas principais metas: </span>
  <li class="txt">A aplica��o do processo de Ambienta��o Digital;</li>
  <li class="txt">A capacita��o de pessoas para o uso do computador;</li>
  <li class="txt">O est�mulo a Apropria��o Digital;</li>
  <li class="txt">Criar meios que permitam que as pessoas capacitadas pelo projeto usem a tecnologia para a melhoria de sua qualidade de vida.</li>

  <span class="txt">
  </p>
  </span>
  <p><span class="txt">As contribui��es do projeto para  as pessoas por ele atendidas s�o: </span>
  <li class="txt">Realiza��o da Inclus�o Digital e social para o mundo do trabalho;</li>
  <li class="txt">Fortalecimento da auto-estima, conv�vio social e est�mulo para aprendizagem;</li>
  <li class="txt">Estimulo do exerc�cio da cidadania atrav�s de acesso ao servi�os p�blicos;</li>
  <li class="txt">Inser��o das classes sociais que vivem � margem da sociedade;</li>
  <li class="txt">Democratiza��o de informa��es;</li>
  <li class="txt">Visibilidade de servi�os sociais </li>

  <span class="txt">
  </p>
  </span>
  <p><span class="txt">Metas:
  </span>
  <li class="txt">Inaugurar 115 Telecentros</li>
  <li class="txt">Oferecer  capacita��o para 50.000 cidad�os da Cidade de Nova Igua�u</li>
  <li class="txt">Disponibilizar Laborat�rios de Inform�tica em 70 escolas da Rede Municipal de Educa��o </li>
  <li class="txt">Capacitar professores do Munic�pio </li>
  <li class="txt">Capacitar profissionais da Rede Municipal de Educa��o</li>
  <li class="txt">Capacitar�jovens do munic�pio</li>
  <li class="txt">Atender a representa��es comunit�rias </li>

</p>
</div>
  </div>
	<div id="direito">
	  <table width="100%"  border="0" cellspacing="2" cellpadding="2">
        <tr>
          <th scope="row"><a href="http://www.novaiguacu.rj.gov.br" target="_blank"><img src="images/Logo-PMNI-Branco.gif" alt="PMNI" width="100" height="88" border="0" /></a></th>
        </tr>
        <tr>
          <th scope="row"><img src="images/softwarelivre.jpg" alt="Software Livre" width="150" height="76" /></th>
        </tr>
      </table>
	</div>
	 	<div id="logos">
		  <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="20%"><div align="center"><a href="http://www.novaiguacu.rj.gov.br" target="_blank"><img src="images/brasao.jpg" alt="Bras&atilde;o" width="57" height="65" border="0"></a></div></td>
              <td width="20%"><div align="center"><a href="http://www.novaiguacu.rj.gov.br" target="_blank"></a><a href="http://www.abrasol.org.br" target="_blank"><img src="images/logo_abrasol.gif" alt="Abrasol" width="100" height="26" border="0"></a></div></td>
              <td width="20%"><div align="center"><a href="http://www.cic.org.br" target="_blank"><img src="images/Logo_-_CIC.jpg" alt="CIC" width="100" height="38" border="0"></a></div></td>
              <td width="20%"><div align="center"><a href="http://www.bairroescola.com.br" target="_blank" title="Bairro Escola"><img src="images/bairroescola.jpg" alt="Bairro Escola" width="100" height="46" border="0"></a></div></td>
              <td width="20%"><div align="center"><a href="http://www.hostnet.com.br" target="_blank"><img src="images/logo_hostnet_novo.jpg" alt="Host Net" width="100" height="66" border="0"></a></div></td>
            </tr>
          </table>
	</div>
	<div id="rodape"><img src="images/rodape.jpg" alt="Desenvolvido por Nova Igua&ccedil;u Digital" width="183" height="40"></div>
</div>
</body>
</html>
