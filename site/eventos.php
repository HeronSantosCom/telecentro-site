<?php
//Conectando o banco
include ('adm/classes/banco.class.php');
$banco = new Banco;
$banco->conectar();

$categoria = $_GET['g_categoria'];

// Consultando ao Banco de Dados
$sql 		= "SELECT * FROM evento WHERE disponivel = 'S' and pagina".$categoria."='S' ORDER BY id DESC";
$resultado 	= mysql_query ($sql);

// Rediecionando caso consulta ao banco n�o seja realizada
if (!$resultado) {
	?>
	<script language="javascript" type="text/javascript">
	alert ('Consulta n�o realizada!');
	</script>
	<?php
	exit();
}
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Telecentro Nova Igua&ccedil;u</title>

<!-- Estilo da P�gina -->
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<style type="text/css">
body {
	margin:0px;
	text-align:center;
}
</style>

<!-- Validar Login -->
<script language="javascript" type="text/javascript" src="javascript/script.js"></script>
</head>

<body>
<div id="pagina">
	<div id="topo">
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="77%"><img src="images/topoprincipal.jpg" alt="Telecentro Nova Igua&ccedil;u" width="602" height="96"></td>
          <td width="23%" background="images/topoprincipal2.jpg">&nbsp;</td>
        </tr>
      </table>
	</div>
	<div id="esquerdo">
	  <?php include ('menu.php'); ?>
	  
      <img src="images/linhamenu.jpg" width="178" height="3"><br>
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td bgcolor="#f5f5f5"><div align="center" class="titulo1">Acesso</div></td>
        </tr>
        <tr>
          <td bgcolor="#f5f5f5"><div align="center">
            <table width="100%" height="100%" border="0" cellspacing="2" cellpadding="2" class="txt">
              <form name="login" method="post" action="adm/login.php" onSubmit="javascript:return validarLogin()">
                <tr>
                  <td width="34%"><div align="right">Usu&aacute;rio:</div></td>
                  <td width="66%"><input name="usuario" type="text" class="caixa" id="usuario" size="15" maxlength="15">
                  </td>
                </tr>
                <tr>
                  <td><div align="right">Senha:</div></td>
                  <td><input name="senha" type="password" class="caixa" id="senha" size="15" maxlength="15"></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td><input name="acessar" type="submit" class="caixa" id="acessar" value=" Acessar "></td>
                </tr>
              </form>
            </table>
            </div></td>
        </tr>
      </table>
  </div>
	<div id="centro">
		 <div id="apresentacao">
		   <div class="data"><script language="javascript" type="text/javascript" src="javascript/data.js"></script></div><hr size="1px" color="#CCCCCC">
	  </div>
	  <div id="conteudo">
        <?php
		//Verificando se existe not�cia
		$nroreg = mysql_num_rows ($resultado);
		if ($nroreg == 0) {
			?><span class="titulo1">Nenhum evento a exibir.</span><?php
		} else {
			?><span class="titulo1">Eventos do Portal</span><br /><br /><?php
		}
		
		// Transformando o resultado da consulta em um vetor e separando-os em vari�veis
		while ($registro = mysql_fetch_array ($resultado)) {
			$id			= $registro['id'];
			$data 		= $registro['data'];
			$titulo 	= $registro['titulo'];
			$subtitulo 	= $registro['subtitulo'];
			
			// Formatando as vari�veis para impress�o
			$titulo = stripslashes ($titulo);
			$subtitulo = stripslashes ($subtitulo);
			$data = substr ($data, 8, 2).'/'.substr ($data, 5, 2).'/'. substr ($data, 0, 4);
			
			//Imprimindo as not�cias
			?><span class="titulo2"><?php echo $titulo; ?></span><br />
			<span class="txt2"><?php echo $data; ?></span><br />
			<span class="txt"><?php echo $subtitulo; ?></span><br />
            <a href="evento-individual.php?g_id=<?php echo $id; ?>" class="int">leia mais</a><br />
            <br /><?php
		}
		$banco->fechar();
		?>
        </div>
	</div>
	<div id="direito">
	  <table width="100%"  border="0" cellspacing="2" cellpadding="2">
        <tr>
          <th scope="row"><a href="http://www.novaiguacu.rj.gov.br" target="_blank"><img src="images/Logo-PMNI-Branco.gif" alt="PMNI" width="100" height="88" border="0" /></a></th>
        </tr>
        <tr>
          <th scope="row"><img src="images/softwarelivre.jpg" alt="Software Livre" width="150" height="76" /></th>
        </tr>
      </table>
	</div>
	 	<div id="logos">
		  <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="20%"><div align="center"><a href="http://www.novaiguacu.rj.gov.br" target="_blank"><img src="images/brasao.jpg" alt="Bras&atilde;o" width="77" height="88" border="0"></a></div></td>
              <td width="20%"><div align="center"><a href="http://www.novaiguacu.rj.gov.br" target="_blank"></a><a href="http://www.abrasol.org.br" target="_blank"><img src="images/logo_abrasol.gif" alt="Abrasol" width="100" height="26" border="0"></a></div></td>
              <td width="20%"><div align="center"><a href="http://www.cic.org.br" target="_blank"><img src="images/Logo_-_CIC.jpg" alt="CIC" width="100" height="38" border="0"></a></div></td>
              <td width="20%"><div align="center"><a href="http://www.bairroescola.com.br" target="_blank" title="Bairro Escola"><img src="images/bairroescola.jpg" alt="Bairro Escola" width="100" height="46" border="0"></a></div></td>
              <td width="20%"><div align="center"><a href="http://www.hostnet.com.br" target="_blank"><img src="images/logo_hostnet_novo.jpg" alt="Host Net" width="100" height="66" border="0"></a></div></td>
            </tr>
          </table>
	</div>
	<div id="rodape"><img src="images/rodape.jpg" alt="Desenvolvido por Nova Igua&ccedil;u Digital" width="183" height="40"></div>
</div>
</body>
</html>
