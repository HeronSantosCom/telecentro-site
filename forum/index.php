<?php require_once("./configurations.php"); ?>
<?php 
	function microtime_float(){
		if (version_compare(phpversion(), '5.0.0', '>=')){
			return microtime(true);
		} else {
			list($usec, $sec) = explode(' ', microtime());
			return ((float) $usec + (float) $sec);
		}
	}
	$start = microtime_float();

	header("Cache-Control: no-cache, must-revalidate");
	header("Content-Type: text/html; charset=iso-8859-1",true);
	header("Pragma: no-cache");
?>
<?php include("./kernel/php-class/string.php"); ?>
<?php include("./kernel/php-function/redirect.php"); ?>
<?php include("./kernel/php-function/load_module.php"); ?>
<?php include("./kernel/php-function/get_date.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!--<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />-->
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title><?php echo $head_title ?></title>
<!-- descritores do site -->
	<meta name="description" content="<?php echo $head_description ?>" />
	<meta name="keywords" content="<?php echo $head_keyword ?>" />
	<meta name="author" content="<?php echo $head_webmaster ?>"/>
	<meta name="copyright" content="<?php echo $head_copyright ?>">
	<meta name="robots" content="index,follow">
	<meta name="revisit-after" content="1 days">
	<meta name="resource-type" content="document">
	<meta name="distribution" content="global">
	<meta name="rating" content="general">
	<meta http-equiv="Expires" content="0">
<!-- links relativos -->
	<style>
		@import "./kernel/style/general.css";
		@import "./kernel/style/index.css";
	</style>
<!-- javascrips -->
	<script type="text/javascript" src="kernel/jscript/fix_netscape.js"></script>
	<script type="text/javascript" src="kernel/jscript/redirect.php"></script>
</head>

<body>
<div id="background">
	<table cellspacing="0" id="container">
		<tr>
			<td class="container">
				<!-- inicio do topo -->
					<table cellspacing="0" id="topo">
						<tr>
							<td class="logotipo"><img src="kernel/image/topo.logotipo.gif" /></td>
							<td class="data"><img src="kernel/image/topo.data.gif" /></td>
						</tr>
					</table>
				<!-- fim do topo -->

				<!-- inicio do corpo -->
					<map name="menu" id="menu">
						<area shape="rect" coords="32,22,97,42" href="?" />
						<area shape="rect" coords="54,46,138,66" href="?p=abertura" />
						<area shape="rect" coords="54,66,156,86" href="?p=inscricao" />
						<area shape="rect" coords="54,86,172,106" href="?p=programacao" />
						<area shape="rect" coords="54,106,166,126" href="?p=documentos" />
						<area shape="rect" coords="54,126,154,146" href="?p=localizacao" />
						<area shape="rect" coords="54,146,130,166" href="?p=contato" />
					</map>
					<table cellspacing="0" id="corpo">
						<tr>
							<td rowspan="2" class="menu"><img src="kernel/image/corpo.menu.gif" border="0" usemap="#menu" /></td>
							<td class="titulo"><img src="kernel/image/corpo.titulo.<?php echo $module_page ?>.gif" /></td>
						</tr>
						<tr>
							<td class="corpo"><?php require_once($opener); ?></td>
						</tr>
						<tr>
							<td colspan="2" class="processamento">processado em <?php echo round(microtime_float()-$start, 3); ?> segundos</td>
						</tr>
					</table>
				<!-- fim do corpo -->
								
				<!-- inicio do rodape -->
					<table cellspacing="0" id="rodape">
						<tr>
							<td class="parceiros">
                                <p><img src="kernel/image/fundo.titulo.organizacao.gif" /></p>
                                <p>
                                    <img src="kernel/image/fundo.orgranizacao.semdes.gif" /><br />
                                </p>
                                <p><img src="kernel/image/fundo.titulo.realizacao.gif" /></p>
                                <p>
                                    <img src="kernel/image/fundo.realizacao.prefeitura.gif" />
                                    <img src="kernel/image/fundo.realizacao.bairroescola.gif" />
                                    <img src="kernel/image/fundo.realizacao.abrasol.gif" />
                                    <img src="kernel/image/fundo.realizacao.estacio.gif" /><br />
                                </p>
                                <p><img src="kernel/image/fundo.titulo.apoio.gif" /></p>
                                <p>
                                    <img src="kernel/image/fundo.apoio.serpro.gif" />
                                    <img src="kernel/image/fundo.apoio.assisdata.gif" />
                                    <img src="kernel/image/fundo.apoio.taho.gif" />
                                    <img src="kernel/image/fundo.apoio.hostnet.gif" />
                                    <img src="kernel/image/fundo.apoio.casabrasil.gif" />
                                    <img src="kernel/image/fundo.apoio.governofederal.gif" /><br />
                                </p>
                            </td>
							<td class="fundo"><img src="kernel/image/fundo.background.gif" /></td>
						</tr>
					</table>
				<!-- fim do rodape -->
			</td>
		</tr>
	</table>
</div>
</body>
</html>
