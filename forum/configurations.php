<?php
	header("Cache-Control: no-cache, must-revalidate");
	header("Pragma: no-cache");
	session_start();
	
	$system_name    = "1&deg; F&oacute;rum de Inclus&atilde;o Digital da Baixada Fluminense"; // nome do sistema
	$system_sign    = "FID BF"; // sigla do sistema
	$system_version = "1"; // vers�o do sistema
	
	$head_title = $system_name;
	$head_description = "1� F�rum de Inclus�o Digital da Baixada Fluminense";
	$head_keyword = "1� F�rum de Inclus�o Digital da Baixada Fluminense";
	$head_copyright = "Copyright 2008 $system_name";
	$head_webmaster = "forum@telecentronovaiguacu.org.br";
 
	$patch_root   = "/projetos/fid"; // pasta do projeto
	$patch_kernel = $patch_root."/kernel"; // pasta principal do projeto

	require("kernel/php-class/mysql.php");
	$mysql = new MySQL;

	require("kernel/php-class/cryptographic.php");
	$cripto = new CripTo;
?>