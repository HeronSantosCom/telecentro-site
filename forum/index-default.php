<p>O 1� F�rum para Inclus�o Digital, que acontecer� em Nova Igua�u de 24 a 27 de mar�o de 2008, organizado pela Associa��o Brasileira de Software Livre (ABRASOL), pela Prefeitura da Cidade de Nova Igua�u e pela Universidade Est�cio de S� em conjunto com entidades da sociedade civil e parceiros regionais.<br />
	<br />
</p>
<p>Espelhado na 6� Oficina para Inclus�o Digital, realizado em Salvador pelo Governo Federal, o F�rum de Inclus�o Digital reafirma seu compromisso com profissionais que atuam na ponta dos projetos de inclus�o digital atendendo a popula��o em todas as regi�es do pa�s.<br />
	<br />
</p>
<h3>Proposta<br />
	<br />
</h3>
<p> A proposta do F&oacute;rum  aproximar gestores p&uacute;blicos, educadores, implementadores, t&eacute;cnicos, monitores e estudiosos dos grandes temas da atualidade que envolve a&ccedil;&otilde;es de inclus&atilde;o digital.<br />
	<br />
</p>
<p>O F&oacute;rum ir&aacute; abordar os seguintes temas:<br />
	<br />
</p>
<ul>
	<li><em> Infra-estrutura,  cidades  digitais  e  redes locais;</em></li>
	<li><em> TICs nas escolas;</em></li>
	<li><em> Conte&uacute;dos locais (produ&ccedil;&atilde;o e difus&atilde;o);</em></li>
	<li><em> Economia  solid&aacute;ria  e  arranjos  produtivos locais;</em></li>
	<li><em> Governan&ccedil;a da Internet;</em></li>
	<li><em> Pol&iacute;ticas p&uacute;blicas de Inclus&atilde;o Digital.</em><br />
		<br />
	</li>
</ul>
<h3>Democratiza&ccedil;&atilde;o da Comunica&ccedil;&atilde;o e Dissemina&ccedil;&atilde;o das Experi&ecirc;ncias<br />
	<br />
</h3>
<p>O F&oacute;rum est&aacute; aberto para cobertura da imprensa em geral,  representado  por  r&aacute;dio,  TV,  ve&iacute;culos, impressos, grandes portais e tamb&eacute;m para as m&iacute;dias independentes, como r&aacute;dios comunit&aacute;ria, jornais e tamb&eacute;m portais sociais blogs, fotologs e videologs. Haver&aacute; uma sala de suporte a imprensa no local.<br />
	<br />
</p>
<p> Toda &aacute;era reservada para o evento ser&aacute; coberta por internet sem fio. Computadores com acesso Internet tamb&eacute;m estar&aacute; dispon&iacute;vel durante todo o per&iacute;odo para os participantes.</p>
