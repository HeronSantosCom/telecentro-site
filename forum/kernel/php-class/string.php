<?php
/*
	Classe/Função: Manipulador de String
	Modificado: 06/02/2008
	Autor: Heron Santos by TelosOnline.info
*/
/*
	$string = new string;
	$string->get_lower("VALOR");
	$string->get_upper("VALOR");
	$string->get_clear("VALOR");
*/
/* verificador de seguança */
$checkurl = $_SERVER["PHP_SELF"];
if (eregi("string.php", "$checkurl")) {
	header ("Location: ../../index.php");
}
class string {
	/* construtor */
	function __construct() {
    }
	
	/* retorna valores em minusculo */
	function get_lower($value){
		$retorno = strtolower($value);
		return $retorno;
	}
	
	/* retorna valores em maiusculo */
	function get_upper($value){
		$retorno = strtoupper($value);
		return $retorno;
	}
	
	/* limpa texto */
	function get_clear($value){
		$retorno = strip_tags($value);
		$retorno = str_replace('Leia mais', '', $retorno);
		$retorno = str_replace('Leia o artigo completo', '', $retorno);
		$retorno = str_replace('Veja mais', '', $retorno);
		$retorno = str_replace('Leia Mais', '', $retorno);
		$retorno = str_replace("\"", "&quot;", $retorno);
		$retorno = str_replace("'", "&#39;", $retorno);
		$retorno = str_replace(chr(34), "&quot;", $retorno);
		$retorno = str_replace(chr(39), "&#39;", $retorno);
		$retorno = str_replace(chr(96), "&#96;", $retorno);
		$retorno = str_replace("[", '(', $retorno);
		$retorno = str_replace("]", ')', $retorno);
		$retorno = str_replace("\n", '', $retorno);
		$retorno = str_replace(chr(10), '', $retorno);
		$retorno = str_replace(chr(13), '', $retorno);
		$retorno = str_replace('&nbsp;', ' ', $retorno);
		$retorno = str_replace('  ', '', $retorno);
		return trim($retorno);
	}

	/* limpa url */
	function get_clear_url($value){
		$retorno = strip_tags($value);
		$retorno = str_replace("\"", "&quot;", $retorno);
		$retorno = str_replace("\n", '', $retorno);
		$retorno = str_replace(chr(10), '', $retorno);
		$retorno = str_replace(chr(13), '', $retorno);
		$retorno = str_replace('&nbsp;', ' ', $retorno);
		$retorno = str_replace('  ', '', $retorno);
		return trim($retorno);
	}
}
?>