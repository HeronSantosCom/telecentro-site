<?php
/*
	Classe/Função: Criptografia de dados
	Modificado: 11/11/2007
	Autor: Heron Santos by TelosOnline.info
*/
/*
	$cripto = new cripto;
	$cripto->hide("VALOR");
	$cripto->unveil("VALOR");
*/
/* verificador de seguança */
$checkurl = $_SERVER["PHP_SELF"];
if (eregi("cryptographic.php", "$checkurl")) {
	header ("Location: ../../index.php");
}
class CripTo {
	/* construtor */
	function __construct() {
    }
	
	/* criptografa */
	function hide($value) {
		return base64_encode($value);
	}
	
	/* descriptografa */
	function unveil($value) {
		return base64_decode($value);
	}
}
?>