<?php 
	// carrega configura��o
	require("include.configuracao.php");
	require("include.funcao.php");

	// faz o backup no banco de dados
	// ===============================
	$ArquivoTar = "$PastaInstalada/backup/bkp_". date("Ymd") .".zip";

	$diretorio = "backup/". date("Ymd");
	if (!is_dir($diretorio)) {
		// faz a limpeza no banco de dados
		// ===============================
		// remove caso o telecentro n�o exista
		$delete = mysql_query("DELETE FROM usuario WHERE NOT(codigo_telecentro IN(SELECT tlc.codigo FROM telecentro tlc))");
		$delete = mysql_query("DELETE FROM pesquisa_opiniao WHERE NOT(codigo_telecentro IN(SELECT tlc.codigo FROM telecentro tlc))");
		$delete = mysql_query("DELETE FROM pesquisa_resposta WHERE NOT(codigo_telecentro IN(SELECT tlc.codigo FROM telecentro tlc))");
		$delete = mysql_query("DELETE FROM turma WHERE NOT(codigo_telecentro IN(SELECT tlc.codigo FROM telecentro tlc))");
		
		// remove caso o usu�rio n�o exista
		$delete = mysql_query("DELETE FROM usuario_conectividade WHERE NOT(matricula_usuario IN(SELECT usu.matricula FROM usuario usu))");
		$delete = mysql_query("DELETE FROM usuario_deficiencia WHERE NOT(matricula_usuario IN(SELECT usu.matricula FROM usuario usu))");
		$delete = mysql_query("DELETE FROM usuario_programagoverno WHERE NOT(matricula_usuario IN(SELECT usu.matricula FROM usuario usu))");
		$delete = mysql_query("DELETE FROM turma_usuario WHERE NOT(matricula_usuario IN(SELECT usu.matricula FROM usuario usu))");
		$delete = mysql_query("DELETE FROM turma_usuario WHERE NOT(matricula_usuario IN(SELECT usu.matricula FROM usuario usu))");
		$delete = mysql_query("DELETE FROM diariodeclasse_presenca WHERE NOT(matricula_usuario IN(SELECT usu.matricula FROM usuario usu))");
		$delete = mysql_query("DELETE FROM pesquisa_opiniao WHERE NOT(matricula_usuario IN(SELECT usu.matricula FROM usuario usu))");
		$delete = mysql_query("DELETE FROM pesquisa_resposta WHERE NOT(matricula_usuario IN(SELECT usu.matricula FROM usuario usu))");
		
		// exclua caso n�o exista turma
		$delete = mysql_query("DELETE FROM diariodeclasse_presenca WHERE NOT(codigo_turma IN(SELECT tma.codigo FROM turma tma))");
		$delete = mysql_query("DELETE FROM diariodeclasse_observacao WHERE NOT(codigo_turma IN(SELECT tma.codigo FROM turma tma))");
		$delete = mysql_query("DELETE FROM turma_usuario WHERE NOT(codigo_turma IN(SELECT tma.codigo FROM turma tma))");
		$delete = mysql_query("DELETE FROM turma_grade WHERE NOT(codigo_turma IN(SELECT tma.codigo FROM turma tma))");

		// cria diretorio
		mkdir($diretorio);
		
		// Pega a lista de todas as tabelas
		$sql = mysql_list_tables("siget") or die(mysql_error());
		while ($row = mysql_fetch_row($sql)) {
			$table = $row[0]; // cada uma das tabelas
			if ($table == "telecentro" || $table == "perfildelogin" || $table == "estagiario" || $table == "pesquisa" || $table == "pesquisa_opiniao" || $table == "pesquisa_resposta" || $table == "turma" || $table == "turma_grade" || $table == "turma_usuario" || $table == "diariodeclasse_observacao" || $table == "diariodeclasse_presenca" || $table == "usuario" || $table == "usuario_conectividade" || $table == "usuario_deficiencia" || $table == "usuario_programagoverno") {
				ExportaMysql($diretorio,$table);
			}
		}
	}
	
	// compacta o banco de dados
	
	$backup = shell_exec("sudo zip -r ". $ArquivoTar ." ". $PastaInstalada ."/backup/". date("Ymd"));
	GravaLog("backup gerado");
	/* Redirect to a different page in the current directory that was requested */
	$host  = $_SERVER['HTTP_HOST'];
	$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	$extra = "backup/bkp_". date("Ymd") .".zip";
	header("Location: http://$host$uri/$extra");
	exit;
	GravaLog("arquivo de backup criado pronto para ser baixado");
?>