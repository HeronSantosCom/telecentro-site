<?php 

	// carrega configuração

	require("include.configuracao.php");

	require("include.funcao.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<title><?php echo $NomeSistema ?></title>

<link href="include.estilo.css" rel="stylesheet" type="text/css" />

<script language="JavaScript" type="text/javascript" src="include.javascript.js"></script>

</head>

<body>

<table width="100%" height="58" border="0" cellpadding="0" cellspacing="0" id="status">

	<tr>

		<td width="5" height="5" align="right" valign="bottom"><img src="imagens/curva-2-cima-esq.png" alt="canto cima esquerdo" width="5" height="5" /></td>

		<td height="5" bgcolor="#666666"></td>

		<td width="5" height="5" align="left" valign="bottom"><img src="imagens/curva-2-cima-dir.png" alt="canto cima direito" width="5" height="5" /></td>

	</tr>

	<tr>

		<td width="5" bgcolor="#666666"></td>

		<td align="center" bgcolor="#666666" class="textopequeno-branco"><?php echo "$NomeSistemaAbreviado $VersaoSistema - $NomeSistema"; ?></td>

		<td width="5" bgcolor="#666666"></td>

	</tr>

	<tr>

		<td width="5" height="5" align="right" valign="top"><img src="imagens/curva-2-baixo-esq.png" alt="canto baixo esquerdo" width="5" height="5" /></td>

		<td height="5" bgcolor="#666666"></td>

		<td width="5" height="5" align="left" valign="top"><img src="imagens/curva-2-baixo-dir.png" alt="canto baixo direito" width="5" height="5" /></td>

	</tr>

</table>

</body>

</html>

