<?php 

	// carrega configura��o

	require("include.configuracao.php");

	require("include.funcao.php");

	require("include.menu.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title><?php echo $NomeSistema ?></title>

<link href="include.estilo.css" rel="stylesheet" type="text/css" />

<script language="JavaScript" type="text/javascript" src="include.javascript.js"></script>

<style type="text/css">
	body {
		overflow-y:scroll;
	}
</style>

</head>

<body>

<table border="0" cellpadding="0" cellspacing="0" id="menu">

	<tr>

		<td width="5"></td>

		<?php 

	// cria o menu com os itens definidos no include.configuracao.php

	foreach ($ArrayPaginas as $key => $row) {

		if ($EstadoAtual == $row['exibe'] || $row['exibe'] == "ambos") {

			if ($row['tipo'] == "pm") {

				if ($autenticacao_acessonivel <= $row['nivel']) {

					if ($PaginaMatriz == $row['link']) { // se for a p&aacute;gina atual ou selecionada, colore a aba

						$TituloPagina = $row['titulo'];

						$acessonivel = $row['nivel'];

?>

		<td valign="bottom"><table border="0" cellpadding="0" cellspacing="0">

				<tr>

					<td align="right" valign="bottom"><img src="imagens/curva-5-cima-esq.png" alt="curva" /></td>

					<td bgcolor="#FF9900"></td>

					<td align="left" valign="bottom"><img src="imagens/curva-5-cima-dir.png" alt="curva" /></td>

				</tr>

				<tr bgcolor="#FF9900">

					<td bgcolor="#FF9900"></td>

					<td align="center" valign="middle" bgcolor="#FF9900" class="textopequeno-preto"><a href="?pm=<?php echo $row['link'] ?>"><font color="#000000"><?php echo $row['titulo'] ?></font></a></td>

					<td bgcolor="#FF9900"></td>

				</tr>

			</table></td>

		<td width="5"></td>

		<?php

					} else { // sen&atilde;o deixa normal

?>

		<td valign="bottom"><table border="0" cellpadding="0" cellspacing="0">

				<tr>

					<td align="right" valign="bottom"><img src="imagens/curva-4-cima-esq.png" alt="curva" /></td>

					<td bgcolor="#FFCC33"></td>

					<td align="left" valign="bottom"><img src="imagens/curva-4-cima-dir.png" alt="curva" /></td>

				</tr>

				<tr bgcolor="#FFCC33">

					<td bgcolor="#FFCC33"></td>

					<td align="center" valign="middle" bgcolor="#FFCC33" class="textopequeno-preto"><a href="?pm=<?php echo $row['link'] ?>"><font color="#000000"><?php echo $row['titulo'] ?></font></a></td>

					<td bgcolor="#FFCC33"></td>

				</tr>

			</table></td>

		<td width="5"></td>

		<?php

					}

				}

			}

		}

	}

?>

	</tr>

</table>

<table width="700" border="0" cellpadding="0" cellspacing="0">

	<tr>

		<td align="right" valign="bottom"><img src="imagens/curva-5-cima-esq.png" alt="curva" /></td>

		<td bgcolor="#FF9900"></td>

		<td align="left" valign="bottom"><img src="imagens/curva-5-cima-dir.png" alt="curva" /></td>

	</tr>

	<tr bgcolor="#FF9900">

		<td bgcolor="#FF9900"></td>

		<td width="100%" bgcolor="#FF9900"><table height="24" border="0" cellpadding="0" cellspacing="0" id="pagina">

				<tr>

					<td width="24" height="24" align="center" valign="middle"><img src="imagens/marcador-normal.png" alt="normal" width="14" height="14" /></td>

					<td class="textoextragrande-branco"><strong><?php echo $TituloPagina; ?></strong></td>

				</tr>

			</table>

			<?php

	// se existir sub p&aacute;ginas

	if (array_key_exists("ps",$_GET)) {

		foreach ($ArrayPaginas as $key => $row) {

			if ($SubPagina == $row['link']) { // se for a p&aacute;gina atual ou selecionada, colore a aba

				$TituloSubPagina = $row['titulo'];

				$acessonivel = $row['nivel'];

			}

		}

?>

			<table height="24" border="0" cellpadding="0" cellspacing="0" id="pagina">

				<tr>

					<td width="25"></td>

					<td width="24" height="24" align="center" valign="middle"><img src="imagens/marcador-destacado.png" alt="destacado" width="14" height="14" /></td>

					<td class="textogrande-preto"><strong><?php echo $TituloSubPagina; ?></strong></td>

				</tr>

			</table>

			<?php

	}

?></td>

		<td bgcolor="#FF9900"></td>

	</tr>

	<tr>

		<td align="right" valign="bottom"><img src="imagens/curva-5-baixo-esq.png" alt="curva" /></td>

		<td bgcolor="#FF9900"></td>

		<td align="left" valign="bottom"><img src="imagens/curva-5-baixo-dir.png" alt="curva" /></td>

	</tr>

</table>

<table width="700" border="0" cellpadding="0" cellspacing="0">

	<tr>

		<td height="5"></td>

	</tr>

	<tr>

		<td height="200" align="center" valign="top" class="textomedio-preto"><?php

	if ($acessonivel_perfildelogin <= $acessonivel) { // carrega a p�gina

		include($CarregaPagina);

	} else { // mensagem de erro!

?>

<br />

<br />

<table width="350" border="0" cellpadding="0" cellspacing="0" id="status">

	<tr>

		<td width="5" height="5" align="right" valign="bottom"><img src="imagens/curva-6-cima-esq.png" alt="canto cima esquerdo" width="5" height="5" /></td>

		<td height="5" bgcolor="#EBEBEB"></td>

		<td width="5" height="5" align="left" valign="bottom"><img src="imagens/curva-6-cima-dir.png" alt="canto cima direito" width="5" height="5" /></td>

	</tr>

	<tr>

		<td width="5" bgcolor="#EBEBEB"></td>

		<td align="center" bgcolor="#EBEBEB"><table width="100%" border="0" cellspacing="10" cellpadding="0">

				<tr>

					<td width="48" height="48"><img src="imagens/exclamacao.png" alt="exclamacao" width="48" height="48" /></td>

					<td align="center" class="textogrande-preto"><strong>Acesso Negado </strong></td>

				</tr>

				<tr>

					<td valign="top">&nbsp;</td>

					<td align="center" valign="top" class="textomedio-preto">								Entre em contato com o administrador para obter mais detalhes... <br />

						<br />

						<table width="100%" border="0" cellspacing="3" cellpadding="0">

							<tr>

								<td width="65%"><input type="button" class="button-destacado" value="Voltar" onClick="javascript:CarregaPagina('javascript:history.go(-1)','_self')" /></td>

							</tr>

						</table></td>

				</tr>

			</table></td>

		<td width="5" bgcolor="#EBEBEB"></td>

	</tr>

	<tr>

		<td width="5" height="5" align="right" valign="top"><img src="imagens/curva-6-baixo-esq.png" alt="canto baixo esquerdo" width="5" height="5" /></td>

		<td height="5" bgcolor="#EBEBEB"></td>

		<td width="5" height="5" align="left" valign="top"><img src="imagens/curva-6-baixo-dir.png" alt="canto baixo direito" width="5" height="5" /></td>

	</tr>

</table>

<?php

	}

?></td>

	</tr>

</table>

</body>

</html>

