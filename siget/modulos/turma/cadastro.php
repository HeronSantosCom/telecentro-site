<?php 
	if (empty($_GET["passo"])) {
		$passoAtual = $_POST["passo"];
	} else {
		$passoAtual = $_GET["passo"];
	}
	
	// verifica a que passo dever� ser aberto
	if ($_POST["post"] == "Avan�ar >>") {
		switch ($passoAtual) {
			case "configuracao": $passo = "horario"; break;
			case "horario": $passo = "usuario"; break;
			case "usuario": $passo = "confirmacao"; break;
			case "confirmacao": $passo = "concluido"; break;
			default: $passo = "configuracao"; break;
		}
	} else {
		if ($_POST["post"] == "<< Voltar") {
			switch ($passoAtual) {
				case "confirmacao": $passo = "usuario"; break;
				case "usuario": $passo = "horario"; break;
				case "horario": $passo = "configuracao"; break;
				default: $passo = "configuracao"; break;
			}
		} else { $passo = "configuracao"; }
	}
	if ($_POST["post"] == "Cancelar") { $passo = "cancelado"; }
	if ($_POST["post"] == "+" and $passoAtual == "horario") { $passo = "horario"; }
	if ($_POST["post"] == "-" and $passoAtual == "horario") { $passo = "horario"; }
	if ($_POST["post"] == "Consultar" and $passoAtual == "usuario") { $passo = "usuario"; }
	if ($_POST["post"] == "+" and $passoAtual == "usuario") { $passo = "usuario"; }
	if ($_POST["post"] == "-" and $passoAtual == "usuario") { $passo = "usuario"; }

	// verifica se vai editar ou cadastrar e resgata os dados
	if (!empty($_POST["id_turma"])) {
		//echo "POST<br>";
		$id_turma = $_POST["id_turma"];
		$id_curso = $_POST["id_curso"];
		$codigo_telecentro = $_POST["codigo_telecentro"];
		$codigo = $_POST["codigo"];
		$datainicio = $_POST["datainicio"];
		$estado = $_POST["estado"];
	} else {
		//echo "GET<br>";
		$id_turma = $_GET["id_turma"];
		$id_curso = $_GET["id_curso"];
		$codigo_telecentro = $_GET["codigo_telecentro"];
		$codigo = $_GET["codigo"];
		$datainicio = $_GET["datainicio"];
		$estado = $_GET["estado"];
	}
	
	// se o id da turma for nulo, cria
	if (empty($id_turma)) {
		//echo "CRIA UMA NOVA ID<br>";
		$id_turma = ProximoRegistro(id,turma);
	}

	if ($passo == "configuracao" || empty($passo)) {
		if (!empty($id_turma)) {
			//echo "N�O EXISTE ID<br>";
			$sql = mysql_query("select * from turma where id='$id_turma'");
			if ($turma = mysql_fetch_array($sql)) {
				//echo "EXISTE TURMA<br>";
				$id_turma = $turma["id"];
				$id_curso = $turma["id_curso"];
				$codigo_telecentro = $turma["codigo_telecentro"];
				$codigo = $turma["codigo"];
				$datainicio = ConverteData($turma["datainicio"], "/", "mysql.normal");
				$estado = $turma["estado"];
			}
		}
		
		//echo "$id_turma<br>";
		//echo "$id_curso<br>";
		//echo "$codigo_telecentro<br>";
		//echo "$codigo<br>";
		//echo "$datainicio<br>";
		//echo "$estado<br>";
		//echo "$id_turma<br>";
?>
<form action="?pm=turma&amp;ps=cadastro" method="post" name="turma" id="turma">
	<?php TopicoCinza("Configura&ccedil;&atilde;o da Turma"); ?>
	<table width="700" border="0" cellspacing="2" cellpadding="0">
		<tr>
			<td width="200" class="linha-salmao-fundo">Curso:</td>
			<td><select name="id_curso" id="id_curso" class="input-destacado" style="width:100%" >
					<option value="" selected="selected">Selecione...</option>
					<option value="">-------------------</option>
<?php 
		// enfilera os sexos
		$sql = mysql_query("select * from curso");
		while ($curso = mysql_fetch_array($sql)) {
			if ($curso['id'] == $id_curso) {		
				echo "<option value=\"".$curso['id']."\" selected=\"selected\">".$curso['codigo']." - ".$curso['curso']."</option>";
			} else {
				echo "<option value=\"".$curso['id']."\">".$curso['codigo']." - ".$curso['curso']."</option>";
			}
		}
?>
				</select></td>
		</tr>
<?php 
		if ($TipoTelecentro == 3) {
?>
		<tr>
			<td class="linha-salmao-fundo">Telecentro:</td>
			<td><select name="codigo_telecentro" id="codigo_telecentro" style="width:100%" class="input-destacado" >
					<option value="" selected="selected">Selecione...</option>
					<option value="">-------------------</option>
<?php 
			// enfilera os tipos de usu�rios
			$sql = mysql_query("select * from telecentro order by codigo asc");
			while ($telecentro = mysql_fetch_array($sql)) {
				if ($codigo_telecentro == $telecentro['codigo']) {
					echo "<option value=\"".$telecentro['codigo']."\" selected=\"selected\">".$telecentro['codigo']." - ".$telecentro['nome']."</option>";
				} else {
					echo "<option value=\"".$telecentro['codigo']."\">".$telecentro['codigo']." - ".$telecentro['nome']."</option>";
				}
			}
?>
				</select></td>
		</tr>
<?php 
		} else {
			echo "<input name=\"codigo_telecentro\" type=\"hidden\" id=\"codigo_telecentro\" value=\"$CodigoTelecentro\">";
		}
?>
		<tr>
			<td class="linha-salmao-fundo">In&iacute;cio das aulas:</td>
			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input name="datainicio" type="text" class="input-destacado" id="datainicio" style="width:100%" onkeypress="return BloqueiaAlfa(event);" onkeyup="FormataCampo(this, '##/##/####', event);" value="<?php echo $datainicio ?>" maxlength="10" /></td>
						<td width="2">&nbsp;</td>
						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>
					</tr>
				</table></td>
		</tr>
<?php 
		if ($TipoTelecentro == 3) {
?>
		<tr>
			<td class="linha-salmao-fundo">Estado:</td>
			<td><select name="estado" id="estado" class="input-destacado" style="width:50%" >
					<option value="" selected="selected">Selecione...</option>
					<option value="">-------------------</option>
<?php
		foreach ($array_estado as $key => $row) {
			if ($estado == $row['estado']) {
				echo "<option value=\"".$row['estado']."\" selected=\"selected\">".$row['estado_nome']."</option>";
			} else {
				echo "<option value=\"".$row['estado']."\">".$row['estado_nome']."</option>";
			}
		}
?>
				</select></td>
		</tr>
<?php 
		} else {
			echo "<input name=\"estado\" type=\"hidden\" id=\"estado\" value=\"andamento\">";
		}
?>
	</table>
	<input name="id_turma" type="hidden" id="id_turma" value="<?php echo $id_turma ?>">
	<input name="codigo" type="hidden" id="codigo" value="<?php echo $codigo ?>">
	<input name="acao" type="hidden" id="acao" value="<?php echo $acao ?>">
	<input name="passo" type="hidden" id="passo" value="<?php echo $passo ?>">
	<table width="700" border="0" cellspacing="3" cellpadding="0">
		<tr>
			<td width="50%"><input name="post" type="submit" class="button-normal" id="post" value="Cancelar" /></td>
			<td width="50%"><input name="post" type="submit" class="button-destacado" id="post" value="Avan�ar >>"  onClick="return validaTurma();" /></td>
		</tr>
	</table>
</form>
<?php 
	} elseif ($passo == "horario") {
		$sql = mysql_query("SELECT * FROM curso WHERE id=$id_curso");
		if ($curso = mysql_fetch_array($sql)) {
			$curso_codigo = $curso["codigo"];
			$codigo = ($curso_codigo.date("m").date("y").str_pad($id_turma, 5, "0", STR_PAD_LEFT).$codigo_telecentro);
		}

		// verifica se j� existe o cadastro do diario de classe
		$total = RecordCount("turma","id=$id_turma");
		if ($total == 0) { // se n�o existe o diario de classe cadastra-o
			$insert = mysql_query("INSERT INTO turma SET id = '$id_turma', id_curso = '$id_curso', codigo_telecentro = '$codigo_telecentro', codigo = '$codigo', datainicio = '". ConverteData($datainicio,"-","normal.mysql") ."', estado = '$estado'");
			GravaLog("turma cadastrado: $codigo");
		} else { // se j� existe o diario de classe atualiza-se o atual
			$update = mysql_query("UPDATE turma SET id_curso = '$id_curso', codigo_telecentro = '$codigo_telecentro', codigo = '$codigo', datainicio = '". ConverteData($datainicio,"-","normal.mysql") ."', estado = '$estado' WHERE id=$id_turma");
			GravaLog("turma atualizada: $codigo");
		}
		
		$sql = mysql_query("SELECT * FROM curso_modulo WHERE id_curso=$id_curso");
		while ($curso_modulo = mysql_fetch_array($sql)) {
			$cargahoraria += RecordSum("modulo","cargahoraria","id=".$curso_modulo["id_modulo"]."");
			$diaatual = 0;
		}
		
		if($_POST["post"] == "+" && !empty($_POST["gradeDia"]) && !empty($_POST["gradeHoraIni"]) && !empty($_POST["gradeHoraFim"])) { // adiciona o dia a grade
			$id_turma_grade = ProximoRegistro(id,turma_grade);
			$diasemana = $_POST["gradeDia"];
			$horainicio = $_POST["gradeHoraIni"];
			$horafim = $_POST["gradeHoraFim"];
			$total_turma_grade = RecordCount("turma_grade","codigo_turma='$codigo' AND diasemana = '$diasemana'");
			if ($total_turma_grade <= 0) {
				$insert = mysql_query("INSERT INTO turma_grade SET id = '$id_turma_grade', codigo_turma = '$codigo', diasemana = '$diasemana', horainicio = '$horainicio', horafim = '$horafim'");
				GravaLog("dia cadastrado na grade da turma: $codigo");
			}
		}
		
		if($_POST["post"] == "-") { // remove o dia a grade
			if (!empty($_POST["idRemoveAula"])) {
				$delete = mysql_query("DELETE FROM turma_grade WHERE id=". $_POST["idRemoveAula"]);
				GravaLog("dia remivodo da grade da turma: $codigo");
			}
		}
		
		$total_turma_grade = RecordCount("turma_grade","codigo_turma='$codigo'");
		if ($total_turma_grade > 0 ) {
			if ($cargahoraria == 0) {
				$diaatual = 274;
			}
			while ($cargahoraria > 0) {
				$data = SomarData($datainicio, $diaatual, 0, 0);
				$mostraData = $data;
				$data = explode("/", $data);
				$codigoSemana = ConversorUniversal(date('D',mktime(0, 0, 0, $data[1], $data[0], $data[2])), "semanacurta.num");
				$sql = mysql_query("SELECT * FROM turma_grade WHERE codigo_turma='$codigo'");
				while ($turma_grade = mysql_fetch_array($sql)) { 
					if ($turma_grade["diasemana"] == $codigoSemana) {
						$horainicio = $turma_grade["horainicio"];
						$horafim = $turma_grade["horafim"];
						$nDiff = strtotime($horafim) - strtotime($horainicio);
						$nHour = round($nDiff / 3600, 1);
						//echo "<br>Carga Atualmente: $cargahoraria - $nHour = ";
						$cargahoraria -= $nHour;
						//echo "$cargahoraria (D + $diaatual = $mostraData)";
					}
				}
				$diaatual += 1;
			}
			$datatermino = SomarData($datainicio, $diaatual-1, 0, 0);
			//echo "<br>Ultima aula: $datatermino";
			$update = mysql_query("UPDATE turma SET datatermino = '". ConverteData($datatermino,"-","normal.mysql") ."' WHERE id=$id_turma");
			GravaLog("termino da turma calculado: $codigo");
		}
?>
<form action="?pm=turma&amp;ps=cadastro" method="post" name="turma" id="turma">
	<?php TopicoCinza("Grade de Hor&aacute;rio"); ?>
	<input name="id_turma" type="hidden" id="id_turma" value="<?php echo $id_turma ?>">
	<input name="id_curso" type="hidden" id="id_curso" value="<?php echo $id_curso ?>">
	<input name="codigo_telecentro" type="hidden" id="codigo_telecentro" value="<?php echo $codigo_telecentro ?>">
	<input name="codigo" type="hidden" id="codigo" value="<?php echo $codigo ?>">
	<input name="datainicio" type="hidden" id="datainicio" value="<?php echo $datainicio ?>">
	<input name="estado" type="hidden" id="estado" value="<?php echo $estado ?>">
	<input name="acao" type="hidden" id="acao" value="<?php echo $acao ?>">
	<input name="passo" type="hidden" id="passo" value="<?php echo $passo ?>">
	<table width="700" border="0" cellspacing="2" cellpadding="0">
		<tr>
			<td width="200" class="linha-salmao-fundo">Dia da aula: </td>
			<td width="518" colspan="6"><select name="gradeDia" id="gradeDia" style="width:50%" class="input-normal">
					<option value="">Selecione...</option>
					<option value="">-------------------</option>
					<option value="1">Domingo</option>
					<option value="2">Segunda-feira</option>
					<option value="3">Ter&ccedil;a-feira</option>
					<option value="4">Quarta-feira</option>
					<option value="5">Quinta-feira</option>
					<option value="6">Sexta-feira</option>
					<option value="7">S&aacute;bado</option>
				</select></td>
		</tr>
		<tr>
			<td class="linha-salmao-fundo">Hora de in&iacute;cio da aula:</td>
			<td width="102"><input name="gradeHoraIni" type="text" class="input-normal" id="gradeHoraIni" style="width:100%" onkeypress="return BloqueiaAlfa(event);" onkeyup="FormataCampo(this, '##:##', event);" maxlength="5" /></td>
			<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>
			<td width="200" class="linha-salmao-fundo">&nbsp;&nbsp;&nbsp;Hora de sa&iacute;da da aula:</td>
			<td width="102"><input name="gradeHoraFim" type="text" class="input-normal" id="gradeHoraFim" style="width:100%" onkeypress="return BloqueiaAlfa(event);" onkeyup="FormataCampo(this, '##:##', event);" maxlength="5" /></td>
			<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>
			<td width="30"><input name="post" type="submit" class="button-destacado" id="post" value="+" /></td>
		</tr>
	</table>
<?php 
		$total = RecordCount("turma_grade","codigo_turma='$codigo'");
		if ($total > 0) {
?>
	<br />
	<table width="435" border="0" cellpadding="0" cellspacing="2">
		<tr>
			<td width="200" align="center" class="linha-salmao-fundo" bgcolor="#FFCCCC">Dia da Semana</td>
			<td width="200" align="center" class="linha-salmao-fundodireito" bgcolor="#FFCCCC">Hor�rio</td>
		</tr>
<?php 
			// lista os hor�rios
			$sql_turma_grade = mysql_query("SELECT * FROM turma_grade WHERE codigo_turma='$codigo' ORDER BY diasemana ASC");
			while($turma_grade = mysql_fetch_array($sql_turma_grade)) {
?>
		<tr onmouseover="this.className='celulaefeito-over'" onmouseout="this.className='celulaefeito-out'">
			<td align="center" class="linha-salmao-fundo"><?php echo ucfirst(ConversorUniversal($turma_grade["diasemana"], "num.semana")); ?></td>
			<td align="center" class="linha-salmao-fundo"><?php echo $turma_grade["horainicio"] . " - " . $turma_grade["horafim"]; ?></td>
			<td align="center" width="35"><input name="post" type="submit" class="button-destacado" id="post" value="-" onclick="return removendoaula('<?php echo $turma_grade["id"] ?>');" /></td>
		</tr>
<?php 
			}
?>
	</table>
	<input name="idRemoveAula" type="hidden" id="idRemoveAula" value="">
<?php 
		}
?>
	<br />
	<table width="700" border="0" cellspacing="3" cellpadding="0">
		<tr>
			<td width="50%"><input name="post" type="submit" class="button-normal" id="post" value="<< Voltar" /></td>
			<td width="50%"><input name="post" type="submit" class="button-destacado" id="post" value="Avan�ar >>" /></td>
		</tr>
	</table>
</form>
<?php 
	} elseif ($passo == "usuario") {
		if($_POST["post"] == "+") { // adiciona o usu�rio a grade
			if (!empty($_POST["idAdicionaUsuario"])) {
				$id_turma_usuario = ProximoRegistro(id,turma_usuario);
				$matricula_usuario = $_POST["idAdicionaUsuario"];
				$datainsercao = date("Y-m-d");
				$sql_turma_usuario = mysql_query("SELECT * FROM turma_usuario WHERE codigo_turma='$codigo' AND matricula_usuario='$matricula_usuario'");
				if (!($turma_usuario = mysql_fetch_array($sql_turma_usuario))) {
					$insert = mysql_query("INSERT INTO turma_usuario SET id = '$id_turma_usuario', codigo_turma = '$codigo', matricula_usuario = '$matricula_usuario', datainsercao = '$datainsercao'");
					GravaLog("usu�rio adicionado na turma: $codigo");
				}
			}
		}

		if($_POST["post"] == "-") { // remove o usu�rio a grade
			if (!empty($_POST["idRemoveUsuario"])) {
				$delete = mysql_query("DELETE FROM turma_usuario WHERE id=". $_POST["idRemoveUsuario"]);
				GravaLog("usu�rio removido da turma: $codigo");
			}
		}	
?>
<form action="?pm=turma&amp;ps=cadastro" method="post" name="turma" id="turma">
	<?php TopicoCinza("Grade de Usu&aacute;rios"); ?>
<?php 
		$total = RecordCount("turma_usuario","codigo_turma='$codigo'");
		if ($total > 0) {
?>
	<table width="600" border="0" cellspacing="2" cellpadding="0">
		<tr>
			<td width="100" align="center" bgcolor="#FFCCCC" class="linha-salmao-fundo">Matr&iacute;cula</td>
			<td align="center" bgcolor="#FFCCCC" class="linha-salmao-fundo">Nome</td>
			<td width="100" align="center" bgcolor="#FFCCCC" class="linha-salmao-fundodireito">Data inscri&ccedil;&atilde;o </td>
		</tr>
<?php 
			// lista os hor�rios
			$sql_turma_usuario = mysql_query("SELECT * FROM turma_usuario WHERE codigo_turma='$codigo'");
			while($turma_usuario = mysql_fetch_array($sql_turma_usuario)) {
				$sql_usuario = mysql_query("SELECT * FROM usuario WHERE matricula='". $turma_usuario["matricula_usuario"]."'");
				$usuario = mysql_fetch_array($sql_usuario);
?>
		<tr onmouseover="this.className='celulaefeito-over'" onmouseout="this.className='celulaefeito-out'">
			<td align="center" class="linha-salmao-fundo"><?php echo $usuario["matricula"]; ?></td>
			<td class="linha-salmao-fundo"><?php echo $usuario["nome"]; ?></td>
			<td align="center" class="linha-salmao-fundodireito"><?php echo ConverteData($usuario["datainscricao"], "/", "mysql.normal"); ?></td>
			<td align="center" width="35"><input name="post" type="submit" class="button-destacado" id="post" value="-" onclick="return removendoUsuario('<?php echo $turma_usuario["id"] ?>');" /></td>
		</tr>
<?php 
			}
?>
	</table>
	<input name="idRemoveUsuario" type="hidden" id="idRemoveUsuario" value="">
<?php 
		} else {
?>
	<table width="350" border="0" cellpadding="0" cellspacing="0" id="status">
		<tr>
			<td width="5" height="5" align="right" valign="bottom"><img src="imagens/curva-6-cima-esq.png" alt="canto cima esquerdo" width="5" height="5" /></td>
			<td height="5" bgcolor="#EBEBEB"></td>
			<td width="5" height="5" align="left" valign="bottom"><img src="imagens/curva-6-cima-dir.png" alt="canto cima direito" width="5" height="5" /></td>
		</tr>
		<tr>
			<td width="5" bgcolor="#EBEBEB"></td>
			<td align="center" bgcolor="#EBEBEB"><table width="100%" border="0" cellspacing="10" cellpadding="0">
					<tr>
						<td width="48" valign="top"><img src="imagens/exclamacao.png" alt="exclamacao" width="48" height="48" /></td>
						<td align="center" valign="middle" class="textopequeno-preto">Nenhum usu&aacute;rio cadastrado!</td>
					</tr>
				</table></td>
			<td width="5" bgcolor="#EBEBEB"></td>
		</tr>
		<tr>
			<td width="5" height="5" align="right" valign="top"><img src="imagens/curva-6-baixo-esq.png" alt="canto baixo esquerdo" width="5" height="5" /></td>
			<td height="5" bgcolor="#EBEBEB"></td>
			<td width="5" height="5" align="left" valign="top"><img src="imagens/curva-6-baixo-dir.png" alt="canto baixo direito" width="5" height="5" /></td>
		</tr>
	</table>
<?php 
		}
?>
	<?php TopicoLaranja("Inserir Usu&aacute;rio"); ?>
	<?php
		// pega a variavel busca do formulario de consulta
		if (empty($_POST["busca"])) {
			$stringBusca = trim($_GET["busca"]);
		} else {
			$stringBusca = trim($_POST["busca"]);
		}
?>
	<table width="600" border="0" cellpadding="0" cellspacing="2">
		<tr>
			<td width="125" class="linha-salmao-fundo">Nome ou matr&iacute;cula:</td>
			<td><input name="busca" type="text" class="input-destacado" id="busca" style="width:100%" value="<?php echo $stringBusca; ?>" onkeyup="this.value = this.value.toUpperCase();" /></td>
			<td width="25"><input type="button" class="button-normal" onclick="alert('Pesquisar por matr�cula ou por nome...')" value="?" /></td>
			<td width="100"><input name="post" type="submit" class="button-destacado" id="post" value="Consultar"/></td>
		</tr>
	</table>
<?php 
		if(!empty($stringBusca)) {

			// monta a string da conexao
			$sqlforma = "matricula = '".$stringBusca."' OR nome LIKE '%".$stringBusca."%' AND codigo_telecentro='".$codigo_telecentro."'";
			
			// conta quantos usu�rio existem
			$total = RecordCount("usuario",$sqlforma);
		
			// se existir mais que um
			if ($total > 0) {
?>
	<table width="600" border="0" cellspacing="2" cellpadding="0">
		<tr>
			<td width="100" align="center" bgcolor="#FFCCCC" class="linha-salmao-fundo">Matr&iacute;cula</td>
			<td align="center" bgcolor="#FFCCCC" class="linha-salmao-fundo">Nome</td>
			<td width="100" align="center" bgcolor="#FFCCCC" class="linha-salmao-fundodireito">Data inscri&ccedil;&atilde;o </td>
		</tr>
<?php 
				// lista os usu�rios
				$sql_usuario = mysql_query("SELECT * FROM usuario WHERE ".$sqlforma." ORDER BY nome ASC");
				while($usuario = mysql_fetch_array($sql_usuario)) {
					$sql_turma_usuario = mysql_query("SELECT * FROM turma_usuario WHERE codigo_turma='$codigo' AND matricula_usuario='".$usuario["matricula"]."'");
					if(!$turma_usuario = mysql_fetch_array($sql_turma_usuario)) {
?>
		<tr onmouseover="this.className='celulaefeito-over'" onmouseout="this.className='celulaefeito-out'">
			<td align="center" class="linha-salmao-fundo"><?php echo $usuario["matricula"]; ?></td>
			<td class="linha-salmao-fundo"><?php echo $usuario["nome"]; ?></td>
			<td align="center" class="linha-salmao-fundodireito"><?php echo ConverteData($usuario["datainscricao"], "/", "mysql.normal"); ?></td>
			<td align="center" width="35"><input name="post" type="submit" class="button-destacado" id="post" value="+" onclick="return adicionandoUsuario('<?php echo $usuario["matricula"] ?>');" /></td>
		</tr>
<?php 
					} else {
?>
		<tr>
			<td style="color:#999999" align="center" class="linha-salmao-fundo"><?php echo $usuario["matricula"]; ?></td>
			<td style="color:#999999" class="linha-salmao-fundo"><?php echo $usuario["nome"]; ?></td>
			<td style="color:#999999" align="center" class="linha-salmao-fundodireito"><?php echo ConverteData($usuario["datainscricao"], "/", "mysql.normal"); ?></td>
			<td style="color:#999999" align="center" width="35"><input name="post" type="submit" class="button-destacado" id="post" value="+" disabled="disabled" /></td>
		</tr>
<?php
					}
				}
?>
	</table>
	<input name="idAdicionaUsuario" type="hidden" id="idAdicionaUsuario" value="">
<?php 
			} else {
?>
	<br />
	<span class="textomedio-preto">Usu&aacute;rio n&atilde;o encontrado!<br />
	Verifique se voc&ecirc; digitou o nome ou a matr&iacute;cula corretamente.<br />
	</span><br />
<?php 
			}
		}
?>
	<br />
	<input name="id_turma" type="hidden" id="id_turma" value="<?php echo $id_turma ?>">
	<input name="id_curso" type="hidden" id="id_curso" value="<?php echo $id_curso ?>">
	<input name="codigo_telecentro" type="hidden" id="codigo_telecentro" value="<?php echo $codigo_telecentro ?>">
	<input name="codigo" type="hidden" id="codigo" value="<?php echo $codigo ?>">
	<input name="datainicio" type="hidden" id="datainicio" value="<?php echo $datainicio ?>">
	<input name="estado" type="hidden" id="estado" value="<?php echo $estado ?>">
	<input name="acao" type="hidden" id="acao" value="<?php echo $acao ?>">
	<input name="passo" type="hidden" id="passo" value="<?php echo $passo ?>">
	<table width="700" border="0" cellspacing="3" cellpadding="0">
		<tr>
			<td width="50%"><input name="post" type="submit" class="button-normal" id="post" value="<< Voltar" /></td>
			<td width="50%"><input name="post" type="submit" class="button-destacado" id="post" value="Avan�ar >>" /></td>
		</tr>
	</table>
</form>
<?php 
	} elseif ($passo == "confirmacao") {
?>
<form action="?pm=turma&amp;ps=cadastro" method="post" name="turma" id="turma">
<?php TopicoCinza("Revisando Turma"); ?>
<table width="700" border="0" cellspacing="2" cellpadding="0">
	<tr>
		<td width="200" class="linha-salmao-fundo">Curso:</td>
		<td><select name="id_curso" id="id_curso" class="input-destacado" style="width:100%; color:#666666;" disabled="disabled" >
				<option value="" selected="selected">Selecione...</option>
				<option value="">-------------------</option>
<?php 
		// enfilera os sexos
		$sql = mysql_query("select * from curso");
		while ($curso = mysql_fetch_array($sql)) {
			if ($curso['id'] == $id_curso) {		
				echo "<option value=\"".$curso['id']."\" selected=\"selected\">".$curso['codigo']." - ".$curso['curso']."</option>";
			} else {
				echo "<option value=\"".$curso['id']."\">".$curso['codigo']." - ".$curso['curso']."</option>";
			}
		}
?>
			</select></td>
	</tr>
	<tr>
		<td class="linha-salmao-fundo">Telecentro:</td>
		<td><select name="codigo_telecentro" id="codigo_telecentro" style="width:100%; color:#666666;" class="input-destacado" disabled="disabled" >
				<option value="" selected="selected">Selecione...</option>
				<option value="">-------------------</option>
<?php 
		// enfilera os tipos de usu�rios
		$sql = mysql_query("select * from telecentro order by codigo asc");
		while ($telecentro = mysql_fetch_array($sql)) {
			if ($codigo_telecentro == $telecentro['codigo']) {
				echo "<option value=\"".$telecentro['codigo']."\" selected=\"selected\">".$telecentro['codigo']." - ".$telecentro['nome']."</option>";
			} else {
				echo "<option value=\"".$telecentro['codigo']."\">".$telecentro['codigo']." - ".$telecentro['nome']."</option>";
			}
		}
?>
			</select></td>
	</tr>
	<tr>
		<td class="linha-salmao-fundo">In&iacute;cio das aulas:</td>
		<td><table width="30%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td><input name="datainicio" type="text" class="input-destacado" id="datainicio" style="width:100%; color:#666666;" onkeypress="return BloqueiaAlfa(event);" onkeyup="FormataCampo(this, '##/##/####', event);" value="<?php echo $datainicio ?>" maxlength="10" disabled="disabled" /></td>
					<td width="2">&nbsp;</td>
					<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" disabled="disabled" /></td>
				</tr>
			</table></td>
	</tr>
	<tr>
		<td class="linha-salmao-fundo">Estado:</td>
		<td><select name="estado" id="estado" class="input-destacado" style="width:50%; color:#666666;" disabled="disabled" >
				<option value="" selected="selected">Selecione...</option>
				<option value="">-------------------</option>
<?php
		foreach ($array_estado as $key => $row) {
			if ($estado == $row['estado']) {
				echo "<option value=\"".$row['estado']."\" selected=\"selected\">".$row['estado_nome']."</option>";
			} else {
				echo "<option value=\"".$row['estado']."\">".$row['estado_nome']."</option>";
			}
		}
?>
			</select></td>
	</tr>
</table>
<?php 
		$total = RecordCount("turma_grade","codigo_turma='$codigo'");
		if ($total > 0) {
?>
<br />
<table width="435" border="0" cellpadding="0" cellspacing="2">
	<tr>
		<td width="200" align="center" class="linha-salmao-fundo" bgcolor="#FFCCCC">Dia da Semana</td>
		<td width="200" align="center" class="linha-salmao-fundodireito" bgcolor="#FFCCCC">Hor�rio</td>
	</tr>
<?php 
			// lista os hor�rios
			$sql_turma_grade = mysql_query("SELECT * FROM turma_grade WHERE codigo_turma='$codigo' ORDER BY diasemana ASC");
			while($turma_grade = mysql_fetch_array($sql_turma_grade)) {
?>
	<tr onmouseover="this.className='celulaefeito-over'" onmouseout="this.className='celulaefeito-out'">
		<td align="center" class="linha-salmao-fundo" style="color:#666666"><?php echo ucfirst(ConversorUniversal($turma_grade["diasemana"], "num.semana")); ?></td>
		<td align="center" class="linha-salmao-fundo" style="color:#666666"><?php echo $turma_grade["horainicio"] . " - " . $turma_grade["horafim"]; ?></td>
	</tr>
<?php 
			}
?>
</table>
<?php 
		}
?>
<?php 
		$total = RecordCount("turma_usuario","codigo_turma='$codigo'");
		if ($total > 0) {
?>
<br />
<table width="600" border="0" cellspacing="2" cellpadding="0">
	<tr>
		<td width="100" align="center" bgcolor="#FFCCCC" class="linha-salmao-fundo">Matr&iacute;cula</td>
		<td align="center" bgcolor="#FFCCCC" class="linha-salmao-fundo">Nome</td>
		<td width="100" align="center" bgcolor="#FFCCCC" class="linha-salmao-fundodireito">Data inscri&ccedil;&atilde;o </td>
	</tr>
<?php 
			// lista os hor�rios
			$sql_turma_usuario = mysql_query("SELECT * FROM turma_usuario WHERE codigo_turma='$codigo'");
			while($turma_usuario = mysql_fetch_array($sql_turma_usuario)) {
				$sql_usuario = mysql_query("SELECT * FROM usuario WHERE matricula='". $turma_usuario["matricula_usuario"]."'");
				$usuario = mysql_fetch_array($sql_usuario);
?>
	<tr onmouseover="this.className='celulaefeito-over'" onmouseout="this.className='celulaefeito-out'">
		<td align="center" class="linha-salmao-fundo" style="color:#666666"><?php echo $usuario["matricula"]; ?></td>
		<td class="linha-salmao-fundo" style="color:#666666"><?php echo $usuario["nome"]; ?></td>
		<td align="center" class="linha-salmao-fundodireito" style="color:#666666"><?php echo ConverteData($usuario["datainscricao"], "/", "mysql.normal"); ?></td>
	</tr>
<?php 
			}
?>
</table>
<?php 
		} else {
?>
<br />
<table width="350" border="0" cellpadding="0" cellspacing="0" id="status">
	<tr>
		<td width="5" height="5" align="right" valign="bottom"><img src="imagens/curva-6-cima-esq.png" alt="canto cima esquerdo" width="5" height="5" /></td>
		<td height="5" bgcolor="#EBEBEB"></td>
		<td width="5" height="5" align="left" valign="bottom"><img src="imagens/curva-6-cima-dir.png" alt="canto cima direito" width="5" height="5" /></td>
	</tr>
	<tr>
		<td width="5" bgcolor="#EBEBEB"></td>
		<td align="center" bgcolor="#EBEBEB"><table width="100%" border="0" cellspacing="10" cellpadding="0">
				<tr>
					<td width="48" valign="top"><img src="imagens/exclamacao.png" alt="exclamacao" width="48" height="48" /></td>
					<td align="center" valign="middle" class="textopequeno-preto">Nenhum usu&aacute;rio cadastrado!</td>
				</tr>
			</table></td>
		<td width="5" bgcolor="#EBEBEB"></td>
	</tr>
	<tr>
		<td width="5" height="5" align="right" valign="top"><img src="imagens/curva-6-baixo-esq.png" alt="canto baixo esquerdo" width="5" height="5" /></td>
		<td height="5" bgcolor="#EBEBEB"></td>
		<td width="5" height="5" align="left" valign="top"><img src="imagens/curva-6-baixo-dir.png" alt="canto baixo direito" width="5" height="5" /></td>
	</tr>
</table>
<?php 
		}
?>
<br />
<input name="id_turma" type="hidden" id="id_turma" value="<?php echo $id_turma ?>">
<input name="id_curso" type="hidden" id="id_curso" value="<?php echo $id_curso ?>">
<input name="codigo_telecentro" type="hidden" id="codigo_telecentro" value="<?php echo $codigo_telecentro ?>">
<input name="codigo" type="hidden" id="codigo" value="<?php echo $codigo ?>">
<input name="datainicio" type="hidden" id="datainicio" value="<?php echo $datainicio ?>">
<input name="estado" type="hidden" id="estado" value="<?php echo $estado ?>">
<input name="acao" type="hidden" id="acao" value="<?php echo $acao ?>">
<input name="passo" type="hidden" id="passo" value="<?php echo $passo ?>">
<table width="700" border="0" cellspacing="3" cellpadding="0">
	<tr>
		<td width="50%"><input name="post" type="submit" class="button-normal" id="post" value="<< Voltar" /></td>
		<td width="50%"><input name="post" type="submit" class="button-destacado" id="post" value="Avan�ar >>" /></td>
	</tr>
</table>
<?php 
	} elseif ($passo == "concluido") {
?>
<br />
<table width="350" border="0" cellpadding="0" cellspacing="0" id="status">
	<tr>
		<td width="5" height="5" align="right" valign="bottom"><img src="imagens/curva-6-cima-esq.png" alt="canto cima esquerdo" width="5" height="5" /></td>
		<td height="5" bgcolor="#EBEBEB"></td>
		<td width="5" height="5" align="left" valign="bottom"><img src="imagens/curva-6-cima-dir.png" alt="canto cima direito" width="5" height="5" /></td>
	</tr>
	<tr>
		<td width="5" bgcolor="#EBEBEB"></td>
		<td align="center" bgcolor="#EBEBEB"><table width="100%" border="0" cellspacing="10" cellpadding="0">
				<tr>
					<td width="48" height="48"><img src="imagens/adicionado.png" alt="adicionado" width="48" height="48" /></td>
					<td align="center" class="textogrande-preto"><strong>Cadastro de Turma </strong></td>
				</tr>
				<tr>
					<td valign="top">&nbsp;</td>
					<td align="center" valign="top" class="textomedio-preto">Opera&ccedil;&atilde;o efetuada com  sucesso!<br />
						<br />
						<table width="100%" border="0" cellspacing="3" cellpadding="0">
							<tr>
								<td width="65%"><input type="button" class="button-destacado" onclick="javascript:CarregaPagina('?pm=turma','_self')" value="Ok" /></td>
							</tr>
						</table></td>
				</tr>
			</table></td>
		<td width="5" bgcolor="#EBEBEB"></td>
	</tr>
	<tr>
		<td width="5" height="5" align="right" valign="top"><img src="imagens/curva-6-baixo-esq.png" alt="canto baixo esquerdo" width="5" height="5" /></td>
		<td height="5" bgcolor="#EBEBEB"></td>
		<td width="5" height="5" align="left" valign="top"><img src="imagens/curva-6-baixo-dir.png" alt="canto baixo direito" width="5" height="5" /></td>
	</tr>
</table>
<?php 
	} elseif ($passo == "cancelado") {
		$codigo = $_POST["codigo"];
		$delete = mysql_query("DELETE FROM turma WHERE codigo='$codigo'");
		$delete = mysql_query("DELETE FROM turma_grade WHERE codigo_turma='$codigo'");
		$delete = mysql_query("DELETE FROM turma_usuario WHERE codigo_turma='$codigo'");
		GravaLog("cadastro cancelado da turma: $codigo");
?>
<br />
<table width="350" border="0" cellpadding="0" cellspacing="0" id="status">
	<tr>
		<td width="5" height="5" align="right" valign="bottom"><img src="imagens/curva-6-cima-esq.png" alt="canto cima esquerdo" width="5" height="5" /></td>
		<td height="5" bgcolor="#EBEBEB"></td>
		<td width="5" height="5" align="left" valign="bottom"><img src="imagens/curva-6-cima-dir.png" alt="canto cima direito" width="5" height="5" /></td>
	</tr>
	<tr>
		<td width="5" bgcolor="#EBEBEB"></td>
		<td align="center" bgcolor="#EBEBEB"><table width="100%" border="0" cellspacing="10" cellpadding="0">
				<tr>
					<td width="48" height="48"><img src="imagens/exclamacao.png" alt="exclamacao" width="48" height="48" /></td>
					<td align="center" class="textogrande-preto"><strong>Cadastro de Turma </strong></td>
				</tr>
				<tr>
					<td valign="top">&nbsp;</td>
					<td align="center" valign="top" class="textomedio-preto">Opera&ccedil;&atilde;o Cancelada!<br />
						<br />
						<table width="100%" border="0" cellspacing="3" cellpadding="0">
							<tr>
								<td width="65%"><input type="button" class="button-destacado" onclick="javascript:CarregaPagina('?pm=turma','_self')" value="Ok" /></td>
							</tr>
						</table></td>
				</tr>
			</table></td>
		<td width="5" bgcolor="#EBEBEB"></td>
	</tr>
	<tr>
		<td width="5" height="5" align="right" valign="top"><img src="imagens/curva-6-baixo-esq.png" alt="canto baixo esquerdo" width="5" height="5" /></td>
		<td height="5" bgcolor="#EBEBEB"></td>
		<td width="5" height="5" align="left" valign="top"><img src="imagens/curva-6-baixo-dir.png" alt="canto baixo direito" width="5" height="5" /></td>
	</tr>
</table>
<?php 
	} else {
?>
<br />
<table width="350" border="0" cellpadding="0" cellspacing="0" id="status">
	<tr>
		<td width="5" height="5" align="right" valign="bottom"><img src="imagens/curva-6-cima-esq.png" alt="canto cima esquerdo" width="5" height="5" /></td>
		<td height="5" bgcolor="#EBEBEB"></td>
		<td width="5" height="5" align="left" valign="bottom"><img src="imagens/curva-6-cima-dir.png" alt="canto cima direito" width="5" height="5" /></td>
	</tr>
	<tr>
		<td width="5" bgcolor="#EBEBEB"></td>
		<td align="center" bgcolor="#EBEBEB"><table width="100%" border="0" cellspacing="10" cellpadding="0">
				<tr>
					<td width="48" height="48"><img src="imagens/exclamacao.png" alt="exclamacao" width="48" height="48" /></td>
					<td align="center" class="textogrande-preto"><strong>Erro</strong></td>
				</tr>
				<tr>
					<td valign="top">&nbsp;</td>
					<td align="center" valign="top" class="textomedio-preto">Erro ao efetuar opera��o!<br />
						<br />
						<table width="100%" border="0" cellspacing="3" cellpadding="0">
							<tr>
								<td width="65%"><input type="button" class="button-destacado" onclick="javascript:CarregaPagina('?pm=turma','_self')" value="Voltar" /></td>
							</tr>
						</table></td>
				</tr>
			</table></td>
		<td width="5" bgcolor="#EBEBEB"></td>
	</tr>
	<tr>
		<td width="5" height="5" align="right" valign="top"><img src="imagens/curva-6-baixo-esq.png" alt="canto baixo esquerdo" width="5" height="5" /></td>
		<td height="5" bgcolor="#EBEBEB"></td>
		<td width="5" height="5" align="left" valign="top"><img src="imagens/curva-6-baixo-dir.png" alt="canto baixo direito" width="5" height="5" /></td>
	</tr>
</table>
<?php 
	}
?>
