<?php 

	// carregas os dados cadastrados

	$sql = mysql_query("SELECT * FROM estagiario WHERE id=" . $_GET["id"]);

	$estagiario = mysql_fetch_array($sql);

	$id_estagiario 							= $estagiario["id"];

	$nome											= $estagiario["nome"];

	if (empty($estagiario["datanascimento"])) {
		$datanascimento							= ConverteData($estagiario["datanascimento"], "/", "mysql.normal");
	}

	$id_sexo										= $estagiario["id_sexo"];

	$nomepai										= $estagiario["nomepai"];

	$nomemae										= $estagiario["nomemae"];

	$matricula									= $estagiario["matricula"];

	if (empty($estagiario["dataadmissao"])) {
		$dataadmissao								= ConverteData($estagiario["dataadmissao"], "/", "mysql.normal");
	}
	

	$horaentrada								= $estagiario["horaentrada"];

	$horasaida									= $estagiario["horasaida"];

	$codigo_telecentro								= $estagiario["codigo_telecentro"];

	$id_estadocivil							= $estagiario["id_estadocivil"];

	$nis											= $estagiario["nis"];

	if ($estagiario["identidadenumero"] == "0") { $identidadenumero = "*"; } else { $identidadenumero = $estagiario["identidadenumero"]; }

	$identidadeorgaoemissor					= $estagiario["identidadeorgaoemissor"];

	if (empty($estagiario["identidadedataemissao"])) {
		$identidadedataemissao					= ConverteData($estagiario["identidadedataemissao"], "/", "mysql.normal");
	}

	if ($estagiario["ctpsnumero"] == "0") { $ctpsnumero = "*"; } else { $ctpsnumero = $estagiario["ctpsnumero"]; }

	if ($estagiario["ctpsserie"] == "0") { $ctpsserie = "*"; } else { $ctpsserie = $estagiario["ctpsserie"]; }

	if (empty($estagiario["ctpsdataemissao"])) {
		$ctpsdataemissao = ConverteData($estagiario["ctpsdataemissao"], "/", "mysql.normal");
	}

	$ctps_id_uf = $estagiario["ctps_id_uf"];

	if ($estagiario["cpf"] == "0") { $cpf = "*"; } else { $cpf = $estagiario["cpf"]; }

	if ($estagiario["tituloeleitornumero"] == "0") { $tituloeleitornumero = "*"; } else { $tituloeleitornumero = $estagiario["tituloeleitornumero"]; }

	if ($estagiario["tituloeleitorzona"] == "0") { $tituloeleitorzona = "*"; } else { $tituloeleitorzona = $estagiario["tituloeleitorzona"]; }

	if ($estagiario["tituloeleitorsecao"] == "0") { $tituloeleitorsecao = "*"; } else { $tituloeleitorsecao = $estagiario["tituloeleitorsecao"]; }

	$escola_id_escolatipo					= $estagiario["escola_id_escolatipo"];

	$escola_id_escolagrau					= $estagiario["escola_id_escolagrau"];

	$escola_id_escolaserie					= $estagiario["escola_id_escolaserie"];

	$escola_id_escolaturno					= $estagiario["escola_id_escolaturno"];

	$escola_escolanome						= $estagiario["escola_escolanome"];

	$enderecologradouro						= $estagiario["enderecologradouro"];

	$endereconumero							= $estagiario["endereconumero"];

	$enderecocomplemento						= $estagiario["enderecocomplemento"];

	$enderecocep								= $estagiario["enderecocep"];

	$endereco_id_bairro						= $estagiario["endereco_id_bairro"];
	
	if ($endereco_id_bairro == 999999) $endereco_id_urg = 999999;

	$endereco_id_municipio					= $estagiario["endereco_id_municipio"];

	$endereco_id_uf							= $estagiario["endereco_id_uf"];

	$telefonefixo								= $estagiario["telefonefixo"];

	$telefonecelular							= $estagiario["telefonecelular"];

	$email										= $estagiario["email"];

	$escola_curso								= $estagiario["escola_curso"];

	$cursos_adicionais						= $estagiario["cursos_adicionais"];

	$documento_identidade					= $estagiario["documento_identidade"];

	$documento_cpf								= $estagiario["documento_cpf"];

	$documento_comprovanteresidencia		= $estagiario["documento_comprovanteresidencia"];

	$documento_declaracaofaculdade		= $estagiario["documento_declaracaofaculdade"];

	$documento_foto							= $estagiario["documento_foto"];
	
	$disponibilidade_trabalho				= $estagiario["disponibilidade_trabalho"];
	
	$disponibilidade_estagio				= $estagiario["disponibilidade_estagio"];

?>

<form action="?pm=estagiario&amp;ps=edita.post" method="post" name="estagiario" id="estagiario" onSubmit="return validaEstagiario();">

	<input name="id_estagiario" value="<?php echo $id_estagiario ?>" type="hidden" />

	<?php TopicoCinza("Identifica&ccedil;&atilde;o do estagi&aacute;rio"); ?>

	<table width="700" border="0" cellspacing="2" cellpadding="0">

		<tr>

			<td width="200" class="linha-salmao-fundo">Nome:</td>

			<td><input name="nome" type="text" class="input-destacado" id="nome" style="width:100%" onKeyUp="this.value = this.value.toUpperCase();" value="<?php echo $nome ?>" maxlength="150" /></td>

		</tr>

		<tr>

			<td width="200" class="linha-salmao-fundo">Data de nascimento: </td>

			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">

					<tr>

						<td><input name="datanascimento" type="text" class="input-destacado" id="datanascimento" style="width:100%" onKeyPress="return BloqueiaAlfa(event);" onKeyUp="FormataCampo(this, '##/##/####', event);" value="<?php echo $datanascimento ?>" maxlength="10" /></td>

						<td width="2">&nbsp;</td>

						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>

					</tr>

				</table></td>

		</tr>

		<tr>

			<td width="200" class="linha-salmao-fundo">Sexo:</td>

			<td><select name="id_sexo" id="id_sexo" class="input-destacado" style="width:50%" >

					<option value="" selected="selected">Selecione...</option>

					<option value="">-------------------</option>

<?php 

	// enfilera os sexos

	$sql = mysql_query("select * from sexo order by sexo asc");

	while ($sexo = mysql_fetch_array($sql)) {

		if ($id_sexo == $sexo['id']) {		

			echo "<option value=\"".$sexo['id']."\" selected=\"selected\">".$sexo['sexo']."</option>";

		} else {

			echo "<option value=\"".$sexo['id']."\">".$sexo['sexo']."</option>";

		}

	}

?>

				</select></td>

		</tr>

		<tr>

			<td width="200" class="linha-salmao-fundo">Nome completo do pai:</td>

			<td><input name="nomepai" type="text" class="input-normal" id="nomepai" style="width:100%" onKeyUp="this.value = this.value.toUpperCase();" value="<?php echo $nomepai ?>" maxlength="150"/></td>

		</tr>

		<tr>

			<td width="200" class="linha-salmao-fundo">Nome completo da m&atilde;e:</td>

			<td><input name="nomemae" type="text" class="input-normal" id="nomemae" style="width:100%" onKeyUp="this.value = this.value.toUpperCase();" value="<?php echo $nomemae ?>" maxlength="150"/></td>

		</tr>

		<tr>

			<td width="200" class="linha-salmao-fundo">Estado civil:</td>

			<td><select name="id_estadocivil" id="id_estadocivil" class="input-destacado" style="width:50%" >

					<option value="" selected="selected">Selecione...</option>

					<option value="">-------------------</option>

<?php 

	// enfilera os estados civis

	$sql = mysql_query("select * from estadocivil order by estadocivil asc");

	while ($estadocivil = mysql_fetch_array($sql)) {

		if ($id_estadocivil == $estadocivil['id']) {		

			echo "<option value=\"".$estadocivil['id']."\" selected=\"selected\">".$estadocivil['estadocivil']."</option>";

		} else {

			echo "<option value=\"".$estadocivil['id']."\">".$estadocivil['estadocivil']."</option>";

		}

	}

?>

				</select></td>

		</tr>
		
		<tr>
			<td width="200" class="linha-salmao-fundo">Disponibilidade:</td>
			<td><select name="disponibilidade_estagio" id="disponibilidade_estagio" class="input-normal" style="width:50%" >
					<option value="" selected="selected">Selecione...</option>
					<option value="">-------------------</option>
<?php 
	// enfilera turnos
	$sql = mysql_query("select * from escolaturno");
	while ($escolaturno = mysql_fetch_array($sql)) {
		if ($disponibilidade_estagio == $escolaturno['id']) {		
			echo "<option value=\"".$escolaturno['id']."\" selected=\"selected\">".$escolaturno['turno']."</option>";
		} else {
			echo "<option value=\"".$escolaturno['id']."\">".$escolaturno['turno']."</option>";
		}
	}
?>
				</select></td>
		</tr>

		<tr>
			<td width="200" class="linha-salmao-fundo">Hor�rio de V�nculo Externo:</td>
			<td><select name="disponibilidade_trabalho" id="disponibilidade_trabalho" class="input-normal" style="width:50%" >
					<option value="" selected="selected">Selecione...</option>
					<option value="">-------------------</option>
<?php 
	// enfilera turnos
	$sql = mysql_query("select * from escolaturno");
	while ($escolaturno = mysql_fetch_array($sql)) {
		if ($disponibilidade_trabalho == $escolaturno['id']) {		
			echo "<option value=\"".$escolaturno['id']."\" selected=\"selected\">".$escolaturno['turno']."</option>";
		} else {
			echo "<option value=\"".$escolaturno['id']."\">".$escolaturno['turno']."</option>";
		}
	}
?>
				</select></td>
		</tr>

	</table>

	<?php TopicoCinza("Identifica��o do domic�lio"); ?>

	<table width="700" border="0" cellspacing="2" cellpadding="0">

		<tr>

			<td width="200" align="left" class="linha-salmao-fundo"> Logradouro:</td>

			<td><input name="enderecologradouro" type="text" class="input-destacado" id="enderecologradouro" style="width:100%" onKeyUp="this.value = this.value.toUpperCase();" value="<?php echo $enderecologradouro ?>" maxlength="100"/></td>

		</tr>

		<tr>

			<td width="200" align="left" class="linha-salmao-fundo">N&uacute;mero:</td>

			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">

					<tr>

						<td><input name="endereconumero" type="text" class="input-destacado" id="endereconumero" style="width:100%"onkeypress="return BloqueiaAlfa(event);" value="<?php echo $endereconumero ?>" maxlength="7" /></td>

						<td width="2">&nbsp;</td>

						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>

					</tr>

				</table></td>

		</tr>

		<tr>

			<td width="200" align="left" class="linha-salmao-fundo">Complemento:</td>

			<td><input name="enderecocomplemento" type="text" class="input-normal" id="enderecocomplemento" style="width:100%" onKeyUp="this.value = this.value.toUpperCase();" value="<?php echo $enderecocomplemento ?>" maxlength="50"/></td>

		</tr>

		<tr>

			<td width="200" align="left" class="linha-salmao-fundo">CEP:</td>

			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">

					<tr>

						<td><input name="enderecocep" type="text" class="input-normal" id="enderecocep" style="width:100%" onKeyPress="return BloqueiaAlfa(event);" onKeyUp="FormataCampo(this, '#####-###', event);" value="<?php echo $enderecocep ?>" maxlength="9" /></td>

						<td width="2">&nbsp;</td>

						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>

					</tr>

				</table></td>

		</tr>

		<tr>

			<td align="left" class="linha-salmao-fundo">Estado:</td>

			<td><select name="endereco_id_uf" id="endereco_id_uf" class="input-destacado" style="width:75%" onChange="comboDynamic(this.value,'municipio')" >

					<option value="" selected="selected">Selecione...</option>

					<option value="">-------------------</option>

					<?php 

	// enfilera os estados

	$sql = mysql_query("select * from uf order by estado asc");

	while ($uf = mysql_fetch_array($sql)) {

		if ($endereco_id_uf == $uf['id']) {		

			echo "<option value=\"".$uf['id']."\" selected=\"selected\">".$uf['estado']."</option>";

		} else {

			echo "<option value=\"".$uf['id']."\">".$uf['estado']."</option>";

		}

	}

?>
					<option value="">-------------------</option>
					<option value="999999" <?php if ($endereco_id_uf == "999999") echo "selected=\"selected\""; ?>>N&atilde;o Listado...</option>

			</select></td>

		</tr>

		<tr>

			<td align="left" class="linha-salmao-fundo">Munic&iacute;pio:</td>

			<td><select name="endereco_id_municipio" id="endereco_id_municipio" class="input-destacado" style="width:75%" onChange="comboDynamic(this.value,'bairro')" >

					<option value="" selected="selected">Selecione...</option>

					<option value="">-------------------</option>

					<?php 

	// enfilera municipios

	$sql = mysql_query("select * from municipio where id_uf = $endereco_id_uf order by municipio asc");

	while ($municipio = mysql_fetch_array($sql)) {

		if ($endereco_id_municipio == $municipio['id']) {		

			echo "<option value=\"".$municipio['id']."\" selected=\"selected\">".$municipio['municipio']."</option>";

		} else {

			echo "<option value=\"".$municipio['id']."\">".$municipio['municipio']."</option>";

		}

	}

?>
					<option value="">-------------------</option>
					<option value="999999" <?php if ($endereco_id_municipio == "999999") echo "selected=\"selected\""; ?>>N&atilde;o Listado...</option>

			</select></td>

		</tr>

		<tr>

			<td width="200" align="left" class="linha-salmao-fundo">Bairro:</td>

			<td><select name="endereco_id_bairro" id="endereco_id_bairro" class="input-destacado" style="width:75%" >

					<option value="" selected="selected">Selecione...</option>

					<option value="">-------------------</option>

					<?php 

	// enfilera bairros

	$sql = mysql_query("select * from bairro where (id_urg in(select id from urg where id_municipio = $endereco_id_municipio)) order by bairro asc");

	while ($bairro = mysql_fetch_array($sql)) {

		if ($endereco_id_bairro == $bairro['id']) {		

			echo "<option value=\"".$bairro['id']."\" selected=\"selected\">".$bairro['bairro']."</option>";

		} else {

			echo "<option value=\"".$bairro['id']."\">".$bairro['bairro']."</option>";

		}

	}

?>
					<option value="">-------------------</option>
					<option value="999999" <?php if ($endereco_id_bairro == "999999") echo "selected=\"selected\""; ?>>N&atilde;o Listado...</option>

			</select></td>

		</tr>

	</table>

	<?php TopicoCinza("Formas de contato"); ?>

	<table width="700" border="0" cellspacing="2" cellpadding="0">

		<tr>

			<td width="200" align="left" class="linha-salmao-fundo">Telefone Fixo:</td>

			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">

					<tr>

						<td><input name="telefonefixo" type="text" class="input-destacado" id="telefonefixo" style="width:100%" onKeyPress="return BloqueiaAlfa(event);" onKeyUp="FormataCampo(this, '## ####-####', event);" value="<?php echo $telefonefixo ?>" maxlength="12"/></td>

						<td width="2">&nbsp;</td>

						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>

					</tr>

				</table></td>

		</tr>

		<tr>

			<td width="200" align="left" class="linha-salmao-fundo">Telefone Celular:</td>

			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">

					<tr>

						<td><input name="telefonecelular" type="text" class="input-normal" id="telefonecelular" style="width:100%" onKeyPress="return BloqueiaAlfa(event);" onKeyUp="FormataCampo(this, '## ####-####', event);" value="<?php echo $telefonecelular ?>" maxlength="12"/></td>

						<td width="2">&nbsp;</td>

						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>

					</tr>

				</table></td>

		</tr>

		<tr>

			<td width="200" align="left" class="linha-salmao-fundo">E-mail:</td>

			<td><table width="75%" border="0" cellspacing="0" cellpadding="0">

					<tr>

						<td><input name="email" type="text" class="input-normal" id="email" style="width:100%" onKeyUp="this.value = this.value.toLowerCase();" value="<?php echo $email ?>" maxlength="100"/></td>

						<td width="2">&nbsp;</td>

						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>

					</tr>

				</table></td>

		</tr>

	</table>

	<?php TopicoCinza("Documentos"); ?>

	<table width="700" border="0" cellspacing="2" cellpadding="0">

		<tr>

			<td width="200" align="left" class="linha-salmao-fundo">NIS:</td>

			<td><table width="35%" border="0" cellspacing="0" cellpadding="0">

					<tr>

						<td><input name="nis" type="text" class="input-normal" id="nis" style="width:100%" onKeyPress="return BloqueiaAlfa(event);" value="<?php echo $nis ?>" maxlength="11" /></td>

						<td width="2">&nbsp;</td>

						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>

					</tr>

				</table></td>

		</tr>

		<tr>

			<td width="200" align="left" class="linha-salmao-fundo">CPF:</td>

			<td><table width="35%" border="0" cellspacing="0" cellpadding="0">

					<tr>

						<td><input name="cpf" type="text" class="input-destacado" id="cpf" style="width:100%" onKeyPress="return BloqueiaAlfa(event);" onKeyUp="FormataCampo(this, '###.###.###-##', event);" value="<?php echo $cpf ?>" maxlength="14"/></td>

						<td width="2">&nbsp;</td>

						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...\n\nCaso n&atilde;o tenha em m&atilde;os, digite somente *!')" value="?" /></td>

					</tr>

				</table></td>

		</tr>

		<tr>

			<td width="200" class="linha-salmao-fundo">Identidade:</td>

			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">

					<tr>

						<td><input name="identidadenumero" type="text" class="input-destacado" id="identidadenumero" style="width:100%"onkeypress="return BloqueiaAlfa(event);" value="<?php echo $identidadenumero ?>" maxlength="11" /></td>

						<td width="2">&nbsp;</td>

						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...\n\nCaso n&atilde;o tenha em m&atilde;os, digite somente *!')" value="?" /></td>

					</tr>

				</table></td>

		</tr>

		<tr>

			<td width="200" class="linha-salmao-fundo"><img src="imagens/marcador-normal.png" alt="marcador" width="14" height="14" hspace="2" vspace="0" border="0" align="absmiddle" />Data de emiss&atilde;o:</td>

			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">

					<tr>

						<td><input name="identidadedataemissao" type="text" class="input-normal" id="identidadedataemissao" style="width:100%" onKeyPress="return BloqueiaAlfa(event);" onKeyUp="FormataCampo(this, '##/##/####', event);" value="<?php echo $identidadedataemissao ?>" maxlength="10"/></td>

						<td width="2">&nbsp;</td>

						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>

					</tr>

				</table></td>

		</tr>

		<tr>

			<td width="200" class="linha-salmao-fundo"><img src="imagens/marcador-normal.png" alt="marcador" width="14" height="14" hspace="2" vspace="0" border="0" align="absmiddle" /><span class="celulaLinha">Org&atilde;o emissor</span>:</td>

			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">

					<tr>

						<td><input name="identidadeorgaoemissor" type="text" class="input-normal" id="identidadeorgaoemissor" style="width:100%" onKeyUp="this.value = this.value.toUpperCase();" value="<?php echo $identidadeorgaoemissor ?>" maxlength="10"/></td>

						<td width="2">&nbsp;</td>

						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>

					</tr>

				</table></td>

		</tr>

		<tr>

			<td width="200" class="linha-salmao-fundo">CTPS:</td>

			<td><table width="25%" border="0" cellspacing="0" cellpadding="0">

					<tr>

						<td><input name="ctpsnumero" type="text" class="input-destacado" id="ctpsnumero" style="width:100%" onKeyPress="return BloqueiaAlfa(event);" value="<?php echo $ctpsnumero ?>"maxlength="7"/></td>

						<td width="2">&nbsp;</td>

						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...\n\nCaso n&atilde;o tenha em m&atilde;os, digite somente *!')" value="?" /></td>

					</tr>

				</table></td>

		</tr>

		<tr>

			<td width="200" class="linha-salmao-fundo"><img src="imagens/marcador-normal.png" alt="marcador" width="14" height="14" hspace="2" vspace="0" border="0" align="absmiddle" />S&eacute;rie:</td>

			<td><table width="15%" border="0" cellspacing="0" cellpadding="0">

					<tr>

						<td><input name="ctpsserie" type="text" class="input-destacado" id="ctpsserie" style="width:100%" onKeyPress="return BloqueiaAlfa(event);" value="<?php echo $ctpsserie ?>"maxlength="5"/></td>

						<td width="2">&nbsp;</td>

						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...\n\nCaso n&atilde;o tenha em m&atilde;os, digite somente *!')" value="?" /></td>

					</tr>

				</table></td>

		</tr>

		<tr>

			<td width="200" class="linha-salmao-fundo"><img src="imagens/marcador-normal.png" alt="marcador" width="14" height="14" hspace="2" vspace="0" border="0" align="absmiddle" />Data de emiss&atilde;o:</td>

			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">

					<tr>

						<td><input name="ctpsdataemissao" type="text" class="input-normal" id="ctpsdataemissao" style="width:100%" onKeyPress="return BloqueiaAlfa(event);" onKeyUp="FormataCampo(this, '##/##/####', event);" value="<?php echo $ctpsdataemissao ?>" maxlength="10"/></td>

						<td width="2">&nbsp;</td>

						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>

					</tr>

				</table></td>

		</tr>

		<tr>

			<td width="200" class="linha-salmao-fundo"><img src="imagens/marcador-normal.png" alt="marcador" width="14" height="14" hspace="2" vspace="0" border="0" align="absmiddle" />Estado:</td>

			<td><select name="ctps_id_uf" id="ctps_id_uf" style="width:50%" class="input-normal" >

					<option value="" selected="selected">Selecione...</option>

					<option value="">-------------------</option>

<?php 

	// enfilera os uf's da ctps

	$sql = mysql_query("select * from uf order by estado asc");

	while ($uf = mysql_fetch_array($sql)) {

		if ($ctps_id_uf == $uf['id']) {		

			echo "<option value=\"".$uf['id']."\" selected=\"selected\">".$uf['estado']."</option>";

		} else {

			echo "<option value=\"".$uf['id']."\">".$uf['estado']."</option>";

		}

	}

?>

				</select></td>

		</tr>

		<tr>

			<td width="200" class="linha-salmao-fundo">TItulo Eleitor: </td>

			<td><table width="35%" border="0" cellspacing="0" cellpadding="0">

					<tr>

						<td><input name="tituloeleitornumero" type="text" class="input-destacado" id="tituloeleitornumero" style="width:100%" onKeyPress="return BloqueiaAlfa(event);" value="<?php echo $tituloeleitornumero ?>"maxlength="13"/></td>

						<td width="2">&nbsp;</td>

						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...\n\nCaso n&atilde;o tenha em m&atilde;os, digite somente *!')" value="?" /></td>

					</tr>

				</table></td>

		</tr>

		<tr>

			<td width="200" class="linha-salmao-fundo"><img src="imagens/marcador-normal.png" alt="marcador" width="14" height="14" hspace="2" vspace="0" border="0" align="absmiddle" />Zona:</td>

			<td><table width="15%" border="0" cellspacing="0" cellpadding="0">

					<tr>

						<td><input name="tituloeleitorzona" type="text" class="input-destacado" id="tituloeleitorzona" style="width:100%" onKeyPress="return BloqueiaAlfa(event);" value="<?php echo $tituloeleitorzona ?>"maxlength="4"/></td>

						<td width="2">&nbsp;</td>

						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...\n\nCaso n&atilde;o tenha em m&atilde;os, digite somente *!')" value="?" /></td>

					</tr>

				</table></td>

		</tr>

		<tr>

			<td width="200" class="linha-salmao-fundo"><img src="imagens/marcador-normal.png" alt="marcador" width="14" height="14" hspace="2" vspace="0" border="0" align="absmiddle" />Se&ccedil;&atilde;o:</td>

			<td><table width="15%" border="0" cellspacing="0" cellpadding="0">

					<tr>

						<td><input name="tituloeleitorsecao" type="text" class="input-destacado" id="tituloeleitorsecao" style="width:100%" onKeyPress="return BloqueiaAlfa(event);" value="<?php echo $tituloeleitorsecao ?>" maxlength="4"/></td>

						<td width="2">&nbsp;</td>

						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...\n\nCaso n&atilde;o tenha em m&atilde;os, digite somente *!')" value="?" /></td>

					</tr>

				</table></td>

		</tr>

	</table>

	<?php TopicoCinza("Qualifica&ccedil;&atilde;o escolar"); ?>

	<table width="700" border="0" cellspacing="2" cellpadding="0">

		<tr>

			<td width="200" class="linha-salmao-fundo">Frequenta institui&ccedil;&atilde;o:</td>

			<td><select name="escola_id_escolatipo" id="escola_id_escolatipo" class="input-destacado" style="width:50%" >

					<option value="" selected="selected">Selecione...</option>

					<option value="">-------------------</option>

<?php 

	// enfilera tipo de escola

	$sql = mysql_query("select * from escolatipo order by tipo asc");

	while ($escolatipo = mysql_fetch_array($sql)) {

		if ($escola_id_escolatipo == $escolatipo['id']) {		

			echo "<option value=\"".$escolatipo['id']."\" selected=\"selected\">".$escolatipo['tipo']."</option>";

		} else {

			echo "<option value=\"".$escolatipo['id']."\">".$escolatipo['tipo']."</option>";

		}

	}

?>

				</select></td>

		</tr>

		<tr>

			<td width="200" class="linha-salmao-fundo">Grau de instru&ccedil;&atilde;o:</td>

			<td><select name="escola_id_escolagrau" id="escola_id_escolagrau" class="input-destacado" style="width:100%" >

					<option value="" selected="selected">Selecione...</option>

					<option value="">-------------------</option>

<?php 

	// enfilera os graus escolares

	$sql = mysql_query("select * from escolagrau order by grau asc");

	while ($escolagrau = mysql_fetch_array($sql)) {

		if ($escola_id_escolagrau == $escolagrau['id']) {		

			echo "<option value=\"".$escolagrau['id']."\" selected=\"selected\">".$escolagrau['grau']."</option>";

		} else {

			echo "<option value=\"".$escolagrau['id']."\">".$escolagrau['grau']."</option>";

		}

	}

?>

				</select></td>

		</tr>

		<tr>

			<td width="200" class="linha-salmao-fundo">S&eacute;rie escolar:</td>

			<td><select name="escola_id_escolaserie" id="escola_id_escolaserie" class="input-destacado" style="width:100%" >

					<option value="" selected="selected">Selecione...</option>

					<option value="">-------------------</option>

<?php 

	// enfilera as s&eacute;ries escolares

	$sql = mysql_query("select * from escolaserie");

	while ($escolaserie = mysql_fetch_array($sql)) {

		if ($escola_id_escolaserie == $escolaserie['id']) {		

			echo "<option value=\"".$escolaserie['id']."\" selected=\"selected\">".$escolaserie['serie']."</option>";

		} else {

			echo "<option value=\"".$escolaserie['id']."\">".$escolaserie['serie']."</option>";

		}

	}

?>

				</select></td>

		</tr>

		<tr>

			<td width="200" class="linha-salmao-fundo">Turno:</td>

			<td><select name="escola_id_escolaturno" id="escola_id_escolaturno" class="input-destacado" style="width:50%" >

					<option value="" selected="selected">Selecione...</option>

					<option value="">-------------------</option>

<?php 

	// enfilera turnos

	$sql = mysql_query("select * from escolaturno");

	while ($escolaturno = mysql_fetch_array($sql)) {

		if ($escola_id_escolaturno == $escolaturno['id']) {		

			echo "<option value=\"".$escolaturno['id']."\" selected=\"selected\">".$escolaturno['turno']."</option>";

		} else {

			echo "<option value=\"".$escolaturno['id']."\">".$escolaturno['turno']."</option>";

		}

	}

?>

				</select></td>

		</tr>

		<tr>

			<td width="200" class="linha-salmao-fundo">Nome da institui&ccedil;&atilde;o:</td>

			<td><input name="escola_escolanome" type="text" class="input-destacado" id="escola_escolanome" style="width:100%" onKeyUp="this.value = this.value.toUpperCase();" value="<?php echo $escola_escolanome ?>" maxlength="150"/></td>

		</tr>

		<tr>

			<td class="linha-salmao-fundo">Curso:</td>

			<td><input name="escola_curso" type="text" class="input-destacado" id="escola_curso" style="width:100%" onKeyUp="this.value = this.value.toUpperCase();" value="<?php echo $escola_curso ?>" maxlength="150"/></td>

		</tr>

		<tr>

			<td valign="top" class="textomedio-preto">Cursos Adicionais:</td>

			<td><textarea name="cursos_adicionais" rows="2" class="input-normal" id="cursos_adicionais" style="width:100%" onKeyUp="this.value = this.value.toUpperCase();"><?php echo $cursos_adicionais ?></textarea></td>

		</tr>

	</table>

	<?php TopicoCinza("Dados contratuais"); ?>

	<table width="700" border="0" cellspacing="2" cellpadding="0">

		<tr>

			<td width="200" class="linha-salmao-fundo">Matr&iacute;cula:</td>

			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">

					<tr>

						<td><input name="matricula" type="text" class="input-destacado" id="matricula" style="width:100%" onkeypress="return BloqueiaAlfa(event);" onkeyup="FormataCampo(this, '##/###.###-#', event);" value="<?php echo $matricula ?>" maxlength="12" /></td>

						<td width="2">&nbsp;</td>

						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>

					</tr>

				</table></td>

		</tr>

		<tr>

			<td class="linha-salmao-fundo">Data de admiss&atilde;o:</td>

			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">

					<tr>

						<td><input name="dataadmissao" type="text" class="input-destacado" id="dataadmissao" style="width:100%" onkeypress="return BloqueiaAlfa(event);" onkeyup="FormataCampo(this, '##/##/####', event);" value="<?php echo $dataadmissao ?>" maxlength="10" /></td>

						<td width="2">&nbsp;</td>

						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>

					</tr>

				</table></td>

		</tr>

		<tr>

			<td class="linha-salmao-fundo">Hor&aacute;rio de entrada:</td>

			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">

					<tr>

						<td><input name="horaentrada" type="text" class="input-destacado" id="horaentrada" style="width:100%" onkeypress="return BloqueiaAlfa(event);" onkeyup="FormataCampo(this, '##:##', event);" value="<?php echo $horaentrada ?>" maxlength="5" /></td>

						<td width="2">&nbsp;</td>

						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>

					</tr>

				</table></td>

		</tr>

		<tr>

			<td class="linha-salmao-fundo">Hor&aacute;rio de sa&iacute;da:</td>

			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">

					<tr>

						<td><input name="horasaida" type="text" class="input-destacado" id="horasaida" style="width:100%" onkeypress="return BloqueiaAlfa(event);" onkeyup="FormataCampo(this, '##:##', event);" value="<?php echo $horasaida ?>" maxlength="5" /></td>

						<td width="2">&nbsp;</td>

						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>

					</tr>

				</table></td>

		</tr>

		<tr>

			<td class="linha-salmao-fundo">Unidade atuante:</td>

			<td><select name="codigo_telecentro" id="codigo_telecentro" class="input-destacado" style="width:100%" >

					<option value="" selected="selected">Selecione...</option>

					<option value="">-------------------</option>

<?php 

	// enfilera os graus escolares

	$sql = mysql_query("select * from telecentro order by nome asc");

	while ($telecentro = mysql_fetch_array($sql)) {

		if ($codigo_telecentro == $telecentro['codigo']) {		

			echo "<option value=\"".$telecentro['codigo']."\" selected=\"selected\">".$telecentro['nome']."</option>";

		} else {

			echo "<option value=\"".$telecentro['codigo']."\">".$telecentro['nome']."</option>";

		}

	}

?>

				</select></td>

		</tr>

	</table>

	<?php TopicoCinza("Documentos Entregues"); ?>

	<table width="700" border="0" cellspacing="2" cellpadding="0">

		<tr>

			<td width="200" class="linha-salmao-fundo">Identidade:</td>

			<td><table border="0" cellspacing="0" cellpadding="0">

					<tr>

						<td width="100"><select name="documento_identidade" id="documento_identidade" class="input-destacado" style="width:100%" >

								<option value="" selected="selected">Selecione...</option>

								<option value="">-------------------</option>

<?php 

	for ($x=0;$x<=10;$x++) {

		if ($documento_identidade == $x) {		

			echo "<option value=\"".$x."\" selected=\"selected\">".$x."</option>";

		} else {

			echo "<option value=\"".$x."\">".$x."</option>";

		}

	}

?>

							</select></td>

						<td width="2">&nbsp;</td>

						<td class="linha-salmao-fundodireito">c&oacute;pias&nbsp;</td>

					</tr>

				</table></td>

		</tr>

		<tr>

			<td class="linha-salmao-fundo">CPF:</td>

			<td><table border="0" cellspacing="0" cellpadding="0">

					<tr>

						<td width="100"><select name="documento_cpf" id="documento_cpf" class="input-destacado" style="width:100%" >

								<option value="" selected="selected">Selecione...</option>

								<option value="">-------------------</option>

<?php 

	for ($x=0;$x<=10;$x++) {

		if ($documento_cpf == $x) {		

			echo "<option value=\"".$x."\" selected=\"selected\">".$x."</option>";

		} else {

			echo "<option value=\"".$x."\">".$x."</option>";

		}

	}

?>

							</select></td>

						<td width="2">&nbsp;</td>

						<td class="linha-salmao-fundodireito">c&oacute;pias&nbsp;</td>

					</tr>

				</table></td>

		</tr>

		<tr>

			<td class="linha-salmao-fundo">Comprovante de Resid&ecirc;ncia: </td>

			<td><table border="0" cellspacing="0" cellpadding="0">

					<tr>

						<td width="100"><select name="documento_comprovanteresidencia" id="documento_comprovanteresidencia" class="input-destacado" style="width:100%" >

								<option value="" selected="selected">Selecione...</option>

								<option value="">-------------------</option>

<?php 

	for ($x=0;$x<=10;$x++) {

		if ($documento_comprovanteresidencia == $x) {		

			echo "<option value=\"".$x."\" selected=\"selected\">".$x."</option>";

		} else {

			echo "<option value=\"".$x."\">".$x."</option>";

		}

	}

?>

							</select></td>

						<td width="2">&nbsp;</td>

						<td class="linha-salmao-fundodireito">c&oacute;pias&nbsp;</td>

					</tr>

				</table></td>

		</tr>

		<tr>

			<td class="linha-salmao-fundo">Declara&ccedil;&atilde;o da Faculdade: </td>

			<td><table border="0" cellspacing="0" cellpadding="0">

					<tr>

						<td width="100"><select name="documento_declaracaofaculdade" id="documento_declaracaofaculdade" class="input-destacado" style="width:100%" >

								<option value="" selected="selected">Selecione...</option>

								<option value="">-------------------</option>

<?php 

	for ($x=0;$x<=10;$x++) {

		if ($documento_declaracaofaculdade == $x) {		

			echo "<option value=\"".$x."\" selected=\"selected\">".$x."</option>";

		} else {

			echo "<option value=\"".$x."\">".$x."</option>";

		}

	}

?>

							</select></td>

						<td width="2">&nbsp;</td>

						<td class="linha-salmao-fundodireito">c&oacute;pias&nbsp;</td>

					</tr>

				</table></td>

		</tr>

		<tr>

			<td class="linha-salmao-fundo">Foto 3x4: </td>

			<td><table border="0" cellspacing="0" cellpadding="0">

					<tr>

						<td width="100"><select name="documento_foto" id="documento_foto" class="input-destacado" style="width:100%" >

								<option value="" selected="selected">Selecione...</option>

								<option value="">-------------------</option>

<?php 

	for ($x=0;$x<=10;$x++) {

		if ($documento_foto == $x) {		

			echo "<option value=\"".$x."\" selected=\"selected\">".$x."</option>";

		} else {

			echo "<option value=\"".$x."\">".$x."</option>";

		}

	}

?>

							</select></td>

						<td width="2">&nbsp;</td>

						<td class="linha-salmao-fundodireito">c&oacute;pias&nbsp;</td>

					</tr>

				</table></td>

		</tr>

	</table>

	<br />

	<table width="700" border="0" cellspacing="3" cellpadding="0">

		<tr>

			<td width="35%"><input type="button" class="button-normal" value="Cancelar" onClick="javascript:CarregaPagina('?pm=estagiario','_self')" /></td>

			<td width="65%"><input name="post" type="submit" class="button-destacado" id="post" value="Editar" /></td>

		</tr>

	</table>

</form>

