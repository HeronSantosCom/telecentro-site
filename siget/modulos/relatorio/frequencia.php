<center>
<strong>Rela&ccedil;&atilde;o das turmas conclu&iacute;das</strong><br />
<br />
<?php 
	// lista todos os telecentros
	$sql_telecentro = mysql_query("SELECT * FROM telecentro ORDER BY nome ASC");
	while ($telecentro_atual = mysql_fetch_array($sql_telecentro)) {

		// verifica se existe alguma turma neste telecentro
		$total = RecordCount("turma","codigo_telecentro='".$telecentro_atual["codigo"]."' AND estado='concluido'");
		if ($total > 0) {

			echo "<strong>". $telecentro_atual["nome"] ."</strong><br>";
			// lista todos os cursos
			$sql_curso = mysql_query("SELECT * FROM curso ORDER BY curso ASC");
			while ($curso = mysql_fetch_array($sql_curso)) {
				
				// verifica as turmas
				$total = RecordCount("turma","codigo_telecentro='".$telecentro_atual["codigo"]."' AND codigo LIKE '".$curso["codigo"]."%' AND estado='concluido'");
				if ($total > 0) {

					// lista todas as turmas
					$sql_turma = mysql_query("SELECT * FROM turma WHERE codigo_telecentro='".$telecentro_atual["codigo"]."' AND codigo LIKE '".$curso["codigo"]."%' AND estado='concluido' ORDER BY datainicio DESC");
					while ($turma = mysql_fetch_array($sql_turma)) {
						
						// limpa variaveis
						unset ($array_presenca);
						$CargaHoraria = 0;
						
						// calcula a carga horaria
						$sql_curso_modulo = mysql_query("SELECT * FROM curso_modulo WHERE id_curso=".$curso["id"]."");
						while ($curso_modulo = mysql_fetch_array($sql_curso_modulo)) {
							$CargaHoraria += RecordSum("modulo","cargahoraria","id=".$curso_modulo["id_modulo"]."");
						}

						$id_turma = $turma["id"];
						$id_curso = $turma["id_curso"];
						$codigo_telecentro = $turma["codigo_telecentro"];
						$codigo = $turma["codigo"];
						$dataInicio = ConverteData($turma["datainicio"], "/", "mysql.normal");
						$dataTermino = ConverteData($turma["datatermino"], "/", "mysql.normal");
						$estado = $turma["estado"];
						// gera o codigo da turma
						$codigo_turma = (substr("$codigo", 0, 3)."-".substr("$codigo", 3, 2).substr("$codigo", 7, 5).".".substr("$codigo", 5, 2));
						
						// verifica a carga horaria cumprida
						$sql_turma_usuario = mysql_query("SELECT * FROM turma_usuario WHERE codigo_turma='$codigo' ORDER BY matricula_usuario ASC");
						while($turma_usuario = mysql_fetch_array($sql_turma_usuario)) {
							
							// limpa variaveis
							$diaatual = 0;
							$falta = 0;
							$presenca = 0;
							$totaldias = 0;
							$CargaHorariaCumprida = 0;
							
							// pega informacoes dos usuarios
							$sql_usuario = mysql_query("SELECT * FROM usuario WHERE matricula='". $turma_usuario["matricula_usuario"]."'");
							$usuario = mysql_fetch_array($sql_usuario);
							$nome_usuario = $usuario["nome"];
							$matricula_usuario = $usuario["matricula"];
							$CargaHorariaTotal = $CargaHoraria;
							
							// conta dias
							$total_turma_grade = RecordCount("turma_grade","codigo_turma='$codigo'");
							if ($total_turma_grade > 0 ) {
								
								// CargaHorariaTotal for igual a 0 diaatual = 274
								if ($CargaHorariaTotal == 0) { $diaatual = 274; }
								while ($CargaHorariaTotal > 0) {
									$data = SomarData($dataInicio, $diaatual, 0, 0);
									$mostraData = $data;
									$data = explode("/", $data);
									$codigoSemana = ConversorUniversal(date('D',mktime(0, 0, 0, $data[1], $data[0], $data[2])), "semanacurta.num");
									$sql = mysql_query("SELECT * FROM turma_grade WHERE codigo_turma='$codigo'");
									while ($turma_grade = mysql_fetch_array($sql)) { 
										if ($turma_grade["diasemana"] == $codigoSemana) {
											$horainicio = $turma_grade["horainicio"];
											$horafim = $turma_grade["horafim"];
											$nDiff = strtotime($horafim) - strtotime($horainicio);
											$nHour = round($nDiff / 3600, 1);
											$CargaHorariaTotal -= $nHour;
											$total_diariodeclasse_presenca = RecordCount("diariodeclasse_presenca","codigo_turma='$codigo' AND matricula_usuario='".$usuario["matricula"]."' AND data='".ConverteData($mostraData, "-", "normal.mysql")."'");
											if ($total_diariodeclasse_presenca > 0) { $presenca++; $CargaHorariaCumprida += $nHour; } else { $falta++; }
											$totaldias++;
										}
									}
									$diaatual += 1;
								}
							}
							if ($falta > 0) {
								$porcfalta = round((($falta*100)/$totaldias), 1);
							} else {
								$porcfalta = 100;
							}
							$array_presenca[] = array('nome' => $nome_usuario, 'matricula' => $matricula_usuario, 'ch' => $CargaHorariaCumprida, 'falta' => $porcfalta);
							//echo "$matricula_usuario - $nome_usuario ->  $falta / $presenca = $totaldias ($CargaHorariaCumprida horas)<br>";
						}
?>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="200"><strong>Curso: </strong></td>
			<td><?php echo $curso["curso"] ?></td>
		</tr>
		<tr>
			<td width="200"><strong>C&oacute;digo da turma: </strong></td>
			<td><?php echo $codigo_turma ?></td>
		</tr>
		<tr>
			<td width="200"><strong>In&iacute;cio das aulas: </strong></td>
			<td><?php echo $dataInicio ?></td>
		</tr>
		<tr>
			<td width="200"><strong>T&eacute;rmino das aulas: </strong></td>
			<td><?php echo $dataTermino ?></td>
		</tr>
		<tr>
			<td><strong>Carga hor&aacute;ria do curso: </strong></td>
			<td><?php echo $CargaHoraria ?> horas</td>
		</tr>
	</table>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td><strong>Nome</strong></td>
			<td width="125" align="center"><strong>Matr&iacute;cula</strong></td>
			<td width="110" align="right"><strong>C.H. Cumprida</strong></td>
			<td width="100" align="right"><strong>% de Falta</strong></td>
		</tr>
<?php
						foreach ($array_presenca as $key => $row) {
?>
		<tr>
			<td><?php echo $row['nome'] ?></td>
			<td width="125" align="center"><?php echo $row['matricula'] ?></td>
			<td width="110" align="right"><?php echo $row['ch'] ?> h</td>
			<td width="100" align="right"><?php echo $row['falta'] ?> %</td>
		</tr>
<?php 
						}
?>
	</table>
<?php 
						// exibe as observacoes
						$total_diariodeclasse_observacao = RecordCount("diariodeclasse_observacao","codigo_turma='$codigo'");
						if ($total_diariodeclasse_observacao > 0 ) {
?>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="2"><strong>Observa&ccedil;&otilde;es</strong></td>
		</tr>
		<tr>
			<td width="150" align="center"><strong>Data</strong></td>
			<td><strong>Observa&ccedil;&atilde;o</strong></td>
		</tr>
<?php 
							$sql_diariodeclasse_observacao = mysql_query("SELECT * FROM diariodeclasse_observacao WHERE codigo_turma='$codigo' ORDER BY data ASC");
							while ($turma_diariodeclasse_observacao = mysql_fetch_array($sql_diariodeclasse_observacao)) { 
?>
		<tr>
			<td align="center" valign="top"><?php echo ConverteData($turma_diariodeclasse_observacao['data'], "/", "mysql.normal") ?></td>
			<td valign="top"><?php echo $turma_diariodeclasse_observacao['observacao'] ?></td>
		</tr>
<?php 
							}
?>
	</table>
<?php 
						}
?>
	<hr />
<?php
					}
				}
			}
			echo "<br /><br /><br />";
		}
	}
	$total_turma = RecordCount("turma","estado='concluido'");
	if ($total_turma == 0) {
		echo "Nenhuma turma concluida...";
	}
?>
</center>
