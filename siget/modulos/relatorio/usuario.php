<center>
<strong>Estat&iacute;stica geral dos usu&aacute;rios cadastrados</strong><br />
<br />
<?php 
	$seleciona = $_GET["seleciona"];
	$total_usuario = RecordCount("usuario","");
	$total_documentos_nis = RecordCount("usuario","nis IS NOT NULL OR nis>0");
	$porcentagem_documentos_nis = round((($total_documentos_nis*100)/$total_usuario), 2);
	$total_documentos_cpf = RecordCount("usuario","cpf=0");
	$porcentagem_documentos_cpf = round((($total_documentos_cpf*100)/$total_usuario), 2);
	$total_documentos_identidade = RecordCount("usuario","identidadenumero=0");
	$porcentagem_documentos_identidade = round((($total_documentos_identidade*100)/$total_usuario), 2);
	$total_documentos_ctps = RecordCount("usuario","ctpsnumero=0");
	$porcentagem_documentos_ctps = round((($total_documentos_ctps*100)/$total_usuario), 2);
	$total_documentos_tituloeleitor = RecordCount("usuario","tituloeleitornumero=0");
	$porcentagem_documentos_tituloeleitor = round((($total_documentos_tituloeleitor*100)/$total_usuario), 2);
?>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td>&nbsp;</td>
		<td width="100" align="center">TOTAL</td>
		<td width="100" align="center">%</td>
	</tr>
	<tr>
		<td>Total de usu&aacute;rios:</td>
		<td align="center"><?php echo $total_usuario; ?></td>
		<td align="center">100%</td>
	</tr>
	<tr>
		<td>Com N&uacute;mero de Inscri&ccedil;&atilde;o Social (NIS):</td>
		<td align="center"><?php echo $total_documentos_nis ?></td>
		<td align="center"><?php echo $porcentagem_documentos_nis." %"; ?></td>
	</tr>
	<tr>
		<td>CPF n&atilde;o preenchida:</td>
		<td align="center"><?php echo $total_documentos_cpf ?></td>
		<td align="center"><?php echo $porcentagem_documentos_cpf." %"; ?></td>
	</tr>
	<tr>
		<td>Identidade  n&atilde;o preenchida:</td>
		<td align="center"><?php echo $total_documentos_identidade ?></td>
		<td align="center"><?php echo $porcentagem_documentos_identidade." %"; ?></td>
	</tr>
	<tr>
		<td>CTPS n&atilde;o preenchida:</td>
		<td align="center"><?php echo $total_documentos_ctps ?></td>
		<td align="center"><?php echo $porcentagem_documentos_ctps." %"; ?></td>
	</tr>
	<tr>
		<td>T&iacute;tulo de Eleitor  n&atilde;o preenchida:</td>
		<td align="center"><?php echo $total_documentos_tituloeleitor ?></td>
		<td align="center"><?php echo $porcentagem_documentos_tituloeleitor." %"; ?></td>
	</tr>
</table>
<br />
<?php
	$acumulo_total = 0;
	$acumulo_porcentagem = 0;
	$sql_telecentro = mysql_query("SELECT * FROM telecentro ORDER BY nome ASC");
	while ($telecentro = mysql_fetch_array($sql_telecentro)) {
		$total = RecordCount("usuario","codigo_telecentro=".$telecentro["codigo"]);
		$array_telecentro[] = array('id' => $telecentro["id"], 'total' => $total);
	}
?>
<strong>Por  Telecentro</strong>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td>&nbsp;</td>
		<td width="100" align="center">TOTAL</td>
		<td width="100" align="center">%</td>
	</tr>
<?php 
	foreach ($array_telecentro as $key => $row) {
		$total = $row['total'];
		$porcentagem = round((($total*100)/$total_usuario), 2);
		$sql_telecentro = mysql_query("SELECT * FROM telecentro WHERE id=". $row['id']);
		if ($telecentro = mysql_fetch_array($sql_telecentro)) {
			$linha = $telecentro["nome"];
?>
	<tr>
		<td><?php echo $linha ?>:</td>
		<td align="center"><?php echo $total ?></td>
		<td align="center"><?php echo $porcentagem." %"; ?></td>
	</tr>
<?php 
		}
		$acumulo_total += $total;
		$acumulo_porcentagem += $porcentagem;
	}
?>
	<tr>
		<td><font color="#FF0000">Total:</font></td>
		<td align="center"><font color="#FF0000"><?php echo $acumulo_total ?></font></td>
		<td align="center"><font color="#FF0000"><?php echo $acumulo_porcentagem." %"; ?></font></td>
	</tr>
</table>
<br />
<?php
	$acumulo_total = 0;
	$acumulo_porcentagem = 0;
	$sql_tipousuario = mysql_query("SELECT * FROM tipousuario");
	while ($tipousuario = mysql_fetch_array($sql_tipousuario)) {
		$total = RecordCount("usuario","id_tipousuario=".$tipousuario["id"]);
		$array_tipousuario[] = array('id' => $tipousuario["id"], 'total' => $total);
	}
?>
<strong>Por Tipo de Usu&aacute;rio</strong>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td>&nbsp;</td>
		<td width="100" align="center">TOTAL</td>
		<td width="100" align="center">%</td>
	</tr>
<?php 
	foreach ($array_tipousuario as $key => $row) {
		$total = $row['total'];
		$porcentagem = round((($total*100)/$total_usuario), 2);
		$sql_tipousuario = mysql_query("SELECT * FROM tipousuario WHERE id=". $row['id']);
		if ($tipousuario = mysql_fetch_array($sql_tipousuario)) {
			$linha = $tipousuario["tipousuario"];
?>
	<tr>
		<td><?php echo $linha ?>:</td>
		<td align="center"><?php echo $total ?></td>
		<td align="center"><?php echo $porcentagem." %"; ?></td>
	</tr>
<?php 
		}
		$acumulo_total += $total;
		$acumulo_porcentagem += $porcentagem;
	}
?>
	<tr>
		<td><font color="#FF0000">Total:</font></td>
		<td align="center"><font color="#FF0000"><?php echo $acumulo_total ?></font></td>
		<td align="center"><font color="#FF0000"><?php echo $acumulo_porcentagem." %"; ?></font></td>
	</tr>
</table>
<br />
<?php
	$acumulo_total = 0;
	$acumulo_porcentagem = 0;
	$sql_usuario = mysql_query("SELECT * FROM usuario");
	$x = 0;
	while ($usuario = mysql_fetch_array($sql_usuario)) {
		$dia_nasc = substr($usuario["datanascimento"], 8, 2);
		$mes_nasc = substr($usuario["datanascimento"], 5, 2);
		$ano_nasc = substr($usuario["datanascimento"], 0, 4);
		$idade = CalculaIdade($dia_nasc, $mes_nasc, $ano_nasc);
		$array_usuario[] = array('id' => $usuario["id"], 'idade' => $idade);
		$x++;
	}
	array_multisort($array_usuario, SORT_ASC);
	$total_idade_0 = 0;
	$total_idade_1_13 = 0;
	$total_idade_14_24 = 0;
	$total_idade_25_34 = 0;
	$total_idade_35_54 = 0;
	$total_idade_55_64 = 0;
	$total_idade_65_99 = 0;
	$total_idade_100 = 0;
	foreach ($array_usuario as $key => $row) {
		$idade = $row["idade"];
		if ($row["idade"] <= 0) { $total_idade_0++; }
		if ($row["idade"]>= 1 && $row["idade"] <= 13) { $total_idade_1_13++; }
		if ($row["idade"]>= 14 && $row["idade"] <= 24) { $total_idade_14_24++; }
		if ($row["idade"]>= 25 && $row["idade"] <= 34) { $total_idade_25_34++; }
		if ($row["idade"]>= 35 && $row["idade"] <= 54) { $total_idade_35_54++; }
		if ($row["idade"]>= 55 && $row["idade"] <= 64) { $total_idade_55_64++; }
		if ($row["idade"]>= 65 && $row["idade"] <= 99) { $total_idade_65_99++; }
		if ($row["idade"]>= 100) { $total_idade_100++; }
	}
	$porcen_idade_0 = round((($total_idade_0*100)/$total_usuario), 2);
	$porcen_idade_1_13 = round((($total_idade_1_13*100)/$total_usuario), 2);
	$porcen_idade_14_24 = round((($total_idade_14_24*100)/$total_usuario), 2);
	$porcen_idade_25_34 = round((($total_idade_25_34*100)/$total_usuario), 2);
	$porcen_idade_35_54 = round((($total_idade_35_54*100)/$total_usuario), 2);
	$porcen_idade_55_64 = round((($total_idade_55_64*100)/$total_usuario), 2);
	$porcen_idade_65_99 = round((($total_idade_65_99*100)/$total_usuario), 2);
	$porcen_idade_100 = round((($total_idade_100*100)/$total_usuario), 2);
	
	$acumulo_total = ($total_idade_0 + $total_idade_1_13 + $total_idade_14_24 + $total_idade_25_34 + $total_idade_35_54 + $total_idade_55_64 + $total_idade_65_99 + $total_idade_100);
	$acumulo_porcentagem = round(($porcen_idade_0 + $porcen_idade_1_13 + $porcen_idade_14_24 + $porcen_idade_25_34 + $porcen_idade_35_54 + $porcen_idade_55_64 + $porcen_idade_65_99 + $porcen_idade_100), 2);
?>
<strong>Por Faixa Et&aacute;ria</strong>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td>&nbsp;</td>
		<td width="100" align="center">TOTAL</td>
		<td width="100" align="center">%</td>
	</tr>
	<tr>
		<td>1 ~ 13 anos:</td>
		<td align="center"><?php echo $total_idade_1_13 ?></td>
		<td align="center"><?php echo $porcen_idade_1_13." %"; ?></td>
	</tr>
	<tr>
		<td>14 ~ 24 anos:</td>
		<td align="center"><?php echo $total_idade_14_24 ?></td>
		<td align="center"><?php echo $porcen_idade_14_24." %"; ?></td>
	</tr>
	<tr>
		<td>25 ~ 34 anos:</td>
		<td align="center"><?php echo $total_idade_25_34 ?></td>
		<td align="center"><?php echo $porcen_idade_25_34." %"; ?></td>
	</tr>
	<tr>
		<td>35 ~ 54 anos:</td>
		<td align="center"><?php echo $total_idade_35_54 ?></td>
		<td align="center"><?php echo $porcen_idade_35_54." %"; ?></td>
	</tr>
	<tr>
		<td>55 ~ 64 anos:</td>
		<td align="center"><?php echo $total_idade_55_64 ?></td>
		<td align="center"><?php echo $porcen_idade_55_64." %"; ?></td>
	</tr>
	<tr>
		<td>65 ~ 99 anos:</td>
		<td align="center"><?php echo $total_idade_65_99 ?></td>
		<td align="center"><?php echo $porcen_idade_65_99." %"; ?></td>
	</tr>
	<tr>
		<td>Maior de 100 anos:</td>
		<td align="center"><?php echo $total_idade_100 ?></td>
		<td align="center"><?php echo $porcen_idade_100." %"; ?></td>
	</tr>
	<tr>
		<td>Outros:</td>
		<td align="center"><?php echo $total_idade_0 ?></td>
		<td align="center"><?php echo $porcen_idade_0." %"; ?></td>
	</tr>
	<tr>
		<td><font color="#FF0000">Total:</font></td>
		<td align="center"><font color="#FF0000"><?php echo $acumulo_total ?></font></td>
		<td align="center"><font color="#FF0000"><?php echo $acumulo_porcentagem." %"; ?></font></td>
	</tr>
</table>
<br />
<?php
	$acumulo_total = 0;
	$acumulo_porcentagem = 0;
	$sql_sexo = mysql_query("SELECT * FROM sexo ORDER BY sexo ASC");
	while ($sexo = mysql_fetch_array($sql_sexo)) {
		$total = RecordCount("usuario","id_sexo=".$sexo["id"]);
		$array_sexo[] = array('id' => $sexo["id"], 'total' => $total);
	}
?>
<strong>Por Sexo</strong>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td>&nbsp;</td>
		<td width="100" align="center">TOTAL</td>
		<td width="100" align="center">%</td>
	</tr>
<?php 
	foreach ($array_sexo as $key => $row) {
		$total = $row['total'];
		$porcentagem = round((($total*100)/$total_usuario), 2);
		$sql_sexo = mysql_query("SELECT * FROM sexo WHERE id=". $row['id']);
		if ($sexo = mysql_fetch_array($sql_sexo)) {
			$linha = $sexo["sexo"];
?>
	<tr>
		<td><?php echo $linha ?>:</td>
		<td align="center"><?php echo $total ?></td>
		<td align="center"><?php echo $porcentagem." %"; ?></td>
	</tr>
<?php 
		}
		$acumulo_total += $total;
		$acumulo_porcentagem += $porcentagem;
	}
?>
	<tr>
		<td><font color="#FF0000">Total:</font></td>
		<td align="center"><font color="#FF0000"><?php echo $acumulo_total ?></font></td>
		<td align="center"><font color="#FF0000"><?php echo $acumulo_porcentagem." %"; ?></font></td>
	</tr>
</table>
<br />
<?php
	$acumulo_total = 0;
	$acumulo_porcentagem = 0;
	$sql_nacionalidade = mysql_query("SELECT * FROM nacionalidade");
	while ($nacionalidade = mysql_fetch_array($sql_nacionalidade)) {
		$total = RecordCount("usuario","id_nacionalidade=".$nacionalidade["id"]);
		$array_nacionalidade[] = array('id' => $nacionalidade["id"], 'total' => $total);
	}
?>
<strong>Por Nacionalidade</strong>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td>&nbsp;</td>
		<td width="100" align="center">TOTAL</td>
		<td width="100" align="center">%</td>
	</tr>
<?php 
	foreach ($array_nacionalidade as $key => $row) {
		$total = $row['total'];
		$porcentagem = round((($total*100)/$total_usuario), 2);
		$sql_nacionalidade = mysql_query("SELECT * FROM nacionalidade WHERE id=". $row['id']);
		if ($nacionalidade = mysql_fetch_array($sql_nacionalidade)) {
			$linha = $nacionalidade["nacionalidade"];
?>
	<tr>
		<td><?php echo $linha ?>:</td>
		<td align="center"><?php echo $total ?></td>
		<td align="center"><?php echo $porcentagem." %"; ?></td>
	</tr>
<?php 
		}
		$acumulo_total += $total;
		$acumulo_porcentagem += $porcentagem;
	}
?>
	<tr>
		<td><font color="#FF0000">Total:</font></td>
		<td align="center"><font color="#FF0000"><?php echo $acumulo_total ?></font></td>
		<td align="center"><font color="#FF0000"><?php echo $acumulo_porcentagem." %"; ?></font></td>
	</tr>
</table>
<br />
<?php
	$acumulo_total = 0;
	$acumulo_porcentagem = 0;
	$sql_estadocivil = mysql_query("SELECT * FROM estadocivil");
	while ($estadocivil = mysql_fetch_array($sql_estadocivil)) {
		$total = RecordCount("usuario","id_estadocivil=".$estadocivil["id"]);
		$array_estadocivil[] = array('id' => $estadocivil["id"], 'total' => $total);
	}
?>
<strong>Por Estado Civil</strong>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td>&nbsp;</td>
		<td width="100" align="center">TOTAL</td>
		<td width="100" align="center">%</td>
	</tr>
<?php 
	foreach ($array_estadocivil as $key => $row) {
		$total = $row['total'];
		$porcentagem = round((($total*100)/$total_usuario), 2);
		$sql_estadocivil = mysql_query("SELECT * FROM estadocivil WHERE id=". $row['id']);
		if ($estadocivil = mysql_fetch_array($sql_estadocivil)) {
			$linha = $estadocivil["estadocivil"];
?>
	<tr>
		<td><?php echo $linha ?>:</td>
		<td align="center"><?php echo $total ?></td>
		<td align="center"><?php echo $porcentagem." %"; ?></td>
	</tr>
<?php 
		}
		$acumulo_total += $total;
		$acumulo_porcentagem += $porcentagem;
	}
?>
	<tr>
		<td><font color="#FF0000">Total:</font></td>
		<td align="center"><font color="#FF0000"><?php echo $acumulo_total ?></font></td>
		<td align="center"><font color="#FF0000"><?php echo $acumulo_porcentagem." %"; ?></font></td>
	</tr>
</table>
<br />
<?php
	$acumulo_total = 0;
	$acumulo_porcentagem = 0;
	$sql_raca = mysql_query("SELECT * FROM raca");
	while ($raca = mysql_fetch_array($sql_raca)) {
		$contador = mysql_query("SELECT COUNT(*) AS valor FROM usuario WHERE id_raca=".$raca["id"]);
		$total = mysql_result($contador, 0, "valor");
		$array_raca[] = array('id' => $raca["id"], 'total' => $total);
	}
?>
<strong>Por Ra&ccedil;a</strong>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td>&nbsp;</td>
		<td width="100" align="center">TOTAL</td>
		<td width="100" align="center">%</td>
	</tr>
<?php 
	foreach ($array_raca as $key => $row) {
		$total = $row['total'];
		$porcentagem = round((($total*100)/$total_usuario), 2);
		$sql_raca = mysql_query("SELECT * FROM raca WHERE id=". $row['id']);
		if ($raca = mysql_fetch_array($sql_raca)) {
			$linha = $raca["raca"];
?>
	<tr>
		<td><?php echo $linha ?>:</td>
		<td align="center"><?php echo $total ?></td>
		<td align="center"><?php echo $porcentagem." %"; ?></td>
	</tr>
<?php 
		}
		$acumulo_total += $total;
		$acumulo_porcentagem += $porcentagem;
	}
?>
	<tr>
		<td><font color="#FF0000">Total:</font></td>
		<td align="center"><font color="#FF0000"><?php echo $acumulo_total ?></font></td>
		<td align="center"><font color="#FF0000"><?php echo $acumulo_porcentagem." %"; ?></font></td>
	</tr>
</table>
<br />
<?php
	$total_usuario_deficiencia = 0;
	$acumulo_total = 0;
	$acumulo_porcentagem = 0;
	$total_deficiencia = RecordCount("usuario_deficiencia","assinalado='sim'");
	$sql_deficiencia = mysql_query("SELECT * FROM deficiencia");
	while ($deficiencia = mysql_fetch_array($sql_deficiencia)) {
		$contador = mysql_query("SELECT COUNT(*) AS valor FROM usuario_deficiencia WHERE id_deficiencia=".$deficiencia["id"]." AND assinalado='sim'");
		$total = mysql_result($contador, 0, "valor");
		$array_deficiencia[] = array('id' => $deficiencia["id"], 'total' => $total);
		$total_usuario_deficiencia += $total;
	}
?>
<strong>Por Tipo de Defici&ecirc;ncia</strong>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td>&nbsp;</td>
		<td width="100" align="center">TOTAL</td>
		<td width="100" align="center">%</td>
	</tr>
<?php 
	foreach ($array_deficiencia as $key => $row) {
		$total = $row['total'];
		$porcentagem = round((($total*100)/$total_usuario_deficiencia), 2);
		$sql_deficiencia = mysql_query("SELECT * FROM deficiencia WHERE id=". $row['id']);
		if ($deficiencia = mysql_fetch_array($sql_deficiencia)) {
			$linha = $deficiencia["deficiencia"];
?>
	<tr>
		<td><?php echo $linha ?>:</td>
		<td align="center"><?php echo $total ?></td>
		<td align="center"><?php echo $porcentagem." %"; ?></td>
	</tr>
<?php 
		}
		$acumulo_total += $total;
		$acumulo_porcentagem += $porcentagem;
	}
?>
	<tr>
		<td><font color="#FF0000">Total:</font></td>
		<td align="center"><font color="#FF0000"><?php echo $acumulo_total ?></font></td>
		<td align="center"><font color="#FF0000"><?php echo $acumulo_porcentagem." %"; ?></font></td>
	</tr>
</table>
<br />
<?php
	$acumulo_total = 0;
	$acumulo_porcentagem = 0;
	$sql_urg = mysql_query("SELECT * FROM urg");
	while ($urg = mysql_fetch_array($sql_urg)) {
		$contador = mysql_query("SELECT COUNT(*) AS valor FROM usuario WHERE endereco_id_bairro IN(SELECT b.id FROM bairro b WHERE b.id_urg IN(SELECT u.id FROM urg u WHERE id = ".$urg['id']."))");
		$total = mysql_result($contador, 0, "valor");
		$array_urg[] = array('id' => $urg["id"], 'total' => $total);
	}
?>
<strong>Por Unidades Regionais de Governo</strong>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td>&nbsp;</td>
		<td width="100" align="center">TOTAL</td>
		<td width="100" align="center">%</td>
	</tr>
<?php 
	foreach ($array_urg as $key => $row) {
		$total = $row['total'];
		$porcentagem = round((($total*100)/$total_usuario), 2);
		$sql_urg = mysql_query("SELECT * FROM urg WHERE id=". $row['id']);
		if ($urg = mysql_fetch_array($sql_urg)) {
			$linha = $urg["urg"];
?>
	<tr>
		<td><?php echo $linha ?>:</td>
		<td align="center"><?php echo $total ?></td>
		<td align="center"><?php echo $porcentagem." %"; ?></td>
	</tr>
<?php 
		}
		$acumulo_total += $total;
		$acumulo_porcentagem += $porcentagem;
	}
?>
	<tr>
		<td><font color="#FF0000">Total:</font></td>
		<td align="center"><font color="#FF0000"><?php echo $acumulo_total ?></font></td>
		<td align="center"><font color="#FF0000"><?php echo $acumulo_porcentagem." %"; ?></font></td>
	</tr>
</table>
<br />
<?php
	$acumulo_total = 0;
	$acumulo_porcentagem = 0;
	$sql_bairro = mysql_query("SELECT * FROM bairro");
	while ($bairro = mysql_fetch_array($sql_bairro)) {
		$contador = mysql_query("SELECT COUNT(*) AS valor FROM usuario WHERE endereco_id_bairro=".$bairro["id"]);
		$total = mysql_result($contador, 0, "valor");
		$array_bairro[] = array('id' => $bairro["id"], 'total' => $total);
	}
?>
<strong>Por Bairro</strong>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td>&nbsp;</td>
		<td width="100" align="center">TOTAL</td>
		<td width="100" align="center">%</td>
	</tr>
<?php 
	foreach ($array_bairro as $key => $row) {
		$total = $row['total'];
		$porcentagem = round((($total*100)/$total_usuario), 2);
		$sql_bairro = mysql_query("SELECT * FROM bairro WHERE id=". $row['id']);
		if ($bairro = mysql_fetch_array($sql_bairro)) {
			$linha = $bairro["bairro"];
?>
	<tr>
		<td><?php echo $linha ?>:</td>
		<td align="center"><?php echo $total ?></td>
		<td align="center"><?php echo $porcentagem." %"; ?></td>
	</tr>
<?php 
		}
		$acumulo_total += $total;
		$acumulo_porcentagem += $porcentagem;
	}
?>
	<tr>
		<td><font color="#FF0000">Total:</font></td>
		<td align="center"><font color="#FF0000"><?php echo $acumulo_total ?></font></td>
		<td align="center"><font color="#FF0000"><?php echo $acumulo_porcentagem." %"; ?></font></td>
	</tr>
</table>
<br />
<?php
	$acumulo_total = 0;
	$acumulo_porcentagem = 0;
	$sql_domiciliotipo = mysql_query("SELECT * FROM domiciliotipo");
	while ($domiciliotipo = mysql_fetch_array($sql_domiciliotipo)) {
		$contador = mysql_query("SELECT COUNT(*) AS valor FROM usuario WHERE domicilio_id_domiciliotipo=".$domiciliotipo["id"]);
		$total = mysql_result($contador, 0, "valor");
		$array_domiciliotipo[] = array('id' => $domiciliotipo["id"], 'total' => $total);
	}
	$contador = mysql_query("SELECT COUNT(*) AS valor FROM usuario WHERE domicilio_id_domiciliotipo=0");
	$total = mysql_result($contador, 0, "valor");
	$array_domiciliotipo[] = array('id' => 0, 'total' => $total);
?>
<strong>Por Tipo de Domic&iacute;lio</strong></font>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td>&nbsp;</td>
		<td width="100" align="center">TOTAL</td>
		<td width="100" align="center">%</td>
	</tr>
<?php 
	foreach ($array_domiciliotipo as $key => $row) {
		$total = $row['total'];
		$porcentagem = round((($total*100)/$total_usuario), 2);
		$sql_domiciliotipo = mysql_query("SELECT * FROM domiciliotipo WHERE id=". $row['id']);
		if ($domiciliotipo = mysql_fetch_array($sql_domiciliotipo)) {
			$linha = $domiciliotipo["tipocasa"];
?>
	<tr>
		<td><?php echo $linha ?>:</td>
		<td align="center"><?php echo $total ?></td>
		<td align="center"><?php echo $porcentagem." %"; ?></td>
	</tr>
<?php 
		}
		if ($row['id'] == 0) {
			$linha = "N&atilde;o responderam";
?>
	<tr>
		<td><?php echo $linha ?>:</td>
		<td align="center"><?php echo $total ?></td>
		<td align="center"><?php echo $porcentagem." %"; ?></td>
	</tr>
<?php 
		}
		$acumulo_total += $total;
		$acumulo_porcentagem += $porcentagem;
	}
?>
	<tr>
		<td><font color="#FF0000">Total:</font></td>
		<td align="center"><font color="#FF0000"><?php echo $acumulo_total ?></font></td>
		<td align="center"><font color="#FF0000"><?php echo $acumulo_porcentagem." %"; ?></font></td>
	</tr>
</table>
<br />
<?php
	$acumulo_total = 0;
	$acumulo_porcentagem = 0;
	$sql_domiciliotipoconstrucao = mysql_query("SELECT * FROM domiciliotipoconstrucao");
	while ($domiciliotipoconstrucao = mysql_fetch_array($sql_domiciliotipoconstrucao)) {
		$contador = mysql_query("SELECT COUNT(*) AS valor FROM usuario WHERE domicilio_id_domicilioconstrucao=".$domiciliotipoconstrucao["id"]);
		$total = mysql_result($contador, 0, "valor");
		$array_domiciliotipoconstrucao[] = array('id' => $domiciliotipoconstrucao["id"], 'total' => $total);
	}
	$contador = mysql_query("SELECT COUNT(*) AS valor FROM usuario WHERE domicilio_id_domicilioconstrucao=0");
	$total = mysql_result($contador, 0, "valor");
	$array_domiciliotipoconstrucao[] = array('id' => 0, 'total' => $total);
?>
<strong>Por Tipo de Constru&ccedil;&atilde;o</strong></font>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td>&nbsp;</td>
		<td width="100" align="center">TOTAL</td>
		<td width="100" align="center">%</td>
	</tr>
<?php 
	foreach ($array_domiciliotipoconstrucao as $key => $row) {
		$total = $row['total'];
		$porcentagem = round((($total*100)/$total_usuario), 2);
		$sql_domiciliotipoconstrucao = mysql_query("SELECT * FROM domiciliotipoconstrucao WHERE id=". $row['id']);
		if ($domiciliotipoconstrucao = mysql_fetch_array($sql_domiciliotipoconstrucao)) {
			$linha = $domiciliotipoconstrucao["construcao"];
?>
	<tr>
		<td><?php echo $linha ?>:</td>
		<td align="center"><?php echo $total ?></td>
		<td align="center"><?php echo $porcentagem." %"; ?></td>
	</tr>
<?php 
		}
		if ($row['id'] == 0) {
			$linha = "N�o responderam";
?>
	<tr>
		<td><?php echo $linha ?>:</td>
		<td align="center"><?php echo $total ?></td>
		<td align="center"><?php echo $porcentagem." %"; ?></td>
	</tr>
<?php 
		}
		$acumulo_total += $total;
		$acumulo_porcentagem += $porcentagem;
	}
?>
	<tr>
		<td><font color="#FF0000">Total:</font></td>
		<td align="center"><font color="#FF0000"><?php echo $acumulo_total ?></font></td>
		<td align="center"><font color="#FF0000"><?php echo $acumulo_porcentagem." %"; ?></font></td>
	</tr>
</table>
<br />
<?php
	$acumulo_total = 0;
	$acumulo_porcentagem = 0;
	$sql_escolatipo = mysql_query("SELECT * FROM escolatipo");
	while ($escolatipo = mysql_fetch_array($sql_escolatipo)) {
		$contador = mysql_query("SELECT COUNT(*) AS valor FROM usuario WHERE escola_id_escolatipo=".$escolatipo["id"]);
		$total = mysql_result($contador, 0, "valor");
		$array_escolatipo[] = array('id' => $escolatipo["id"], 'total' => $total);
	}
?>
<strong>Por Tipo de Escola Freq&uuml;&ecirc;ntada</strong></font>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td>&nbsp;</td>
		<td width="100" align="center">TOTAL</td>
		<td width="100" align="center">%</td>
	</tr>
<?php 
	foreach ($array_escolatipo as $key => $row) {
		$total = $row['total'];
		$porcentagem = round((($total*100)/$total_usuario), 2);
		$sql_escolatipo = mysql_query("SELECT * FROM escolatipo WHERE id=". $row['id']);
		if ($escolatipo = mysql_fetch_array($sql_escolatipo)) {
			$linha = $escolatipo["tipo"];
?>
	<tr>
		<td><?php echo $linha ?>:</td>
		<td align="center"><?php echo $total ?></td>
		<td align="center"><?php echo $porcentagem." %"; ?></td>
	</tr>
<?php 
		}
		$acumulo_total += $total;
		$acumulo_porcentagem += $porcentagem;
	}
?>
	<tr>
		<td><font color="#FF0000">Total:</font></td>
		<td align="center"><font color="#FF0000"><?php echo $acumulo_total ?></font></td>
		<td align="center"><font color="#FF0000"><?php echo $acumulo_porcentagem." %"; ?></font></td>
	</tr>
</table>
<br />
<?php
	$acumulo_total = 0;
	$acumulo_porcentagem = 0;
	$sql_escolagrau = mysql_query("SELECT * FROM escolagrau");
	while ($escolagrau = mysql_fetch_array($sql_escolagrau)) {
		$contador = mysql_query("SELECT COUNT(*) AS valor FROM usuario WHERE escola_id_escolagrau=".$escolagrau["id"]);
		$total = mysql_result($contador, 0, "valor");
		$array_escolagrau[] = array('id' => $escolagrau["id"], 'total' => $total);
	}
?>
<strong>Por Grau de Instru&ccedil;&atilde;o</strong></font>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td>&nbsp;</td>
		<td width="100" align="center">TOTAL</td>
		<td width="100" align="center">%</td>
	</tr>
<?php 
	foreach ($array_escolagrau as $key => $row) {
		$total = $row['total'];
		$porcentagem = round((($total*100)/$total_usuario), 2);
		$sql_escolagrau = mysql_query("SELECT * FROM escolagrau WHERE id=". $row['id']);
		if ($escolagrau = mysql_fetch_array($sql_escolagrau)) {
			$linha = $escolagrau["grau"];
?>
	<tr>
		<td><?php echo $linha ?>:</td>
		<td align="center"><?php echo $total ?></td>
		<td align="center"><?php echo $porcentagem." %"; ?></td>
	</tr>
<?php 
		}
		$acumulo_total += $total;
		$acumulo_porcentagem += $porcentagem;
	}
?>
	<tr>
		<td><font color="#FF0000">Total:</font></td>
		<td align="center"><font color="#FF0000"><?php echo $acumulo_total ?></font></td>
		<td align="center"><font color="#FF0000"><?php echo $acumulo_porcentagem." %"; ?></font></td>
	</tr>
</table>
<br />
<?php
	$acumulo_total = 0;
	$acumulo_porcentagem = 0;
	$sql_escolaserie = mysql_query("SELECT * FROM escolaserie");
	while ($escolaserie = mysql_fetch_array($sql_escolaserie)) {
		$contador = mysql_query("SELECT COUNT(*) AS valor FROM usuario WHERE escola_id_escolaserie=".$escolaserie["id"]);
		$total = mysql_result($contador, 0, "valor");
		$array_escolaserie[] = array('id' => $escolaserie["id"], 'total' => $total);
	}
	$contador = mysql_query("SELECT COUNT(*) AS valor FROM usuario WHERE escola_id_escolaserie=0");
	$total = mysql_result($contador, 0, "valor");
	$array_escolaserie[] = array('id' => 0, 'total' => $total);
?>
<strong>Por S&eacute;rie Escolar</strong></font>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td>&nbsp;</td>
		<td width="100" align="center">TOTAL</td>
		<td width="100" align="center">%</td>
	</tr>
<?php 
	foreach ($array_escolaserie as $key => $row) {
		$total = $row['total'];
		$porcentagem = round((($total*100)/$total_usuario), 2);
		$sql_escolaserie = mysql_query("SELECT * FROM escolaserie WHERE id=". $row['id']);
		if ($escolaserie = mysql_fetch_array($sql_escolaserie)) {
			$linha = $escolaserie["serie"];
?>
	<tr>
		<td><?php echo $linha ?>:</td>
		<td align="center"><?php echo $total ?></td>
		<td align="center"><?php echo $porcentagem." %"; ?></td>
	</tr>
<?php 
		}
		if ($row['id'] == 0) {
			$linha = "N�o responderam";
?>
	<tr>
		<td><?php echo $linha ?>:</td>
		<td align="center"><?php echo $total ?></td>
		<td align="center"><?php echo $porcentagem." %"; ?></td>
	</tr>
<?php 
		}
		$acumulo_total += $total;
		$acumulo_porcentagem += $porcentagem;
	}
?>
	<tr>
		<td><font color="#FF0000">Total:</font></td>
		<td align="center"><font color="#FF0000"><?php echo $acumulo_total ?></font></td>
		<td align="center"><font color="#FF0000"><?php echo $acumulo_porcentagem." %"; ?></font></td>
	</tr>
</table>
<br />
<?php
	$acumulo_total = 0;
	$acumulo_porcentagem = 0;
	$sql_escolaturno = mysql_query("SELECT * FROM escolaturno");
	while ($escolaturno = mysql_fetch_array($sql_escolaturno)) {
		$contador = mysql_query("SELECT COUNT(*) AS valor FROM usuario WHERE escola_id_escolaturno=".$escolaturno["id"]);
		$total = mysql_result($contador, 0, "valor");
		$array_escolaturno[] = array('id' => $escolaturno["id"], 'total' => $total);
	}
	$contador = mysql_query("SELECT COUNT(*) AS valor FROM usuario WHERE escola_id_escolaturno=0");
	$total = mysql_result($contador, 0, "valor");
	$array_escolaturno[] = array('id' => 0, 'total' => $total);
?>
<strong>Por Turno Escolar</strong></font>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td>&nbsp;</td>
		<td width="100" align="center">TOTAL</td>
		<td width="100" align="center">%</td>
	</tr>
<?php 
	foreach ($array_escolaturno as $key => $row) {
		$total = $row['total'];
		$porcentagem = round((($total*100)/$total_usuario), 2);
		$sql_escolaturno = mysql_query("SELECT * FROM escolaturno WHERE id=". $row['id']);
		if ($escolaturno = mysql_fetch_array($sql_escolaturno)) {
			$linha = $escolaturno["turno"];
?>
	<tr>
		<td><?php echo $linha ?>:</td>
		<td align="center"><?php echo $total ?></td>
		<td align="center"><?php echo $porcentagem." %"; ?></td>
	</tr>
<?php 
		}
		if ($row['id'] == 0) {
			$linha = "N�o responderam";
?>
	<tr>
		<td><?php echo $linha ?>:</td>
		<td align="center"><?php echo $total ?></td>
		<td align="center"><?php echo $porcentagem." %"; ?></td>
	</tr>
<?php 
		}
		$acumulo_total += $total;
		$acumulo_porcentagem += $porcentagem;
	}
?>
	<tr>
		<td><font color="#FF0000">Total:</font></td>
		<td align="center"><font color="#FF0000"><?php echo $acumulo_total ?></font></td>
		<td align="center"><font color="#FF0000"><?php echo $acumulo_porcentagem." %"; ?></font></td>
	</tr>
</table>
<br />
<?php
	$acumulo_total = 0;
	$acumulo_porcentagem = 0;
	$sql_escolanome = mysql_query("SELECT * FROM escolanome");
	while ($escolanome = mysql_fetch_array($sql_escolanome)) {
		$contador = mysql_query("SELECT COUNT(*) AS valor FROM usuario WHERE escola_id_escolanome=".$escolanome["Id"]);
		$total = mysql_result($contador, 0, "valor");
		$array_escolanome[] = array('id' => $escolanome["Id"], 'total' => $total);
	}
	$contador = mysql_query("SELECT COUNT(*) AS valor FROM usuario WHERE escola_id_escolanome=0");
	$total = mysql_result($contador, 0, "valor");
	$array_escolanome[] = array('id' => 0, 'total' => $total);
?>
<strong>Por Escola</strong>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td>&nbsp;</td>
		<td width="100" align="center">TOTAL</td>
		<td width="100" align="center">%</td>
	</tr>
<?php 
	foreach ($array_escolanome as $key => $row) {
		$total = $row['total'];
		$porcentagem = round((($total*100)/$total_usuario), 2);
		$sql_escolanome = mysql_query("SELECT * FROM escolanome WHERE Id=". $row['id']);
		if ($escolanome = mysql_fetch_array($sql_escolanome)) {
			$linha = $escolanome["nome"];
?>
	<tr>
		<td><?php echo $linha ?>:</td>
		<td align="center"><?php echo $total ?></td>
		<td align="center"><?php echo $porcentagem." %"; ?></td>
	</tr>
	<?php 
		}
		if ($row['id'] == 0) {
			$linha = "N�o responderam";
?>
	<tr>
		<td><?php echo $linha ?>:</td>
		<td align="center"><?php echo $total ?></td>
		<td align="center"><?php echo $porcentagem." %"; ?></td>
	</tr>
<?php 
		}
		$acumulo_total += $total;
		$acumulo_porcentagem += $porcentagem;
	}
?>
	<tr>
		<td><font color="#FF0000">Total:</font></td>
		<td align="center"><font color="#FF0000"><?php echo $acumulo_total ?></font></td>
		<td align="center"><font color="#FF0000"><?php echo $acumulo_porcentagem." %"; ?></font></td>
	</tr>
</table>
<br />
<?php
	$acumulo_total = 0;
	$acumulo_porcentagem = 0;
	$sql_empregosituacao = mysql_query("SELECT * FROM empregosituacao");
	while ($empregosituacao = mysql_fetch_array($sql_empregosituacao)) {
		$contador = mysql_query("SELECT COUNT(*) AS valor FROM usuario WHERE emprego_id_empregosituacao=".$empregosituacao["id"]);
		$total = mysql_result($contador, 0, "valor");
		$array_empregosituacao[] = array('id' => $empregosituacao["id"], 'total' => $total);
	}
?>
<strong>Por Situa&ccedil;&atilde;o no Trabalho</strong>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td>&nbsp;</td>
		<td width="100" align="center">TOTAL</td>
		<td width="100" align="center">%</td>
	</tr>
<?php 
	foreach ($array_empregosituacao as $key => $row) {
		$total = $row['total'];
		$porcentagem = round((($total*100)/$total_usuario), 2);
		$sql_empregosituacao = mysql_query("SELECT * FROM empregosituacao WHERE Id=". $row['id']);
		if ($empregosituacao = mysql_fetch_array($sql_empregosituacao)) {
			$linha = $empregosituacao["situacao"];
?>
	<tr>
		<td><?php echo $linha ?>:</td>
		<td align="center"><?php echo $total ?></td>
		<td align="center"><?php echo $porcentagem." %"; ?></td>
	</tr>
<?php 
		}
		$acumulo_total += $total;
		$acumulo_porcentagem += $porcentagem;
	}
?>
	<tr>
		<td><font color="#FF0000">Total:</font></td>
		<td align="center"><font color="#FF0000"><?php echo $acumulo_total ?></font></td>
		<td align="center"><font color="#FF0000"><?php echo $acumulo_porcentagem." %"; ?></font></td>
	</tr>
</table>
<br />
<?php
	$total_usuario_programagoverno = 0;
	$acumulo_total = 0;
	$acumulo_porcentagem = 0;
	$total_deficiencia = RecordCount("usuario_programagoverno","assinalado='sim'");
	$sql_programagoverno = mysql_query("SELECT * FROM programagoverno");
	while ($programagoverno = mysql_fetch_array($sql_programagoverno)) {
		$contador = mysql_query("SELECT COUNT(*) AS valor FROM usuario_programagoverno WHERE id_programagoverno=".$programagoverno["id"]." AND assinalado='sim'");
		$total = mysql_result($contador, 0, "valor");
		$array_programagoverno[] = array('id' => $programagoverno["id"], 'total' => $total);
		$total_usuario_programagoverno += $total;
	}
?>
<strong>Por Benif&iacute;cio Social</strong>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td>&nbsp;</td>
		<td width="100" align="center">TOTAL</td>
		<td width="100" align="center">%</td>
	</tr>
<?php 
	foreach ($array_programagoverno as $key => $row) {
		$total = $row['total'];
		$porcentagem = round((($total*100)/$total_usuario_programagoverno), 2);
		$sql_programagoverno = mysql_query("SELECT * FROM programagoverno WHERE id=". $row['id']);
		if ($programagoverno = mysql_fetch_array($sql_programagoverno)) {
			$linha = $programagoverno["programa"];
?>
	<tr>
		<td><?php echo $linha ?>:</td>
		<td align="center"><?php echo $total ?></td>
		<td align="center"><?php echo $porcentagem." %"; ?></td>
	</tr>
<?php 
		}
		$acumulo_total += $total;
		$acumulo_porcentagem += $porcentagem;
	}
?>
	<tr>
		<td><font color="#FF0000">Total:</font></td>
		<td align="center"><font color="#FF0000"><?php echo $acumulo_total ?></font></td>
		<td align="center"><font color="#FF0000"><?php echo $acumulo_porcentagem." %"; ?></font></td>
	</tr>
</table>
</center>