<center>
<strong>Rela&ccedil;&atilde;o de estagi&aacute;rios alocados por telecentro</strong><br />
<br />
<?php 
	$sql_telecentro = mysql_query("SELECT * FROM telecentro ORDER BY codigo ASC");
	while ($telecentro = mysql_fetch_array($sql_telecentro)) {
		$total = RecordCount("estagiario","codigo_telecentro=".$telecentro['codigo']."");
		if ($total > 0) {
			echo "<strong>".$telecentro['nome']."</strong><br><br>";
			$sql_estagiario = mysql_query("SELECT * FROM estagiario WHERE codigo_telecentro=".$telecentro['codigo']." ORDER BY nome ASC");
			while ($estagiario = mysql_fetch_array($sql_estagiario)) {
				$nome							= $estagiario["nome"];
				$dataadmissao           = $estagiario["dataadmissao"];
				$telefonefixo           = $estagiario["telefonefixo"];
				$telefonecelular			= $estagiario["telefonecelular"];
				$email						= strtoupper($estagiario["email"]);
				$matricula					= $estagiario["matricula"];
				$horaentrada				= $estagiario["horaentrada"];
				$horasaida					= $estagiario["horasaida"];
				$escola_escolanome		= $estagiario["escola_escolanome"];
				$escola_curso				= $estagiario["escola_curso"];
				$cursos_adicionais		= $estagiario["cursos_adicionais"];
				$documento_identidade	= $estagiario["documento_identidade"];
				$documento_cpf				= $estagiario["documento_cpf"];
				$documento_comprovanteresidencia	= $estagiario["documento_comprovanteresidencia"];
				$documento_declaracaofaculdade	= $estagiario["documento_declaracaofaculdade"];
				$documento_foto			= $estagiario["documento_foto"];
				$escola_escolaturno		= strtoupper(mysql_result(mysql_query("SELECT * FROM escolaturno WHERE id=". $estagiario["escola_id_escolaturno"]), 0, "turno"));
				$escola_escolagrau 		= strtoupper(mysql_result(mysql_query("SELECT * FROM escolagrau WHERE id=". $estagiario["escola_id_escolagrau"]), 0, "grau"));
				$escola_escolaserie		= strtoupper(mysql_result(mysql_query("SELECT * FROM escolaserie WHERE id=". $estagiario["escola_id_escolaserie"]), 0, "serie"));
				
				if ($horaentrada == "*") {
					$horaentrada = "00:00";
				}
				
				if ($horasaida == "*") {
					$horasaida = "00:00";
				}
				
				$enderecologradouro		= $estagiario["enderecologradouro"];
				$endereconumero			= $estagiario["endereconumero"];
				$enderecocomplemento		= $estagiario["enderecocomplemento"];
				$enderecocep				= $estagiario["enderecocep"];
				$endereco_bairro = strtoupper(mysql_result(mysql_query("SELECT * FROM bairro WHERE id=". $estagiario["endereco_id_bairro"]), 0, "bairro"));
				$endereco_municipio = strtoupper(mysql_result(mysql_query("SELECT * FROM municipio WHERE id=". $estagiario["endereco_id_municipio"]), 0, "municipio"));
				$endereco_uf = strtoupper(mysql_result(mysql_query("SELECT * FROM uf WHERE id=". $estagiario["endereco_id_uf"]), 0, "uf"));
				
				$enderecocompleto = $enderecologradouro;
				if (!(empty($endereconumero) || $endereconumero ==0)) {
					$enderecocompleto .= ", N� $endereconumero";
				}
				if (!empty($enderecocomplemento)) {
					$enderecocompleto .= ", $enderecocomplemento";
				}
				$enderecocompleto .= ", $endereco_bairro, $endereco_municipio, $endereco_uf";
				
				$qualificacaoescolar = "$escola_escolanome ($escola_escolagrau)";
				$qualificacaoescolar .= "<br>$escola_curso / $escola_escolaserie ($escola_escolaturno)";
				if (empty($cursos_adicionais)) {
					$qualificacaoescolar .= "<br>$cursos_adicionais";
				}

				if ($documento_identidade == 0 || $documento_cpf == 0 || $documento_comprovanteresidencia == 0 || $documento_declaracaofaculdade == 0 || $documento_foto == 0) {
					if ($documento_identidade == 0) {
						$documentospendentes = "IDENTIDADE";
					}
					if ($documento_cpf == 0) {
						if (!empty($documentospendentes)) { $documentospendentes .= ", "; };
						$documentospendentes .= "CPF";
					}
					if ($documento_comprovanteresidencia == 0) {
						if (!empty($documentospendentes)) { $documentospendentes .= ", "; };
						$documentospendentes .= "COMPROVANTE DE RESIDENCIA";
					}
					if ($documento_declaracaofaculdade == 0) {
						if (!empty($documentospendentes)) { $documentospendentes .= ", "; };
						$documentospendentes .= "DECLARA��O DA FACULDADE";
					}
					if ($documento_foto == 0) {
						if (!empty($documentospendentes)) { $documentospendentes .= ", "; };
						$documentospendentes .= "FOTOS 3x4";
					}
				}
?>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="6"><strong><em><?php echo $nome ?></em></strong></td>
		</tr>
		<tr>
			<td width="175" valign="top"><strong>Data de Admiss&atilde;o: </strong></td>
			<td><?php echo ConverteData($dataadmissao, "/", "mysql.normal") ?></td>
			<td width="85" valign="top"><strong>Matr&iacute;cula:</strong></td>
			<td><?php echo $matricula ?></td>
			<td width="85" valign="top"><strong>Hor&aacute;rio:</strong></td>
			<td><?php echo $horaentrada ?> &agrave; <?php echo $horasaida ?></td>
		</tr>
		<tr>
			<td valign="top"><strong>Endere&ccedil;o:</strong></td>
			<td colspan="5"><?php echo $enderecocompleto ?></td>
		</tr>
		<tr>
			<td valign="top"><strong>Telefone(s):</strong></td>
			<td colspan="5"><?php echo "$telefonefixo / $telefonecelular" ?></td>
		</tr>
		<tr>
			<td valign="top"><strong>E-mail:</strong></td>
			<td colspan="5"><?php echo $email ?></td>
		</tr>
		<tr>
			<td valign="top"><strong>Qualifica&ccedil;&atilde;o Escolar: </strong></td>
			<td colspan="5"><?php echo $qualificacaoescolar ?></td>
		</tr>
		<tr>
			<td valign="top"><strong>Documentos Pendentes: </strong></td>
			<td colspan="5"><?php echo $documentospendentes ?></td>
		</tr>
	</table>
	<br />
	<?php 
			}
			echo "<hr />";
		}
	}
?>
</center>
