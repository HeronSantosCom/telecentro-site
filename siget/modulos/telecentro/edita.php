<?php 

	// carregas as informações do telecentro

	$sql = mysql_query("SELECT * FROM telecentro WHERE id=" . $_GET["id"]);

	$telecentro = mysql_fetch_array($sql);

	

	$id_telecentro				= $telecentro["id"];

	$codigo	 					= $telecentro["codigo"];

	$nome							= $telecentro["nome"];

	$tipo							= $telecentro["tipo"];

	$estacoes					= $telecentro["estacoes"];

	$enderecologradouro		= $telecentro["enderecologradouro"];

	$endereconumero			= $telecentro["endereconumero"];

	$enderecocomplemento		= $telecentro["enderecocomplemento"];

	$enderecocep				= $telecentro["enderecocep"];

	$endereco_id_bairro		= $telecentro["endereco_id_bairro"];

	$endereco_id_municipio	= $telecentro["endereco_id_municipio"];

	$endereco_id_uf			= $telecentro["endereco_id_uf"];

?>

<?php TopicoCinza("Dados do Telecentro"); ?>

<form action="?pm=telecentro&ps=edita.post" method="post" name="telecentro" id="telecentro" onSubmit="return validaTelecentro();">

	<input name="id_telecentro" value="<?php echo $id_telecentro ?>" type="hidden" />

	<input name="codigo" value="<?php echo $codigo ?>" type="hidden" />

	<table width="700" border="0" cellspacing="2" cellpadding="0">

		<tr>

			<td width="200" class="linha-salmao-fundo">Nome:</td>

			<td><input name="nome" type="text" class="input-destacado" id="nome" style="width:100%" value="<?php echo $nome; ?>" /></td>

		</tr>

		<tr>

			<td class="linha-salmao-fundo">Tipo:</td>

			<td><select name="tipo" id="tipo" style="width:100%" class="input-destacado" >

					<option value="" selected="selected">Selecione...</option>

					<option value="">-------------------</option>

<?php 

	if ($telecentro["tipo"] == 1) {

		$seleciona1 = "selected=\"selected\"";

	} elseif ($telecentro["tipo"] == 2) {

		$seleciona2 = "selected=\"selected\"";

	} elseif ($telecentro["tipo"] == 4) {

		$seleciona4 = "selected=\"selected\"";

	}

?>

					<option value="1" <?php echo $seleciona1 ?>>Telecentro Escolar</option>

					<option value="2" <?php echo $seleciona2 ?>>Telecentro Comunit&aacute;rio</option>

					<option value="4" <?php echo $seleciona4 ?>>Telecentro Conectividade</option>

			</select></td>

		</tr>

		<tr>

			<td class="linha-salmao-fundo">Quantidade de Esta&ccedil;&otilde;es: </td>

			<td><table border="0" cellspacing="0" cellpadding="0">

					<tr>

						<td width="100"><input name="estacoes" type="text" class="input-destacado" id="estacoes" style="width:100%" onkeypress="return BloqueiaAlfa(event);" value="<?php echo $estacoes ?>" maxlength="2" /></td>

						<td width="2">&nbsp;</td>

						<td class="linha-salmao-fundodireito">esta&ccedil;&otilde;es&nbsp;</td>

					</tr>

			</table></td>

		</tr>

	</table>

	<?php TopicoCinza("Identifica&ccedil;&atilde;o do Telecentro"); ?>

	<table width="700" border="0" cellspacing="2" cellpadding="0">

		<tr>

			<td width="200" align="left" class="linha-salmao-fundo"> Logradouro:</td>

			<td><input name="enderecologradouro" type="text" class="input-destacado" id="enderecologradouro" style="width:100%" onkeyup="this.value = this.value.toUpperCase();" value="<?php echo $enderecologradouro ?>" maxlength="100"/></td>

		</tr>

		<tr>

			<td width="200" align="left" class="linha-salmao-fundo">N&uacute;mero:</td>

			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">

					<tr>

						<td><input name="endereconumero" type="text" class="input-destacado" id="endereconumero" style="width:100%"onkeypress="return BloqueiaAlfa(event);" value="<?php echo $endereconumero ?>" maxlength="7" /></td>

						<td width="2">&nbsp;</td>

						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>

					</tr>

				</table></td>

		</tr>

		<tr>

			<td width="200" align="left" class="linha-salmao-fundo">Complemento:</td>

			<td><input name="enderecocomplemento" type="text" class="input-normal" id="enderecocomplemento" style="width:100%" onkeyup="this.value = this.value.toUpperCase();" value="<?php echo $enderecocomplemento ?>" maxlength="50"/></td>

		</tr>

		<tr>

			<td width="200" align="left" class="linha-salmao-fundo">CEP:</td>

			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">

					<tr>

						<td><input name="enderecocep" type="text" class="input-normal" id="enderecocep" style="width:100%" onkeypress="return BloqueiaAlfa(event);" onkeyup="FormataCampo(this, '#####-###', event);" value="<?php echo $enderecocep ?>" maxlength="9" /></td>

						<td width="2">&nbsp;</td>

						<td width="25"><input type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>

					</tr>

				</table></td>

		</tr>

		<tr>

			<td align="left" class="linha-salmao-fundo">Estado:</td>

			<td><select name="endereco_id_uf" id="endereco_id_uf" class="input-destacado" style="width:75%" onChange="comboDynamic(this.value,'municipio')" >

					<option value="" selected="selected">Selecione...</option>

					<option value="">-------------------</option>

					<?php 

	// enfilera os estados

	$sql = mysql_query("select * from uf order by estado asc");

	while ($uf = mysql_fetch_array($sql)) {

		if ($endereco_id_uf == $uf['id']) {		

			echo "<option value=\"".$uf['id']."\" selected=\"selected\">".$uf['estado']."</option>";

		} else {

			echo "<option value=\"".$uf['id']."\">".$uf['estado']."</option>";

		}

	}

?>
					<option value="">-------------------</option>
					<option value="999999" <?php if ($endereco_id_uf == "999999") echo "selected=\"selected\""; ?>>N&atilde;o Listado...</option>

			</select></td>

		</tr>

		<tr>

			<td align="left" class="linha-salmao-fundo">Munic&iacute;pio:</td>

			<td><select name="endereco_id_municipio" id="endereco_id_municipio" class="input-destacado" style="width:75%" onChange="comboDynamic(this.value,'bairro')" >

					<option value="" selected="selected">Selecione...</option>

					<option value="">-------------------</option>

					<?php 

	// enfilera municipios

	$sql = mysql_query("select * from municipio where id_uf = $endereco_id_uf order by municipio asc");

	while ($municipio = mysql_fetch_array($sql)) {

		if ($endereco_id_municipio == $municipio['id']) {		

			echo "<option value=\"".$municipio['id']."\" selected=\"selected\">".$municipio['municipio']."</option>";

		} else {

			echo "<option value=\"".$municipio['id']."\">".$municipio['municipio']."</option>";

		}

	}

?>
					<option value="">-------------------</option>
					<option value="999999" <?php if ($endereco_id_municipio == "999999") echo "selected=\"selected\""; ?>>N&atilde;o Listado...</option>

			</select></td>

		</tr>

		<tr>

			<td width="200" align="left" class="linha-salmao-fundo">Bairro:</td>

			<td><select name="endereco_id_bairro" id="endereco_id_bairro" class="input-destacado" style="width:75%" >

					<option value="" selected="selected">Selecione...</option>

					<option value="">-------------------</option>

					<?php 

	// enfilera bairros

	$sql = mysql_query("select * from bairro where (id_urg in(select id from urg where id_municipio = $endereco_id_municipio)) order by bairro asc");

	while ($bairro = mysql_fetch_array($sql)) {

		if ($endereco_id_bairro == $bairro['id']) {		

			echo "<option value=\"".$bairro['id']."\" selected=\"selected\">".$bairro['bairro']."</option>";

		} else {

			echo "<option value=\"".$bairro['id']."\">".$bairro['bairro']."</option>";

		}

	}

?>
					<option value="">-------------------</option>
					<option value="999999" <?php if ($endereco_id_bairro == "999999") echo "selected=\"selected\""; ?>>N&atilde;o Listado...</option>

			</select></td>

		</tr>

	</table>

	<br />

	<table width="700" border="0" cellspacing="3" cellpadding="0">

		<tr>

			<td width="32%"><input type="button" class="button-normal" value="Cancelar" onClick="javascript:CarregaPagina('?pm=telecentro','_self')" /></td>

			<td width="32%"><input type="button" class="button-normal" value="Remover" onClick="javascript:CarregaPagina('?pm=telecentro&ps=remove&id=<?php echo $id_telecentro ?>','_self')" /></td>

			<td width="36%"><input name="post" type="submit" class="button-destacado" id="post" value="Editar" /></td>

		</tr>

	</table>

</form>

