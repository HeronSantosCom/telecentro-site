<?php TopicoCinza("Dados do Telecentro"); ?>

<form action="?pm=telecentro&ps=cadastro.post" method="post" name="telecentro" id="telecentro" onSubmit="return validaTelecentro();">

	<table width="700" border="0" cellspacing="2" cellpadding="0">

		<tr>

			<td width="200" class="linha-salmao-fundo">Nome:</td>

			<td><input name="nome" type="text" class="input-destacado" id="nome" style="width:100%" /></td>

		</tr>

		<tr>

			<td class="linha-salmao-fundo">Tipo:</td>

			<td><select name="tipo" id="tipo" style="width:100%" class="input-destacado" >

					<option value="" selected="selected">Selecione...</option>

					<option value="">-------------------</option>

					<option value="1">Telecentro Escolar</option>

					<option value="2">Telecentro Comunit&aacute;rio</option>

					<option value="4">Telecentro Conectividade</option>

				</select></td>

		</tr>

		<tr>

			<td class="linha-salmao-fundo">Quantidade de Esta&ccedil;&otilde;es: </td>

			<td><table border="0" cellspacing="0" cellpadding="0">

					<tr>

						<td width="100"><input name="estacoes" type="text" class="input-destacado" id="estacoes" style="width:100%" onkeypress="return BloqueiaAlfa(event);" maxlength="2" /></td>

						<td width="2">&nbsp;</td>

						<td class="linha-salmao-fundodireito">esta&ccedil;&otilde;es&nbsp;</td>

					</tr>

				</table></td>

		</tr>

	</table>

	<?php TopicoCinza("Identifica&ccedil;&atilde;o do Telecentro"); ?>

	<table width="700" border="0" cellspacing="2" cellpadding="0">

		<tr>

			<td width="200" align="left" class="linha-salmao-fundo"> Logradouro:</td>

			<td><input name="enderecologradouro" type="text" class="input-destacado" id="enderecologradouro" maxlength="100" style="width:100%" onkeyup="this.value = this.value.toUpperCase();"/></td>

		</tr>

		<tr>

			<td width="200" align="left" class="linha-salmao-fundo">N&uacute;mero:</td>

			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">

					<tr>

						<td><input name="endereconumero" type="text" class="input-destacado" id="endereconumero" style="width:100%"onkeypress="return BloqueiaAlfa(event);" maxlength="7" /></td>

						<td width="2">&nbsp;</td>

						<td width="25"><input name="button" type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>

					</tr>

				</table></td>

		</tr>

		<tr>

			<td width="200" align="left" class="linha-salmao-fundo">Complemento:</td>

			<td><input name="enderecocomplemento" type="text" class="input-normal" id="enderecocomplemento" maxlength="50" style="width:100%" onkeyup="this.value = this.value.toUpperCase();"/></td>

		</tr>

		<tr>

			<td width="200" align="left" class="linha-salmao-fundo">CEP:</td>

			<td><table width="30%" border="0" cellspacing="0" cellpadding="0">

					<tr>

						<td><input name="enderecocep" type="text" class="input-normal" id="enderecocep" style="width:100%" onkeyup="FormataCampo(this, '#####-###', event);" onkeypress="return BloqueiaAlfa(event);" maxlength="9" /></td>

						<td width="2">&nbsp;</td>

						<td width="25"><input name="button" type="button" class="button-normal" onclick="alert('Digite somente n&uacute;meros...')" value="?" /></td>

					</tr>

				</table></td>

		</tr>

		<tr>
			<td align="left" class="linha-salmao-fundo">Estado:</td>
			<td><select name="endereco_id_uf" id="endereco_id_uf" class="input-destacado" style="width:75%" onChange="comboDynamic(this.value,'municipio')" >
					<option value="" selected="selected">Selecione...</option>
					<option value="">-------------------</option>
					<?php 
	// enfilera os estados
	$sql = mysql_query("select * from uf order by estado asc");
	while ($uf = mysql_fetch_array($sql)) {
		echo "<option value=\"".$uf['id']."\">".$uf['estado']."</option>";
	}
?>
					<option value="">-------------------</option>
					<option value="999999">N&atilde;o Listado...</option>
				</select></td>
		</tr>
		<tr>
			<td align="left" class="linha-salmao-fundo">Munic&iacute;pio:</td>
			<td><select name="endereco_id_municipio" id="endereco_id_municipio" class="input-destacado" style="width:75%" onChange="comboDynamic(this.value,'bairro')" >
					<option value="" selected="selected">Selecione...</option>
				</select></td>
		</tr>
		<tr>
			<td width="200" align="left" class="linha-salmao-fundo">Bairro:</td>
			<td><select name="endereco_id_bairro" id="endereco_id_bairro" class="input-destacado" style="width:75%" >
					<option value="" selected="selected">Selecione...</option>
				</select></td>
		</tr>

	</table>

	<br />

	<table width="700" border="0" cellspacing="3" cellpadding="0">

		<tr>

			<td width="35%"><input type="button" class="button-normal" value="Cancelar" onClick="javascript:CarregaPagina('?pm=telecentro','_self')" /></td>

			<td width="65%"><input name="post" type="submit" class="button-destacado" id="post" value="Cadastrar" /></td>

		</tr>

	</table>

</form>

