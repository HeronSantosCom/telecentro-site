<?php 
	header("Content-type: application/msword");
	header("Content-Disposition: attachment; filename=relatorio_".$_GET["relat"]."_".date("dmY").".doc");
	require("../include.configuracao.php");
	require("../include.funcao.php");
	$relat = $_GET["relat"];
	$loadRelatorio = "../modulos/relatorio/$relat.php";

	include($loadRelatorio);
	
	GravaLog("gerado relatório de $relat");
?>